# PDM-R

## What it is

This is PDM-R, a French fangame based on Pokémon Mystery Dungeon and Poképark, and made in Unity. This project was started on November 2017. The full name of this project is **Pokémon Donjon Mystère : Les Aventuriers du Vide**, which translates to *Adventurers of Void*.

This game uses USUM models and animations for the Pokémon, Gates to Infinity models for items and Mystery Dungeon related objects, and a combination of Wind Waker HD textures and altered photorealistic ones.

## Gameplay

The gameplay is a mix between Mystery Dungeon and Poképark: phases of roguelike dungeon crawling alternate with phases of free exploration in defined areas, where NPCs will entrust you with side quests and other missions.

The dungeon crawling mechanics are mostly based on **Explorers of Sky**, but recently we've started to take the newest **Rescue Team DX** into consideration for improving user experience.

Some custom mechanics are also planned, like the use of Z Crystals and Z Moves in dungeons, and the ability to interact with certain parts of the environment in the overworld.

## Contribution

You can contribute to this project if you want to. To do that, I advise you to read [this document](https://drive.google.com/open?id=1b8ls-FbSkpyp5aHRXFn6G16XVkbtElJo) to find out what kind of skills we're looking for. Then, you can fill your contribution over [here](https://drive.google.com/drive/folders/1Lio_tAfdOT7QVdffMmhNQN0etdni0ZBu), and you'll be contacted by someone from the staff. Everything is in French for the moment, but depending on the demand for contribution we will consider making English versions.

## FAQ

> Are gen 8 Pokémon available in this game? Will they ever be?

Short answer is no. We consider this game as the mystery dungeon game that gen 7 didn't have. Gen 7 Pokémon are being put forward, and the story is heavily tied to Ultra Beasts. Besides, because we are using USUM resources, and because a lot of Pokémon are not available in Sword and Shield, there would be too much work to do on the textures in order to make everything look consistent.

> Will this game implement Mega Evolution like in **Rescue Team DX**?

At the present time, our general consensus is to not implement Mega Evolution, in profit of Z Crystals. That being said, if it becomes a very requested feature, we will take it into consideration.

> What about the canonicity of this game in the Mystery Dungeon lore?

We do not consider this game to be canon in the Mystery Dungeon lore. There may be one or two references or Easter Eggs to the official games, but this game takes place in a totally different universe and different world. If a Pokémon appeared in an official Mystery Dungeon game, it is a completely different character in this game.

> What are the starters? Is there a personality test at the start of the game?

There is a personality test indeed, and as opposed to the latest entries in the franchise, the starter that is given to you cannot be changed afterwards. But you can still choose your partner. There are 16 starters in total, which are Bulbasaur, Squirtle, Charmander, Pikachu, Houndour, Phanpy, Aron, Shinx, Riolu, Axew, Fletchling, Litleo, Rowlet, Litten, Popplio, and Rockruff.

> Aren't you afraid getting a C&D from Nintendo? Won't Unity Technologies shutdown your project?

This is always a possibility of course. As the project grows, it gets closer to getting shutdown. That is why I am personally taking as much precaution as I can, like limiting communication and privatising contribution. The game will also be officially distributed only through peer to peer. We do not have any website. Maybe this is excessive, but since this project is quite big it may take another few years to be completed, and I want it to keep going as long as possible.