﻿using System;
using System.Collections.Generic;
using Pokemon;
using Pokemon.Lexic;

public class Quizz
{
	public int
		score_EI,
		score_SN,
		score_TF,
		score_JP;

	public Gender protagGender;

	private int protagID;
	private int partnerID;

	private readonly Random random = new Random();
	private readonly string[] questions;
	private readonly List<int> suite;

	public Quizz(int numberOf)
	{
		score_EI = 0;
		score_SN = 0;
		score_TF = 0;
		score_JP = 0;

		questions = new string[70];

		for (int i = 0; i < 70; ++i)
			questions[i] = "c0_prologue/q" + (i + 1).ToString();

		suite = new List<int>();

		int index = 0;
		while (index < numberOf)
		{
			int rand = random.Next(0, questions.Length);

			if (!suite.Contains(rand))
			{
				suite.Add(rand);
				++index;
			}
		}
	}

	public List<PokemonStarter> PotentialPartners
	{
		get
		{
			var list = PokemonStarter.starters.FindAll(x => x.id != protagID);
			list.Sort((a, b) => a.pokemon.Species.CompareTo(b.pokemon.Species));

			return list;
		}
	}

	public string CurrentScore
	{
		get
		{
			var finalScore = "";
   
			if (score_EI > 0)
				finalScore += "E";
			else if (score_EI < 0)
				finalScore += "I";
			else
				finalScore += ".";

			if (score_SN > 0)
				finalScore += "S";
			else if (score_SN < 0)
				finalScore += "N";
			else
				finalScore += ".";

			if (score_TF > 0)
				finalScore += "T";
			else if (score_TF < 0)
				finalScore += "F";
			else
				finalScore += ".";

			if (score_JP > 0)
				finalScore += "J";
			else if (score_JP < 0)
				finalScore += "P";
			else
				finalScore += ".";

			return finalScore;
		}
	}

	public List<int> GetSuite()
	{
		return suite;
	}

	public string GetQuestion(int id)
	{
		if (id < 0 || id >= questions.Length)
			return null;

		return questions[id];
	}

	public void Register(int n, int choice)
	{
		int qn = n % 7;

		if (qn == 0) // E or I
		{
			if (choice == 0)
				score_EI++;
			else
				score_EI--;
		}
		else if (qn == 1 || qn == 2) // S or N
		{
			if (choice == 0)
				score_SN++;
			else
				score_SN--;
		}
		else if (qn == 3 || qn == 4) // T or F
		{
			if (choice == 0)
				score_TF++;
			else
				score_TF--;
		}
		else if (qn == 5 || qn == 6) // J or P
		{
			if (choice == 0)
				score_JP++;
			else
				score_JP--;
		}
	}

	public void Validate()
	{
		var score = CurrentScore;

		if (score_EI == 0)
			score = score.ReplaceAt(0, random.NextDouble() >= 0.5 ? 'E' : 'I');

		if (score_SN == 0)
			score = score.ReplaceAt(1, random.NextDouble() >= 0.5 ? 'S' : 'N');

		if (score_TF == 0)
			score = score.ReplaceAt(2, random.NextDouble() >= 0.5 ? 'T' : 'F');

		if (score_JP == 0)
			score = score.ReplaceAt(3, random.NextDouble() >= 0.5 ? 'J' : 'P');

		var playerStarter = PokemonStarter.starters.Find(x => x.identifier == score);

		protagID = playerStarter.id;

		playerStarter.pokemon.gender = protagGender;

		Game.PartyAdd(playerStarter.pokemon);
		Game.Protagonist = Game.PartyIndex(playerStarter.pokemon);
		Game.TeamAddIndex(Game.Protagonist);
	}

	public void ChoosePartner(int id)
	{
		partnerID = id;

		var partnerStarter = PokemonStarter.starters.Find(x => x.id == partnerID);

		Game.PartyAdd(partnerStarter.pokemon);
		Game.Partner = Game.PartyIndex(partnerStarter.pokemon);
		Game.TeamAddIndex(Game.Partner);

		// Postgame partner
		var postgameStarter = PokemonStarter.GetPostgamePartner(protagID, partnerID, protagGender);
		Game.PostgamePartner = postgameStarter;
	}

	public void SetPartnerGender(Gender gender)
	{
		Game.TeamGetPokemonAt(Game.Partner).gender = gender;
	}
}