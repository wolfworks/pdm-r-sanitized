﻿using System.Collections.Generic;
using UnityEngine;
using Pokemon.Lexic;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Species : DataElement, System.IComparable
	{
		[System.Serializable]
		public struct LevelMove
		{
			public byte level;
			[DataReference(typeof(Move))]
			public string move;
		}

		[System.Serializable]
		public struct Evolution
		{
			[DataReference(typeof(Species))]
			public string target;
			public EvolutionType type;
			public string parameterX;
			public string parameterY;
		}

		public int dexNumber;

		[DataReference(typeof(PokemonType))]
		public string[] types;
		public byte[] stats;

		public GenderRate genderRate;
		public GrowthRate growthRate;
		public int baseExp;
		public byte[] effortPoints;

		[DataReference(typeof(Ability))]
		public List<string> abilities;
		public int hiddenAbility;

		[DataReference(typeof(Move))]
		public List<LevelMove> moves;
		[DataReference(typeof(Move))]
		public List<string> eggMoves;
		[DataReference(typeof(Move))]
		public List<string> canAlsoLearn;
		[DataReference(typeof(Move))]
		public List<string> alternativeAnimation;

		public LangString kind;
		public LangString description;
		public float height;
		public float weight;
		public DexColor color;
		public DexBody bodyShape;

		public int stepsToHatch;
		public List<EggGroup> eggGroups;

		public List<Evolution> evolutions;

		[DataReference(typeof(Species))]
		public List<string> forms;
		[DataReference(typeof(Species))]
		public string parentForm;

		public GameObject maleModel;
		public GameObject femaleModel;
		public Sprite[] portraits;

		public Species()
		{
			hiddenAbility = -1;

			types = new string[2];
			stats = new byte[6];
			effortPoints = new byte[6];
			portraits = new Sprite[30];

			abilities = new List<string>();
			moves = new List<LevelMove>();
			eggMoves = new List<string>();
			canAlsoLearn = new List<string>();
			alternativeAnimation = new List<string>();
			eggGroups = new List<EggGroup>();
			evolutions = new List<Evolution>();
			forms = new List<string>();
		}

		public new int CompareTo(object o)
		{
			if (!(o is Species))
				return base.CompareTo(o);

			int dexComparison = dexNumber - ((Species)o).dexNumber;

			if (dexComparison != 0)
				return dexComparison;

			string formId = id.Substring(id.LastIndexOf('-') + 1);
			string otherFormId = ((Species)o).id.Substring(((Species)o).id.LastIndexOf('-') + 1);

			if (formId == id)
				return -1;

			if (otherFormId == ((Species)o).id)
				return 1;

			return int.Parse(formId) - int.Parse(otherFormId);
		}

		public bool IsOfType(PokemonType type)
		{
			return IsOfType(type.id);
		}

		public bool IsOfType(string type)
		{
			return (types[0] == type) || (types[1] == type);
		}
	}
}