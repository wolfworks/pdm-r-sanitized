﻿using UnityEngine;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Floor : DataElement
	{
		public LangString description;
		public bool alwaysActivate;
		public TextAsset effect;
		public GameObject model;
		public AnimationClip animation;
	}
}