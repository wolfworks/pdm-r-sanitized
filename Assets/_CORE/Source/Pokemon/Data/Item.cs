﻿using UnityEngine;
using Pokemon.Lexic;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Item : DataElement
	{
		public LangString plural;
		public LangString description;

		public ItemCategory category;

		public int price;
		public int sellPrice;

		public bool stackable;

		public TextAsset effect;

		public GameObject model;
		public AnimationClip animation;
	}
}