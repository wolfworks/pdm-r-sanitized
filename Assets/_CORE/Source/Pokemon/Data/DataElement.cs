﻿using System;

namespace Pokemon.Data
{
	[Serializable]
	public abstract class DataElement : IComparable, ICloneable
	{
		public string id;
		public LangString name;
		public string notes;
		
		public static implicit operator string(DataElement e)
		{
			if (e == null)
				return null;

			return e.id;
		}

		public static string FormatId(string candidate)
		{
			if (string.IsNullOrEmpty(candidate))
				return "UNNAMED";

			return candidate.ToUpper().Replace(" ", "").Replace("'", "").Replace(":", "");
		}

		public int CompareTo(object o)
		{
			if (!(o is DataElement))
				throw new InvalidOperationException("Cannot compare "+typeof(DataElement)+" to "+o.GetType());

			return id.CompareTo(((DataElement)o).id);
		}

		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}