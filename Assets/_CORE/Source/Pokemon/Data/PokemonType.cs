﻿using System.Collections.Generic;
using UnityEngine;

namespace Pokemon.Data
{
	[System.Serializable]
	public class PokemonType : DataElement
	{
		[DataReference(typeof(PokemonType))]
		public List<string> strongTo;
		[DataReference(typeof(PokemonType))]
		public List<string> weakTo;
		[DataReference(typeof(PokemonType))]
		public List<string> immunedTo;
		[DataReference(typeof(Status))]
		public List<string> resistedStatuses;

		public Sprite icon;
		public Sprite moveIndicator;

		public PokemonType()
		{
			strongTo = new List<string>();
			weakTo = new List<string>();
			immunedTo = new List<string>();
			resistedStatuses = new List<string>();
		}

		public int GetEffectivenessAgainst(string other)
		{
			if (strongTo.Contains(other))
				return 1;
			else if (weakTo.Contains(other))
				return -1;
			else if (immunedTo.Contains(other))
				return 2;
			else
				return 0;
		}
	}
}