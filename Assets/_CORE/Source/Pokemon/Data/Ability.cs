﻿using UnityEngine;
using Pokemon.Lexic;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Ability : DataElement
	{
		public LangString description;
		public TextAsset effect;
	}
}