﻿using UnityEngine;
using Pokemon.Lexic;
using System.Collections.Generic;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Status : DataElement
	{
		public LangString description;
		public StatusType type;
		public bool ailment;

		public TextAsset effect;

		[DataReference(typeof(Move))]
		public List<string> associatedMoves;

		public Status()
		{
			associatedMoves = new List<string>();
		}
	}
}