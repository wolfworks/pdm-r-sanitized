using UnityEngine;
using Pokemon.Lexic;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Move : DataElement
	{
		public LangString description;
		public bool isZMove;

		[DataReference(typeof(PokemonType))]
		public string type;
		public MoveType moveType;
		public ContestCondition contestCondition;

		public byte numberOfPP;
		public byte basePower;
		public byte accuracy;
		public sbyte priority;

		public MoveTargeting targeting;
		public MoveFlags flags;

		public float effectChance;
		public TextAsset effect;

		public byte zPower;
		public TextAsset zEffect;

		public AnimationClip animation;
	}
}