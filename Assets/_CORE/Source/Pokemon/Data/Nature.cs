using Pokemon.Lexic;

namespace Pokemon.Data
{
	[System.Serializable]
	public class Nature : DataElement
	{
		public Stat increase;
		public Stat decrease;

		public float GetModifier(Stat s)
		{
			if (increase == s) return 1.1f;
			if (decrease == s) return 0.9f;

			return 1f;
		}
	}
}