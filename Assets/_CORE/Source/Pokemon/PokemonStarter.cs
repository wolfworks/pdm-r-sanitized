﻿using Pokemon.Data;
using Pokemon.Lexic;
using System;
using System.Collections.Generic;

namespace Pokemon
{
	public class PokemonStarter
	{
		public static List<PokemonStarter> starters;

		public int id;
		public string identifier;
		public PokemonState pokemon;

		public static void InitializeStarters()
		{
			var database = Game.GetDatabase();
			var ivs = new byte[] { 27, 27, 27, 27, 27, 27 };

			starters = new List<PokemonStarter>()
			{
				new PokemonStarter
				{
					id = 0,
					identifier = "ESTJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("PHANPY"),
						name: GameText.Adj(0),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("ADAMANT"),
						a: database.Get<Ability>("SANDVEIL"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("GROWL"), database.Get<Move>("DEFENSECURL"), database.Get<Move>("ICESHARD") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 1,
					identifier = "ESTP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("SHINX"),
						name: GameText.Adj(1),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("HASTY"),
						a: database.Get<Ability>("INTIMIDATE"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("LEER"), database.Get<Move>("QUICKATTACK"), database.Get<Move>("FIREFANG") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 2,
					identifier = "ESFJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("ARON"),
						name: GameText.Adj(2),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("GENTLE"),
						a: database.Get<Ability>("ROCKHEAD"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("HARDEN"), database.Get<Move>("MUDSLAP"), database.Get<Move>("SCREECH") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 3,
					identifier = "ESFP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("ROCKRUFF"),
						name: GameText.Adj(3),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("JOLLY"),
						a: database.Get<Ability>("OWNTEMPO"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("LEER"), database.Get<Move>("SANDATTACK"), database.Get<Move>("THUNDERFANG") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 4,
					identifier = "ENTJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("SQUIRTLE"),
						name: GameText.Adj(4),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("HARDY"),
						a: database.Get<Ability>("TORRENT"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("TAILWHIP"), database.Get<Move>("WATERGUN"), database.Get<Move>("FAKEOUT") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 5,
					identifier = "ENTP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("HOUNDOUR"),
						name: GameText.Adj(5),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("SASSY"),
						a: database.Get<Ability>("EARLYBIRD"),
						m: new[] { database.Get<Move>("LEER"), database.Get<Move>("EMBER"), database.Get<Move>("HOWL"), database.Get<Move>("THUNDERFANG") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 6,
					identifier = "ENFJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("LITLEO"),
						name: GameText.Adj(6),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("BRAVE"),
						a: database.Get<Ability>("RIVALRY"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("LEER"), database.Get<Move>("EMBER"), database.Get<Move>("FLAMECHARGE") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 7,
					identifier = "ENFP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("ROWLET"),
						name: GameText.Adj(7),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("NAIVE"),
						a: database.Get<Ability>("OVERGROW"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("LEAFAGE"), database.Get<Move>("GROWL"), database.Get<Move>("CONFUSERAY") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 8,
					identifier = "ISTJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("LITTEN"),
						name: GameText.Adj(8),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("SERIOUS"),
						a: database.Get<Ability>("BLAZE"),
						m: new[] { database.Get<Move>("SCRATCH"), database.Get<Move>("EMBER"), database.Get<Move>("GROWL"), database.Get<Move>("FAKEOUT") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 9,
					identifier = "ISTP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("CHARMANDER"),
						name: GameText.Adj(9),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("BOLD"),
						a: database.Get<Ability>("BLAZE"),
						m: new[] { database.Get<Move>("SCRATCH"), database.Get<Move>("GROWL"), database.Get<Move>("METALCLAW"), database.Get<Move>("BITE") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 10,
					identifier = "ISFJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("POPPLIO"),
						name: GameText.Adj(10),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("MILD"),
						a: database.Get<Ability>("TORRENT"),
						m: new[] { database.Get<Move>("POUND"), database.Get<Move>("WATERGUN"), database.Get<Move>("GROWL"), database.Get<Move>("CHARM") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 11,
					identifier = "ISFP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("RIOLU"),
						name: GameText.Adj(11),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("IMPISH"),
						a: database.Get<Ability>("INNERFOCUS"),
						m: new[] { database.Get<Move>("FORESIGHT"), database.Get<Move>("QUICKATTACK"), database.Get<Move>("ENDURE"), database.Get<Move>("CIRCLETHROW") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 12,
					identifier = "INTJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("AXEW"),
						name: GameText.Adj(12),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("CAREFUL"),
						a: database.Get<Ability>("MOLDBREAKER"),
						m: new[] { database.Get<Move>("SCRATCH"), database.Get<Move>("LEER"), database.Get<Move>("FOCUSENERGY"), database.Get<Move>("ENDURE") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 13,
					identifier = "INTP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("PIKACHU"),
						name: GameText.Adj(13),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("MODEST"),
						a: database.Get<Ability>("STATIC"),
						m: new[] { database.Get<Move>("TAILWHIP"), database.Get<Move>("THUNDERSHOCK"), database.Get<Move>("GROWL"), database.Get<Move>("DISARMINGVOICE") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 14,
					identifier = "INFJ",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("FLETCHLING"),
						name: GameText.Adj(14),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("CALM"),
						a: database.Get<Ability>("BIGPECKS"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("GROWL"), database.Get<Move>("QUICKATTACK"), database.Get<Move>("AERIALACE") }
					)
					{ iv = ivs }
				},

				new PokemonStarter
				{
					id = 15,
					identifier = "INFP",
					pokemon = new PokemonState
					(
						p: database.Get<Species>("BULBASAUR"),
						name: GameText.Adj(15),
						l: Game.START_LEVEL,
						n: database.Get<Nature>("RELAXED"),
						a: database.Get<Ability>("OVERGROW"),
						m: new[] { database.Get<Move>("TACKLE"), database.Get<Move>("GROWL"), database.Get<Move>("MAGICALLEAF"), database.Get<Move>("SLUDGE") }
					)
					{ iv = ivs }
				}
			};
		}

		public static PokemonState GetPostgamePartner(int playerId, int partnerId, Gender playerGender)
		{
			var postgameId = Math.Abs(16 - (playerId + partnerId));
			var postgameState = starters.Find(x => x.id == postgameId).pokemon;

			postgameState.gender = playerGender == Gender.Male ? Gender.Female : Gender.Male;

			return postgameState;
		}
	}
}