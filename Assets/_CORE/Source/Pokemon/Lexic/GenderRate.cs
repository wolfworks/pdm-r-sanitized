﻿namespace Pokemon.Lexic
{
	public enum GenderRate
	{
		Genderless,
		Female_0,
		Female_125,
		Female_25,
		Female_50,
		Female_75,
		Female_875,
		Female_1
	}
}