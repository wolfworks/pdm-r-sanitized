﻿namespace Pokemon.Lexic
{
	public enum ItemUseType
	{
		Unusable,
		Consumable,
		InfiniteUses,
		InstantUse
	}
}