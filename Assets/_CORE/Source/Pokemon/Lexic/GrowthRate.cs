﻿namespace Pokemon.Lexic
{
	public enum GrowthRate
	{
		Fast,
		Medium,
		Slow,
		Parabolic,
		Erratic,
		Fluctuating
	}
}