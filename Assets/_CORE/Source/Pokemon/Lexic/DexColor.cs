﻿namespace Pokemon.Lexic
{
	public enum DexColor
	{
		Red,
		Blue,
		Yellow,
		Green,
		Black,
		Brown,
		Purple,
		Gray,
		White,
		Pink
	}
}