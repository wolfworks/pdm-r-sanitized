﻿namespace Pokemon.Lexic
{
	[System.Flags]
	public enum MoveFlags
	{
		None = 0,
		MakesContact = 1 << 0,
		AffectedByProtect = 1 << 1,
		AffectedByMagicCoat = 1 << 2,
		AffectedBySnatch = 1 << 3,
		AffectedByMirrorMove = 1 << 4,
		AffectedByKingsRock = 1 << 5,
		ThawsUser = 1 << 6,
		HighCritRatio = 1 << 7,
		IsBitingMove = 1 << 8,
		IsPunchingMove = 1 << 9,
		IsSoundMove = 1 << 10,
		IsPowderMove = 1 << 11,
		IsPulseMove = 1 << 12,
		IsBombMove = 1 << 13,
		IsDanceMove = 1 << 14,
		FailInSkyBattle = 1 << 15,
		TripleDistance = 1 << 16
	}
}