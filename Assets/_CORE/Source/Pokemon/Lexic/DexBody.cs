﻿namespace Pokemon.Lexic
{
	public enum DexBody
	{
		Bipedal,
		TailedBipedal,
		Quadrupedal,
		Insectoid,
		OnePairOfWings,
		TwoPairsOfWings,
		HeadOnly,
		HeadAndLegs,
		HeadAndArms,
		HeadAndBody,
		Serpent,
		Fins,
		Tentacles,
		MultipleBodies
	}
}