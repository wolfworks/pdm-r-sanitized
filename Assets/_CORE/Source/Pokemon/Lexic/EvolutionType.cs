﻿namespace Pokemon.Lexic
{
	public enum EvolutionType
	{
		Level,					// Get to level X
		LevelGender,			// Get to level X and be of gender Y
		LevelTime,				// Get to level X at time Y
		LevelTypeInTeam,		// Get to level X with a pokemon of type Y in your party
		LevelWeather,			// Get to level X during weather Y
		Item,					// Use item X
		HeldItemTrade,			// Hold item X during trade
		HeldItemGender,			// Hold item X and be of gender Y while leveling up
		HeldItemTime,			// Hold item X at time Y while leveling up
		Happiness,				// Happiness above X
		HappinessTime,			// Happiness above X at time Y
		AmieHasMoveType,		// X hearts at Poké Amie and has move of type Y
		SpeciesTrade,			// Trade with species X
		Trade,					// Trade
		HasMove,				// Has move X
		IsInTeam,				// X is in your party
		StatGreater,			// Stat X is greater than stat Y
		StatEquals,				// Stat X equals stat Y
		ContestCondition,		// Constest condition X is above Y
		Location,				// Be at location X while leveling up
		Double,					// Get to level X and also get pokémon Y if there's an empty spot
		Random					// Get to level X and evolve into either TARGET or Y
	}
}