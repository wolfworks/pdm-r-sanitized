﻿namespace Pokemon.Lexic
{
	public enum StatusType
	{
		Major,
		Sleep,
		Mobility,
		Volatile,
		Cursed,
		LeechSeed,
		Accuracy,
		Visibility,
		Muzzled,
		Exposed,
		PerishSong
	}
}