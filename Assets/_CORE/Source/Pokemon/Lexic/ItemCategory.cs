﻿namespace Pokemon.Lexic
{
	public enum ItemCategory
	{
		Regular,
		Berry,
		Seed,
		Projectile,
		Orb,
		TM,
		Money,
		MegaStone,
		ZCrystal
	}
}