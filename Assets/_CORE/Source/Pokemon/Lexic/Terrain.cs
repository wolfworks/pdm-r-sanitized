﻿namespace Pokemon.Lexic
{
	public enum Terrain
	{
		None,
		Electric,
		Grassy,
		Misty,
		Psychic
	}
}