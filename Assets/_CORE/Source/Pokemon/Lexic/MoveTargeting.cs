﻿namespace Pokemon.Lexic
{
	public enum MoveTargeting
	{
		TileInFront,
		TwoTilesFront,
		CutCorners,
		Line,
		Ring,
		Arc,
		CurrentTile,
		Self,
		Allies,
		AlliesAndSelf,
		RandomDirection,
		EntireRoom,
		EntireFloor,
		NoTarget
	}
}