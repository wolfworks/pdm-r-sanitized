﻿namespace Pokemon.Lexic
{
	public enum Stat
	{
		none = -1,
		hp,
		atk,
		def,
		spa,
		spd,
		spe
	}
}