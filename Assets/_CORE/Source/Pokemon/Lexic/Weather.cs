﻿namespace Pokemon.Lexic
{
	public enum Weather
	{
		Clear,
		Rain,
		Sunshine,
		Sandstorm,
		Hail,
		Snow,
		Fog,
		Cloudy
	}
}