﻿namespace Pokemon.Lexic
{
	public enum ContestCondition
	{
		Cool,
		Beautiful,
		Cute,
		Clever,
		Tough
	}
}