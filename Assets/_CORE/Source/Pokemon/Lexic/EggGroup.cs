﻿namespace Pokemon.Lexic
{
	public enum EggGroup
	{
		Monster,
		Water1,
		Water2,
		Water3,
		Bug,
		Flying,
		Field,
		Fairy,
		Grass,
		HumanLike,
		Mineral,
		Amorphous,
		Dragon,
		Ditto,
		Undiscovered
	}
}