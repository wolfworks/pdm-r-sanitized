﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

using Pokemon.Data;

namespace Pokemon
{
	[Serializable]
	public class ItemState
	{
		private string itemRef;

		[NonSerialized]
		private Item item;

		public int ammo;
		public bool sticky;

		public ItemState(Item i, int a = 0, bool s = false)
		{
			ItemType = i;
			ammo = a;
			sticky = s;
		}

		[OnDeserialized]
		internal void Deserialized()
		{
			var dataManager = Game.GetDatabase();
			item = dataManager.Get<Item>(itemRef);
		}

		public Item ItemType
		{
			get
			{
				return item;
			}

			set
			{
				item = value;
				itemRef = item.id;
			}
		}

		public string Name
		{
			get
			{
				if (!item.stackable) return item.name.CorrectTranslation(Game.Lang);
				
				return ammo+" "+(ammo > 1 ? item.plural.CorrectTranslation(Game.Lang) : item.name.CorrectTranslation(Game.Lang));
			}
		}

		public int CompareTo(object o)
		{
			if (o == null) return 1;

			ItemState other = o as ItemState;
			if (other == null) throw new ArgumentException("Object is not an ItemState");

			int nameComparison = string.Compare(item.id, other.item.id);

			if (nameComparison == 0) return ammo - other.ammo;
			else return nameComparison;
		}
	}
}