﻿using UnityEngine;
using Pokemon.Data;
using Pokemon.Lexic;
using System;

namespace Pokemon
{
	public class Formula
	{
		private readonly PokemonState subject;

		public Formula(PokemonState s)
		{
			subject = s;
		}

		public int HP()
		{
			if (subject.Species.id == "SHEDNINJA") return 1;

			Species p = subject.Species;
			return ((2 * p.stats[(int)Stat.hp] + subject.iv[(int)Stat.hp] + subject.ev[(int)Stat.hp] / 4) * subject.Level) / 100 + subject.Level + 10;
		}

		public int Statistic(Stat s)
		{
			Species p = subject.Species;

			return (int)Mathf.Round((((2 * p.stats[(int)s] + subject.iv[(int)s] + subject.ev[(int)s] / 4) * subject.Level) / 100 + 5) * subject.Nature.GetModifier(s));
		}

		public int ExpAtLevel(byte l)
		{
			switch (subject.Species.growthRate)
			{
				case GrowthRate.Fast:
					return (int)Mathf.Round(0.8f * (l * l * l));

				case GrowthRate.Medium:
					return l * l * l;

				case GrowthRate.Slow:
					return (int)Mathf.Round(1.25f * (l * l * l));

				case GrowthRate.Parabolic:
					return (int)Mathf.Round(1.2f * (l * l * l) - 15f * (l * l) + 100f * l - 140);

				case GrowthRate.Erratic:
					if (l <= 50) return (int)Mathf.Round((l * l * l) * ((100f - l) / 50f));
					else if (l <= 68) return (int)Mathf.Round((l * l * l) * ((150f - l) / 100f));
					else if (l <= 98)
					{
						float p = 0f;
						switch (l % 3)
						{
							case 0: p = 0f; break;
							case 1: p = 0.008f; break;
							case 2: p = 0.014f; break;
						}

						return (int)Mathf.Round((l * l * l) * (1.274f - 0.02f * Mathf.Floor(l / 3f) - p));
					}
					else if (l <= 100) return (int)Mathf.Round((l * l * l) * ((160f - l) / 100f));
					break;

				case GrowthRate.Fluctuating:
					int n3 = l * l * l;
					if (l <= 15) return (int)Mathf.Round(n3 * ((24f + Mathf.Floor((l + 1f) / 3f)) / 50f));
					else if (l <= 35) return (int)Mathf.Round(n3 * ((14f + l) / 50f));
					else if (l <= 100) return (int)Mathf.Round(n3 * ((32f + Mathf.Floor(l / 2f)) / 50f));
					break;
			}

			return 0;
		}

		public float TypeEffectiveness(PokemonState target, Move move)
		{
			int firstTypeEffectiveness = target.Species.types[0].As<PokemonType>().GetEffectivenessAgainst(move.type);
			int secondTypeEffectiveness = target.Species.types[1].As<PokemonType>().GetEffectivenessAgainst(move.type);

			int finalEffectiveness;

			if (firstTypeEffectiveness == 2)
				finalEffectiveness = secondTypeEffectiveness == -1 ? 1 : 2;
			else if (secondTypeEffectiveness == 2)
				finalEffectiveness = firstTypeEffectiveness == -1 ? 1 : 2;
			else
				finalEffectiveness = Mathf.Clamp(firstTypeEffectiveness + secondTypeEffectiveness, -1, 1);

			switch (finalEffectiveness)
			{
				case -1: return 1.7f;
				case 0: return 1f;
				case 1: return 0.5f;
				case 2: return 0.25f;

				default: return 1f;
			}
		}

		public float InitialDamage(PokemonState target, Move move, bool willCrit)
		{
			float bp = move.basePower;
			float a, d;

			Stat aStat = move.moveType == MoveType.Physical ? Stat.atk : Stat.spa;
			Stat dStat = move.moveType == MoveType.Physical ? Stat.def : Stat.spd;

			if (willCrit)
			{
				a = subject.GetPositiveStat(aStat);
				d = target.GetNegativeStat(dStat);
			}
			else
			{
				a = subject.GetEffectiveStat(aStat);
				d = target.GetEffectiveStat(dStat);
			}

			float res = (((2 * subject.Level / 5) + 2) * bp * (a / d) / 50) + 2;

			return res;
		}

		public int NormalAttackDamage(PokemonState target)
		{
			int atk = subject.GetStat(Stat.atk);
			int spa = subject.GetStat(Stat.spa);

			float a, d;

			if (atk > spa)
			{
				a = subject.GetEffectiveStat(Stat.atk);
				d = target.GetEffectiveStat(Stat.def);
			}
			else
			{
				a = subject.GetEffectiveStat(Stat.spa);
				d = target.GetEffectiveStat(Stat.spd);
			}

			float res = (((2 * subject.Level / 5) + 2) * 20 * (a / d) / 50) + 2;

			return (int)Mathf.Floor(res);
		}

		public float EffectiveAccuracy(PokemonState target, float moveAccuracy)
		{
			float accuracy, evasion;

			if (subject.accuracy < 0)
				accuracy = 1f / (Mathf.Abs(subject.accuracy) + 1f);
			else if (subject.accuracy > 0)
				accuracy = subject.accuracy * 1.5f;
			else
				accuracy = 1f;

			if (target.evasion < 0)
				evasion = Mathf.Abs(subject.evasion) * 1.5f;
			else if (subject.accuracy > 0)
				evasion = 1f / (subject.accuracy + 1f);
			else
				evasion = 1f;

			return moveAccuracy * accuracy * evasion / 100f;
		}

		#region Private Methods
		private Formula As(PokemonState target)
		{
			return new Formula(target);
		}
		#endregion
	}
}