﻿using Pokemon.Data;
using System;
using System.Runtime.Serialization;

namespace Pokemon
{
	[Serializable]
	public class StatusCondition
	{
		private string statusRef;
  
		[NonSerialized]
		private Status status;

		public int remainingTurns;
		public int lastedFor;

		[OnDeserialized]
		internal void Deserialized()
		{
			var dataManager = Game.GetDatabase();
			status = dataManager.Get<Status>(statusRef);
		}

		public Status StatusType
		{
			get
			{
				return status;
			}

			set
			{
				status = value;
				statusRef = status.id;
			}
		}
	}
}