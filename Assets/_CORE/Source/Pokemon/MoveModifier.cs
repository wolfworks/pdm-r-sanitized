﻿using Pokemon.Data;
using Pokemon.Lexic;
using UnityEngine;

namespace Pokemon
{
	public class MoveModifier
	{
		public BattleManager manager;

		public float effectivePower;

		public float targetDampening;
		public float weather;
		public float critical;
		public float random;
		public float stab;
		public float typeEffectiveness;
		public float burnStatus;
		public float supplemental;

		public MoveModifier(BattleManager mng, PokemonState user, PokemonState target, int numberOfTargets, Move move)
		{
			manager = mng;
		
			targetDampening = numberOfTargets > 1 ? 0.75f : 1f;

			if (manager.weather == Weather.Rain)
			{
				if (move.type == "FIRE") weather = 0.5f;
				else if (move.type == "WATER") weather = 1.5f;
				else weather = 1f;
			}
			else if (manager.weather == Weather.Sunshine)
			{
				if (move.type == "FIRE") weather = 1.5f;
				else if (move.type == "WATER") weather = 0.5f;
				else weather = 1f;
			}
			else if (manager.weather == Weather.Fog)
			{
				if (move.type == "ELECTRIC") weather = 0.5f;
				else weather = 1f;
			}
			else if (manager.weather == Weather.Cloudy)
			{
				if (move.type != "NORMAL") weather = 0.25f;
				else weather = 1f;
			}
			else
			{
				weather = 1f;
			}

			critical = 1f;
			random = Random.Range(0.85f, 1f);
			stab = user.Species.IsOfType(move.type) ? 1.5f : 1f;
			typeEffectiveness = manager.Formula.TypeEffectiveness(target, move);
			burnStatus = user.HasStatus("BURN") && move.moveType == MoveType.Physical ? 0.5f : 1f;
			supplemental = 1f;
		}

		public void Calculate()
		{
			effectivePower =
				targetDampening
				* weather
				* critical
				* random
				* stab
				* typeEffectiveness
				* burnStatus
				* supplemental;
		}
	}
}