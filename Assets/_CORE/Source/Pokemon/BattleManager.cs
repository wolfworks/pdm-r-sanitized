﻿using Dungeon;
using Dungeon.Entity;
using Pokemon.Data;
using Pokemon.Lexic;
using System.Collections.Generic;
using UnityEngine;

using Terrain = Pokemon.Lexic.Terrain;

namespace Pokemon
{
	public class BattleManager
	{
		public Weather weather;
		public Terrain terrain;

		public Formula Formula { get; private set; }
		private LuaScript Script { get; set; }

		public void SetScript(LuaScript scr)
		{
			Script = scr;
		}

		public DealtDamage NormalAttack(DungeonCharacter user, DungeonCharacter target)
		{
			Formula = new Formula(user.pokemon);

			return new DealtDamage
			{
				amount = Formula.NormalAttackDamage(target.pokemon),
				isHealing = false,
				isFailed = false,
				isCritical = false,
				effectiveness = 0
			};
		}

		public List<DealtDamage> Move(DungeonCharacter user, List<DungeonCharacter> targets, Move move)
		{
			Formula = new Formula(user.pokemon);
		
			var result = new List<DealtDamage>();
			bool effectHit = Script != null && (move.effectChance == 0 || Random.value < move.effectChance);

			foreach (var target in targets)
			{
				if (effectHit) Script.Emit("onMoveUse", user, target);

				bool successfulHit = move.accuracy == 0 || Random.value < Formula.EffectiveAccuracy(target.pokemon, move.accuracy);

				if (!successfulHit)
				{
					Script.Emit("onMoveFail", user, target);

					result.Add(new DealtDamage
					{
						amount = 0,
						isHealing = false,
						isFailed = true,
						isCritical = false,
						effectiveness = 0
					});

					continue;
				}

				if (move.moveType != MoveType.Status)
				{
					var modifier = new MoveModifier(this, user.pokemon, target.pokemon, targets.Count, move);

					// Critical Hit Test
					int critRatio;

					if (effectHit) critRatio = Script.Emit<int>("onCriticalRatio") ?? 0;
					else critRatio = 0;

					foreach (var s in user.pokemon.statuses)
						critRatio = (Script?.With(s.StatusType.effect)?.Emit<int>("onMoveCritCalculation", critRatio)) ?? critRatio;

					bool willCrit = false;
					if (critRatio >= 3) willCrit = true;
					else if (critRatio == 2) willCrit = Random.value < 1f / 2f;
					else if (critRatio == 1) willCrit = Random.value < 1f / 8f;
					else willCrit = Random.value < 1f / 24f;

					// Initial Damage Calculation
					float initialDamage = Formula.InitialDamage(target.pokemon, move, willCrit);

					if (effectHit)
						initialDamage = Script.Emit<float>("onInitialDamage", initialDamage) ?? initialDamage;

					// Move Modification
					if (willCrit) modifier.critical = 1.5f;

					if (effectHit)
						Script.Emit("onMoveModify", modifier);

					modifier.Calculate();

					// Final Damage Calculation
					float finalDamage = initialDamage * modifier.effectivePower;

					if (effectHit)
						finalDamage = Script.Emit<float>("onFinalDamage", finalDamage) ?? finalDamage;

					if (finalDamage < 1f) finalDamage = 1f;

					var moveEffectiveness = 0;

					if (modifier.typeEffectiveness > 1f)
						moveEffectiveness = 1;
					else if (modifier.typeEffectiveness < 1f)
						moveEffectiveness = -1;

					result.Add(new DealtDamage
					{
						amount = (int)Mathf.Floor(finalDamage),
						isHealing = false,
						isFailed = false,
						isCritical = willCrit,
						effectiveness = moveEffectiveness
					});
				}
			}

			return result;
		}
	}
}