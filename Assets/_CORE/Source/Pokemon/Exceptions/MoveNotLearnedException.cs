﻿using System;

namespace Pokemon.Exceptions
{
	public class MoveNotLearnedException : Exception
	{
		public MoveNotLearnedException() {}
		public MoveNotLearnedException(string message) : base(message) {}
		public MoveNotLearnedException(string message, Exception inner) : base(message, inner) {}
	}
}