﻿using System;

namespace Pokemon.Exceptions
{
	public class NotDataTypeException : Exception
	{
		public NotDataTypeException() {}
		public NotDataTypeException(string message) : base(message) {}
		public NotDataTypeException(string message, Exception inner) : base(message, inner) {}
	}
}