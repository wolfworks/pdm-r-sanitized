﻿using System;

namespace Pokemon.Exceptions
{
	public class InvalidDataReferenceException : Exception
	{
		public InvalidDataReferenceException() {}
		public InvalidDataReferenceException(string message) : base(message) {}
		public InvalidDataReferenceException(string message, Exception inner) : base(message, inner) {}
	}
}