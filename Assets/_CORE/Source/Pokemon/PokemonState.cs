﻿using Pokemon.Data;
using Pokemon.Exceptions;
using Pokemon.Lexic;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using Random = System.Random;

namespace Pokemon
{
	[Serializable]
	public class PokemonState
	{
		private string speciesRef;

		[NonSerialized]
		private Species species;

		public string nickname;
		public Gender gender;

		private string natureRef;

		[NonSerialized]
		private Nature nature;

		private string abilityRef;

		[NonSerialized]
		private Ability ability;

		private int exp;
		private byte level;

		public byte[] iv;
		public byte[] ev;

		public byte[] statBoosts;
		public byte accuracy;
		public byte evasion;
		public List<StatusCondition> statuses;

		public ItemState heldItem;

		private string[] movesRef;

		[NonSerialized]
		private Move[] moves;

		public bool[] allowedMoves;

		private float currentHP;
		private int currentHunger;
		private int[] currentPP;

		public PokemonState()
		{
			iv = new byte[6];
			ev = new byte[6];
			statBoosts = new byte[] { 1, 1, 1, 1, 1, 1 };

			statuses = new List<StatusCondition>();

			movesRef = new string[4];
			moves = new Move[4];
			allowedMoves = new[] { true, true, true, true };
			currentPP = new int[4];
		}

		public PokemonState(Species p, byte l, string name = null, Gender? g = null, Nature n = null, Ability a = null, Move[] m = null)
		{
			var database = Game.GetDatabase();
			var r = new Random();

			Species = p;
			level = l;
			exp = new Formula(this).ExpAtLevel(l);

			ev = new byte[] { 0, 0, 0, 0, 0, 0 };
			iv = new byte[] { (byte)r.Next(0, 32), (byte)r.Next(0, 32), (byte)r.Next(0, 32), (byte)r.Next(0, 32), (byte)r.Next(0, 32), (byte)r.Next(0, 32) };

			statBoosts = new byte[] { 1, 1, 1, 1, 1, 1 };
			accuracy = 0;
			evasion = 0;

			statuses = new List<StatusCondition>();

			nickname = string.IsNullOrEmpty(name) ? Species.name.CorrectTranslation(Game.Lang) : name;

			if (Species.genderRate == GenderRate.Genderless)
				gender = Gender.None;
			else
				gender = (g == null ? (Gender)r.Next(0, 2) : (Gender)g);

			if (n != null)
			{
				Nature = n;
			}
			else
			{
				int random = r.Next(0, database.Count<Nature>());
				int i = 0;

				foreach (Nature element in database.Iterate<Nature>())
				{
					if (i == random)
					{
						Nature = element;
						break;
					}

					++i;
				}
			}

			if (n != null)
			{
				Ability = a;
			}
			else
			{
				int random = r.Next(0, Species.abilities.Count);
				Ability = Species.abilities[random].As<Ability>();
			}

			currentHP = (new Formula(this)).HP();
			currentHunger = 100;

			movesRef = new string[4];
			moves = new Move[4];
			allowedMoves = new[] { true, true, true, true };

			if (m != null)
			{
				for (int i = 0; i < m.Length; ++i)
					Moves(i, m[i]);
			}
			else
			{
				int i = 0;

				foreach (var e in Species.moves)
				{
					if (e.level > level) break;

					Moves(i, database.Get<Move>(e.move));
					i = (i == 3 ? 0 : i + 1);
				}
			}

			currentPP = new int[4];
			for (int i = 0; i < 4; ++i)
				currentPP[i] = (Moves(i) == null ? 0 : Moves(i).numberOfPP);
		}

		[OnDeserialized]
		internal void Deserialized()
		{
			var dataManager = Game.GetDatabase();
			
			species = dataManager.Get<Species>(speciesRef);
			nature = dataManager.Get<Nature>(natureRef);
			ability = dataManager.Get<Ability>(abilityRef);

			moves = new Move[4];
			for (int i = 0; i < moves.Length; ++i)
			{
				if (!string.IsNullOrEmpty(movesRef[i]))
					moves[i] = dataManager.Get<Move>(movesRef[i]);
			}
		}

		public Species Species
		{
			get
			{
				return species;
			}

			set
			{
				species = value;
				speciesRef = species.id;
			}
		}

		public Nature Nature
		{
			get
			{
				return nature;
			}

			set
			{
				nature = value;
				natureRef = nature.id;
			}
		}

		public Ability Ability
		{
			get
			{
				return ability;
			}

			set
			{
				ability = value;
				abilityRef = ability.id;
			}
		}

		public Move Moves(int index)
		{
			return moves[index];
		}

		public void Moves(int index, Move newMove)
		{
			moves[index] = newMove;
			movesRef[index] = newMove.id;
		}

		public GameObject Model
		{
			get => gender == Gender.Female && Species.femaleModel != null
					? Species.femaleModel
					: Species.maleModel;
		}

		public float HP
		{
			get => currentHP;
			set => currentHP = Mathf.Clamp(value, 0f, GetStat(Stat.hp));
		}

		public int Hunger
		{
			get => currentHunger;
			set => currentHunger = Mathf.Clamp(value, 0, 255);
		}

		public int[] PP => currentPP;

		public byte Level
		{
			get => level;
			set
			{
				level = (byte)Mathf.Clamp(value, 1, 100);
				exp = new Formula(this).ExpAtLevel(value);
			}
		}

		public int Exp
		{
			get => exp;
			set
			{
				exp = value;
				var f = new Formula(this);

				for (; value > f.ExpAtLevel(level); level++) { }
			}
		}

		public void PPRestore()
		{
			for (int i = 0; i < 4; ++i)
				currentPP[i] = Moves(i).numberOfPP;
		}

		public void PPConsume(Move m, int v = 1)
		{
			for (int i = 0; i < 4; ++i)
			{
				if (Moves(i).id == m.id)
				{
					currentPP[i] -= v;
					currentPP[i] = Mathf.Clamp(currentPP[i], 0, Moves(i).numberOfPP);
					return;
				}
			}

			throw new MoveNotLearnedException(Species.id + " does not know " + m.id + ".");
		}

		public int GetStat(Stat stat)
		{
			var f = new Formula(this);

			if (stat == Stat.hp) return f.HP();
			else return f.Statistic(stat);
		}

		public int GetEffectiveStat(Stat stat)
		{
			int baseStat = GetStat(stat);
			return (int)Mathf.Floor(baseStat * statBoosts[(int)stat]);
		}

		public int GetPositiveStat(Stat stat)
		{
			int baseStat = GetStat(stat);
			return (int)Mathf.Floor(baseStat * Mathf.Clamp(statBoosts[(int)stat], 1, Mathf.Infinity));
		}

		public int GetNegativeStat(Stat stat)
		{
			int baseStat = GetStat(stat);
			return (int)Mathf.Floor(baseStat * Mathf.Clamp01(statBoosts[(int)stat]));
		}

		public void ClearStatBoosts()
		{
			statBoosts = new byte[] { 1, 1, 1, 1, 1, 1 };
		}

		public bool HasStatus(string status)
		{
			foreach (var s in statuses)
			{
				if (s.StatusType.id == status)
					return true;
			}

			return false;
		}
	}
}