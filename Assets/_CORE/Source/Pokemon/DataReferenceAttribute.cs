﻿using System;
using UnityEngine;
using Pokemon.Data;
using Pokemon.Exceptions;

namespace Pokemon
{
	public class DataReferenceAttribute : PropertyAttribute
	{
		public Type referenceType;

		public DataReferenceAttribute(Type targetType)
		{
			if (!targetType.IsSubclassOf(typeof(DataElement)))
				throw new NotDataTypeException(targetType+" is not a valid DataElement.");

			referenceType = targetType;
		}
	}
}