﻿using UnityEngine;

namespace Pokemon
{
	[System.Serializable]
	public struct LangString
	{
		public string EN;
		public string FR;
		public string GE;
		public string JP;

		public string Default()
		{
			if (!string.IsNullOrEmpty(EN)) return EN;
			if (!string.IsNullOrEmpty(FR)) return FR;
			if (!string.IsNullOrEmpty(GE)) return GE;
			if (!string.IsNullOrEmpty(JP)) return JP;

			return string.Empty;
		}

		public string CorrectTranslation()
		{
			return CorrectTranslation(Application.systemLanguage);
		}

		public string CorrectTranslation(SystemLanguage language)
		{
			switch (language)
			{
				case SystemLanguage.English: return EN;
				case SystemLanguage.French: return FR;
				case SystemLanguage.German: return GE;
				case SystemLanguage.Japanese: return JP;
				default: return Default();
			}
		}

		public string CorrectTranslation(string language)
		{
			switch (language)
			{
				case "EN": return EN;
				case "FR": return FR;
				case "GE": return GE;
				case "JP": return JP;
				default: return Default();
			}
		}
	}
}