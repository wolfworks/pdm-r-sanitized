﻿public struct DealtDamage
{
	public int amount;
	public bool isHealing;
	public bool isFailed;
	public bool isCritical;
	public int effectiveness;
}