﻿using System;
using UnityEngine;

public enum EmotionEffectType
{
	Surprise,
	Question,
	Exclamation,
	Sweat,
	Hurry,
	Heckle,
	Anger,
	Shout,
	Laugh
}

[Serializable]
public struct EmotionEffect
{
	public GameObject effect;
	public bool loopable;
}

[CreateAssetMenu(fileName = "EmotionEffects", menuName = "Game/Emotion effects", order = 2)]
public class EmotionEffects : ScriptableObject
{
	public EmotionEffect[] effects;

	public EmotionEffect this[EmotionEffectType id] => effects[(int)id];
}