﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pokemon;
using Pokemon.Data;
using Pokemon.Exceptions;

using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "PokemonDatabase", menuName = "PDP/Pokemon Database")]
public class PokemonDatabase : ScriptableObject
{
	[SerializeField]
	private List<PokemonType> typeList = new List<PokemonType>();
	[SerializeField]
	private List<Nature> natureList = new List<Nature>();
	[SerializeField]
	private List<Species> speciesList = new List<Species>();
	[SerializeField]
	private List<Move> moveList = new List<Move>();
	[SerializeField]
	private List<Ability> abilityList = new List<Ability>();
	[SerializeField]
	private List<Status> statusList = new List<Status>();
	[SerializeField]
	private List<Item> itemList = new List<Item>();
	[SerializeField]
	private List<Floor> floorList = new List<Floor>();

	private IList GetList(Type t)
	{
		if (t == typeof(PokemonType)) return typeList;
		if (t == typeof(Nature)) return natureList;
		if (t == typeof(Species)) return speciesList;
		if (t == typeof(Move)) return moveList;
		if (t == typeof(Ability)) return abilityList;
		if (t == typeof(Status)) return statusList;
		if (t == typeof(Item)) return itemList;
		if (t == typeof(Floor)) return floorList;

		throw new DatabaseException("Type '" + t + "' is unsupported.");
	}

	private IEnumerable<DataElement> IterateOnAllLists()
	{
		foreach (DataElement e in typeList)
			yield return e;

		foreach (DataElement e in natureList)
			yield return e;

		foreach (DataElement e in speciesList)
			yield return e;

		foreach (DataElement e in moveList)
			yield return e;

		foreach (DataElement e in abilityList)
			yield return e;

		foreach (DataElement e in statusList)
			yield return e;

		foreach (DataElement e in itemList)
			yield return e;

		foreach (DataElement e in floorList)
			yield return e;
	}

	private void RemoveDuplicates(IList list)
	{
		List<string> alreadyMet = new List<string>();

		for (int i = 0; i < list.Count; ++i)
		{
			if (alreadyMet.Contains(((DataElement)list[i]).id))
			{
				Debug.LogWarning("Pokémon Database: Entry '" + ((DataElement)list[i]).id + "' is a duplicate; it will be deleted.");
				list.RemoveAt(i);
			}
			else
			{
				alreadyMet.Add(((DataElement)list[i]).id);
			}
		}
	}

	private void UpdateReferences(IList modifiedList)
	{
		/*foreach (DataElement e in IterateOnAllLists())
		{
			foreach (var property in e.GetType().GetFields())
			{
				if (Attribute.IsDefined(property, typeof(DataReferenceAttribute)))
				{
					var reference = (DataReferenceAttribute)property.GetCustomAttributes(typeof(DataReferenceAttribute), true)[0];

					if (property.FieldType == typeof(List<string>))
					{
						List<string> target = (List<string>)property.GetValue(e);
						for (int i = 0 ; i < target.Count ; ++i)
						{
							if (!Contains(reference.referenceType, target[i]))
								target.RemoveAt(i);
						}
					}
					else
					{
						string val = (property.GetValue(e) as string);

						if (!Contains(reference.referenceType, val))
							property.SetValue(e, string.Empty);
					}
				}
			}
		}*/
	}

	public bool IsEmpty(Type t)
	{
		return GetList(t).Count == 0;
	}

	public void Add(DataElement e)
	{
		var list = GetList(e.GetType());
		list.Add(e);
		RemoveDuplicates(list);
	}

	public void Remove(DataElement e)
	{
		var list = GetList(e.GetType());
		list.Remove(e);
		UpdateReferences(list);
	}

	public DataElement Get(Type t, string id)
	{
		foreach (DataElement e in GetList(t))
		{
			if (e == id)
				return e;
		}

		throw new DataNotFoundException("Couldn't find entry '" + id + "' of type '" + t + "'' in the database.");
	}

	public T Get<T>(string id) where T : DataElement
	{
		foreach (DataElement e in GetList(typeof(T)))
		{
			if (e == id)
				return (T)e;
		}

		throw new DataNotFoundException("Couldn't find entry '" + id + "' of type '" + typeof(T) + "' in the database.");
	}

	public DataElement GetFirst(Type t)
	{
		var list = GetList(t);

		if (list.Count == 0)
			return null;

		return (DataElement)GetList(t)[0];
	}

	public T GetFirst<T>() where T : DataElement
	{
		var list = GetList(typeof(T));

		if (list.Count == 0)
			return null;

		return (T)GetList(typeof(T))[0];
	}

	public DataElement GetRandom(Type t)
	{
		var list = GetList(t);
		return (DataElement)list[Random.Range(0, list.Count)];
	}

	public T GetRandom<T>() where T : DataElement
	{
		var list = GetList(typeof(T));
		return (T)list[Random.Range(0, list.Count)];
	}

	public bool Contains(Type t, string id)
	{
		foreach (DataElement e in GetList(t))
		{
			if (e == id)
				return true;
		}

		return false;
	}

	public bool Contains<T>(string id) where T : DataElement
	{
		foreach (DataElement e in GetList(typeof(T)))
		{
			if (e == id)
				return true;
		}

		return false;
	}

	public int Count<T>() where T : DataElement
	{
		return GetList(typeof(T)).Count;
	}

	public void Sort(Type t)
	{
		if (t == typeof(PokemonType)) typeList.Sort();
		else if (t == typeof(Nature)) natureList.Sort();
		else if (t == typeof(Species)) speciesList.Sort();
		else if (t == typeof(Move)) moveList.Sort();
		else if (t == typeof(Ability)) abilityList.Sort();
		else if (t == typeof(Status)) statusList.Sort();
		else if (t == typeof(Item)) itemList.Sort();
		else if (t == typeof(Floor)) floorList.Sort();
		else throw new DatabaseException("Type '" + t + "' is unsupported.");
	}

	public IEnumerable<DataElement> Iterate(Type t)
	{
		foreach (DataElement e in GetList(t))
			yield return e;
	}

	public IEnumerable<T> Iterate<T>() where T : DataElement
	{
		foreach (DataElement e in GetList(typeof(T)))
			yield return (T)e;
	}
}