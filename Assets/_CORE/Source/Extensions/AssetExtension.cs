﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class AssetExtension
{
	public static string[] GetAtPath(string path)
	{
		List<string> al = new List<string>();
		string[] fileEntries = Directory.GetFiles(Application.dataPath + "/" + path);
 
		foreach (string fileName in fileEntries)
		{
			if (fileName.EndsWith(".meta")) continue;

			int assetPathIndex = fileName.IndexOf("Assets");
			string localPath = fileName.Substring(assetPathIndex);
			
			al.Add(localPath);
		}

		return al.ToArray();
	}
}