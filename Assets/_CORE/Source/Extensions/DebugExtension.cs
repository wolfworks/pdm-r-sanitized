﻿using UnityEngine;

public static class DebugExtension
{
	public static void DrawRect(Rect r, Color color, float duration = 0.0f, bool depthTest = true)
	{
		Debug.DrawLine(new Vector3(r.x, 0f, r.y), new Vector3(r.xMax-1, 0f, r.y), color, duration, depthTest);
		Debug.DrawLine(new Vector3(r.xMax-1, 0f, r.y), new Vector3(r.xMax-1, 0f, r.yMax-1), color, duration, depthTest);
		Debug.DrawLine(new Vector3(r.xMax-1, 0f, r.yMax-1), new Vector3(r.x, 0f, r.yMax-1), color, duration, depthTest);
		Debug.DrawLine(new Vector3(r.x, 0f, r.yMax-1), new Vector3(r.x, 0f, r.y), color, duration, depthTest);
	}
}