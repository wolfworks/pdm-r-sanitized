﻿using UnityEngine;

public static class Vector3Extension
{
	public static float GreatCircleDistance(Vector3 from, Vector3 to, float radius = 1f)
	{
		return Mathf.Acos(Vector3.Dot(from.normalized, to.normalized)) * radius;
	}

	public static float GreatCircleDistance(Vector3 from, Vector3 to, Vector3 center, float radius = 1f)
	{
		return GreatCircleDistance(from - center, to - center, radius);
	}

	public static Vector2 ToTopviewVector2(this Vector3 source)
	{
		return new Vector2(source.x, source.z);
	}
}