﻿using System;
using UnityEngine;

public static class TransformExtension
{
	public static Transform FindChildRecursive(this Transform root, Func<Transform, bool> predicate)
	{
		foreach (Transform t in root)
		{
			if (predicate(t))
			{
				return t;
			}
			else if (t.childCount > 0)
			{
				Transform child = FindChildRecursive(t, predicate);

				if (child) return child;
			}
		}

		return null;
	}

	public static void SetLayerRecursively(this Transform trans, int layer)
	{
		trans.gameObject.layer = layer;
		foreach (Transform child in trans)
		{
			child.SetLayerRecursively(layer);
		}
	}
}