﻿using Delaunay.Geo;
using System.Collections.Generic;
using UnityEngine;

public static class LineSegmentExtension
{
	public static float Length(this LineSegment source)
	{
		return Vector2.Distance(source.p0.Value, source.p1.Value);
	}

	public static bool Intersects(this LineSegment source, LineSegment dest)
	{
		return source.Intersects(dest, out _);
	}

	public static bool Intersects(this LineSegment source, LineSegment dest, out Vector2 intersectionPoint)
	{
		intersectionPoint = default;

		float cross(Vector2 v, Vector2 w)
		{
			return v.x * w.y - v.y * w.x;
		}

		var p = source.p0.Value;
		var q = dest.p0.Value;
		var r = source.p1.Value - source.p0.Value;
		var s = dest.p1.Value - dest.p0.Value;

		var t = cross(q - p, s) / cross(r, s);
		var u = cross(q - p, r) / cross(r, s);

		if (cross(r, s) != 0 && t >= 0 && t <= 1f && u >= 0 && u <= 1f)
		{
			intersectionPoint = p + t * r;
			return true;
		}

		return false;
	}

	public static bool ContainsPoint(this LineSegment source, Vector2 point, float precision = 0f)
	{
		var distanceSum = Vector2.Distance(source.p0.Value, point) + Vector2.Distance(source.p1.Value, point);
		var length = source.Length();

		return distanceSum > (length - precision) && distanceSum < (length + precision);
	}

	public static IEnumerable<Vector2> Split(this LineSegment source, int verticesCount)
	{
		var currentPoint = source.p0.Value;
		var segmentsCount = verticesCount + 1;

		while (segmentsCount > 1)
		{
			currentPoint = Vector2.Lerp(currentPoint, source.p1.Value, 1f / segmentsCount);
			--segmentsCount;

			yield return currentPoint;
		}
	}
}