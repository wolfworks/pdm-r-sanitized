﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using SimpleJSON;
using Pokemon;
using Pokemon.Lexic;

public struct Mes
{
	public string message;
	public string displayName;
	public string[] choices;
}

public static class GameText
{
	private static Dictionary<string,JSONNode> files;

	public static bool Initialize()
	{
		try
		{
			files = new Dictionary<string,JSONNode>();
			TextAsset[] arr = Resources.LoadAll<TextAsset>("Text/"+Game.Lang);

			foreach (TextAsset ta in arr)
				files.Add(ta.name, JSON.Parse(ta.text));

			return true;
		}
		catch (Exception e)
		{
			Debug.Log(e);
			return false;
		}
	}

	public static string GetString(string reference)
	{
		if (files == null) throw new Exception("GameText has not been initialized yet");

		string[] exploded = reference.Split('/');
		
		return files[exploded[0]][exploded[1]].Value;
	}

	public static Mes GetEntry(string reference)
	{
		if (files == null) throw new Exception("GameText has not been initialized yet");

		string[] exploded = reference.Split('/');
		JSONNode o = files[exploded[0]][exploded[1]];

		Mes res = new Mes();
		res.message = o["message"].Value;

		if (o["name"] != null)
			res.displayName = o["name"].Value;

		if (o["choices"] != null)
		{
			JSONArray j = o["choices"].AsArray;
			res.choices = new string[j.Count];

			for (int i = 0 ; i < res.choices.Length ; ++i) res.choices[i] = j[i].Value;
		}

		return res;
	}

	public static string ParseGamevars(string input)
	{
		if (input == null)
			return null;

		foreach(Match m in Regex.Matches(input, "%n<(\\d+)>"))
		{
			PokemonState p = Game.TeamGetPokemonAt(int.Parse(m.Groups[1].Value));

			if (p != null)
				input = input.Replace(m.Groups[0].Value, p.nickname);
		}

		foreach(Match m in Regex.Matches(input, "%s<(\\d+)>"))
		{
			PokemonState p = Game.TeamGetPokemonAt(int.Parse(m.Groups[1].Value));

			if (p != null)
				input = input.Replace(m.Groups[0].Value, p.Species.name.CorrectTranslation(Game.Lang));
		}

		foreach(Match m in Regex.Matches(input, "%(\\d+)<([^,]+),([^,]+)>"))
		{
			PokemonState p = Game.TeamGetPokemonAt(int.Parse(m.Groups[1].Value));

			if (p != null)
				input = input.Replace(m.Groups[0].Value, (p.gender == Gender.Female ? m.Groups[3].Value : m.Groups[2].Value));
		}

		input = input.Replace("%g", Game.Money.ToString());

		return input;
	}

	// Special method to get the personality test adjectives

	public static string Adj(int id)
	{
		if (files == null) throw new Exception("GameText has not been initialized yet");

		JSONArray j = files["c0_prologue"]["adjectives"].AsArray;
		return j[id].Value;
	}
}