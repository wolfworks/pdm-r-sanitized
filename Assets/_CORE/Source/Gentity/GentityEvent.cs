﻿using Character;
using Character.Controllers;
using Gentity.Actions;
using Gentity.Holders;
using Gentity.Lexic;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gentity
{
	public class GentityEvent : MonoBehaviour
	{
		public GameObject interactionEffect;
		public List<BehaviourEntry> behaviours = new List<BehaviourEntry>();
		public GameObject target;

		private readonly Dictionary<string, BlockPosition> labels = new Dictionary<string, BlockPosition>();
		private PlayerCharacterController player;
		private GentityBehaviour currentBehaviour;
		private GentityActionBlock currentBlock;
		private GentityContext context;
		private IHolder holder;
		private AbstractCharacter targetCharacter;
		private Collider targetCollider;

		private GameObject currentInteractionEffect;

		private Transform TargetTransform => target == null ? transform : target.transform;
		public Queue<IHolder> HolderQueue { get; private set; }
		public int Memory { get; set; }
		public Dictionary<string, int> LocalVariables { get; set; }

		public void StartBehaviour(int index)
		{
			if (index < 0 || index >= behaviours.Count || currentBehaviour != null)
				return;

			var entry = behaviours[index];

			if (entry.behaviour.Running || entry.behaviour.startCondition != ActionEvent.ByScript)
				return;

			AttachBehaviour(entry.behaviour);
		}

		public bool BehaviourHasStarted(int index)
		{
			if (index < 0 || index >= behaviours.Count || currentBehaviour != null)
				return false;

			var entry = behaviours[index];

			return entry.behaviour.Running || entry.behaviour.Completed;
		}

		public bool BehaviourHasEnded(int index)
		{
			if (index < 0 || index >= behaviours.Count || currentBehaviour != null)
				return false;

			var entry = behaviours[index];

			return entry.behaviour.Completed;
		}

		public void StartBlock(List<ScriptableAction> b)
		{
			currentBlock = new GentityActionBlock(b, currentBlock);
		}

		public void SetLabel(string label)
		{
			var bp = new BlockPosition
			{
				block = currentBlock,
				position = currentBlock.Current
			};

			if (!labels.ContainsKey(label))
				labels.Add(label, bp);
			else
				labels[label] = bp;
		}

		public void GoToLabel(string label)
		{
			if (!labels.ContainsKey(label))
				return;

			currentBlock = labels[label].block;
			currentBlock.Current = labels[label].position;
		}

		public ScriptableAction Next()
		{
			var next = currentBlock.Current + 1;

			if (next >= currentBlock.Count)
				return null;

			return currentBlock[next];
		}

		public ScriptableAction Previous()
		{
			var previous = currentBlock.Current - 1;

			if (previous <= 0)
				return null;

			return currentBlock[previous];
		}

		public void SendActionPressed()
		{
			if (currentBehaviour == null)
				SetCurrentBlock(true);
		}

		#region Edition Methods
		public void AddNewBehaviour(GentityBehaviour behaviour, int weight = 0)
		{
			if (behaviours.Any(x => x.behaviour == behaviour))
				return;

			behaviours.Add(new BehaviourEntry
			{
				behaviour = behaviour,
				weight = weight
			});
		}

		public void AddNewBehaviourAt(int index, GentityBehaviour behaviour, int weight = 0)
		{
			if (index < 0 || index >= behaviours.Count || behaviours[index].behaviour != null || behaviours.Any(x => x.behaviour == behaviour))
				return;

			behaviours[index] = new BehaviourEntry
			{
				behaviour = behaviour,
				weight = weight
			};
		}

		public void RemoveEntryAt(int index)
		{
			behaviours.RemoveAt(index);
		}
		#endregion

		#region Component Methods
		private void Start()
		{
			targetCharacter = TargetTransform.GetComponent<AbstractCharacter>();
			targetCollider = TargetTransform.GetComponent<Collider>();

			context = new GentityContext
			{
				caller = this,
				itself = TargetTransform.gameObject,
				character = targetCharacter
			};

			HolderQueue = new Queue<IHolder>();
			LocalVariables = new Dictionary<string, int>();

			var playerExists = GameObject.FindWithTag("Player Controller");

			if (playerExists)
				player = playerExists.GetComponent<PlayerCharacterController>();
		}

		private void Update()
		{
			if (currentBehaviour == null)
			{
				SetCurrentBlock();

				// Interaction Effect
				if (behaviours.Where(b => b.behaviour.startCondition == ActionEvent.OnActionButton).Any(b => BehaviourCanRun(b.behaviour, true)))
					CreateInteractionEffect();
				else
					DeleteInteractionEffect();
			}
			else
			{
				RunCurrentBlock();
			}
		}
		#endregion

		#region Private Methods
		private void SetCurrentBlock(bool actionPressed = false)
		{
			foreach (var b in behaviours.OrderByDescending(x => -x.weight))
			{
				if (VerifyBehaviour(b.behaviour) && BehaviourCanRun(b.behaviour, actionPressed))
				{
					AttachBehaviour(b.behaviour);
					break;
				}
			}
		}

		private void RunCurrentBlock()
		{
			if (holder != null && holder.Hold())
				return;

			if (currentBlock.HasEnded)
			{
				if (currentBlock.parent == null)
				{
					currentBehaviour.Running = false;

					if (!currentBehaviour.loop)
						currentBehaviour.Completed = true;

					currentBehaviour = null;
					currentBlock = null;
					holder = null;
					Memory = 0;

					HolderQueue.Clear();
					labels.Clear();

					return;
				}

				currentBlock = currentBlock.parent;
			}

			holder = currentBlock.NextAction(context);
		}

		private void AttachBehaviour(GentityBehaviour behaviour)
		{
			currentBehaviour = behaviour;
			currentBlock = new GentityActionBlock(behaviour.actions);

			behaviour.Running = true;
		}

		private bool VerifyBehaviour(GentityBehaviour behaviour)
		{
			if (behaviour.Running || behaviour.Completed)
				return false;

			var verified = true;

			foreach (var c in behaviour.conditions)
			{
				int v;

				if (c.local)
				{
					if (!LocalVariables.TryGetValue(c.gamevar, out v))
						v = 0;
				}
				else
				{
					v = Game.Gamevars(c.gamevar);
				}

				if (v == c.val == c.not)
					verified = false;
			}

			return verified;
		}

		private bool BehaviourCanRun(GentityBehaviour behaviour, bool actionPressed)
		{
			if (behaviour.Running || behaviour.Completed)
				return false;

			var playerPos = player.GetCharacterPosition;
			var closeEnough = Vector3.Distance(playerPos, TargetTransform.position) <= behaviour.activationRange;

			switch (behaviour.startCondition)
			{
				case ActionEvent.OnActionButton:
					if (closeEnough && PlayerLookingAt() && actionPressed)
						return true;

					break;

				case ActionEvent.OnContact:
					if (closeEnough)
						return true;

					break;

				case ActionEvent.ImmediateStart:
					return true;
			}

			return false;
		}

		private bool PlayerLookingAt()
		{
			if (player == null)
				return false;

			var interaction = player.GetCurrentInteraction;

			if (interaction == null)
				return false;

			return ReferenceEquals(this, interaction);
		}

		private void CreateInteractionEffect()
		{
			if (currentInteractionEffect != null)
				return;

			var finalPos = TargetTransform.position;

			if (targetCharacter != null)
			{
				finalPos = targetCharacter.GetManager().InteractPoint;
			}
			else if (targetCollider != null)
			{
				var bounds = targetCollider.bounds;
				finalPos = bounds.center + Vector3.up * bounds.extents.y;
			}

			currentInteractionEffect = Instantiate(interactionEffect, finalPos, Quaternion.identity, TargetTransform);
		}

		private void DeleteInteractionEffect()
		{
			if (currentInteractionEffect == null)
				return;

			Destroy(currentInteractionEffect);
			currentInteractionEffect = null;
		}
		#endregion
	}
}