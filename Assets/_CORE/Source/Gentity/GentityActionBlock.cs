﻿using Gentity.Actions;
using Gentity.Holders;
using System.Collections.Generic;

namespace Gentity
{
	public class GentityActionBlock
	{
		public readonly GentityActionBlock parent;

		private readonly List<ScriptableAction> actions;
		private int index;

		public int Count => actions.Count;
		public bool HasEnded => index >= actions.Count;
		public int Current
		{
			get => index - 1;
			set => index = value;
		}

		public ScriptableAction this[int i] => actions[i];

		public GentityActionBlock(List<ScriptableAction> actions)
		{
			this.actions = actions;

			index = 0;
		}
		public GentityActionBlock(List<ScriptableAction> actions, GentityActionBlock parent)
		{
			this.actions = actions;
			this.parent = parent;

			index = 0;
		}

		public IHolder NextAction(GentityContext context)
		{
			if (HasEnded)
				return null;
			
			return actions[index++].Perform(context);
		}
	}
}