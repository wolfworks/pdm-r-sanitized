﻿using UnityEngine;
using UnityEngine.Playables;

namespace Gentity.Timeline
{
	public class GentityActionClip : PlayableAsset
	{
		public uint pageId;

		[Header("Time loop")]
		[Tooltip("Will make the clip loop while the behaviour is running")]
		public bool waitCompletion = true;
		[Tooltip("Will immediately jump to the end of the clip when the loop ends")]
		public bool skipInstantly = true;
		[Tooltip("Has effect only when Skip Instantly is active")]
		public float skipDuration = 0.33f;

		[HideInInspector]
		public double clipStart;
		[HideInInspector]
		public double clipEnd;

		public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
		{
			var playable = ScriptPlayable<GentityActionPlayable>.Create(graph);

			var behaviour = playable.GetBehaviour();
			behaviour.pageId = pageId;
			behaviour.waitCompletion = waitCompletion;
			behaviour.skipInstantly = skipInstantly;
			behaviour.skipDuration = skipDuration;
			behaviour.clipStart = clipStart;
			behaviour.clipEnd = clipEnd;

			return playable;
		}
	}
}