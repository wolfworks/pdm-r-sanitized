﻿using UnityEngine;
using UnityEngine.Playables;

namespace Gentity.Timeline
{
	public class GentityActionPlayable : PlayableBehaviour
	{
		const double TIME_THRESHOLD = 0.075;

		public uint pageId;
		public bool waitCompletion = true;
		public bool skipInstantly = true;
		public float skipDuration = 0.33f;
		public double clipStart;
		public double clipEnd;

		private double? skipReference = null;

		public override void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
#if UNITY_EDITOR
			// Because gentity behaviours only execute in play mode, we need to check that we are actually playing
			if (!Application.isPlaying) return;
#endif
			var director = (PlayableDirector)playable.GetGraph().GetResolver();
			var gentityEvent = (GentityEvent)playerData;

			if (!gentityEvent.BehaviourHasStarted((int)pageId))
				gentityEvent.StartBehaviour((int)pageId);

			if (!waitCompletion) return;

			var canLoop = !gentityEvent.BehaviourHasEnded((int)pageId);

			if (!canLoop && skipInstantly)
			{
				if (skipDuration == 0)
				{
					director.time = clipEnd;
					return;
				}

				if (!skipReference.HasValue)
					skipReference = director.time;

				director.time += (clipEnd - skipReference.Value) * (info.deltaTime / skipDuration);

				return;
			}

			if (canLoop && director.time >= clipEnd - info.deltaTime - TIME_THRESHOLD)
				director.time = clipStart;
		}
	}
}