﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Gentity.Timeline
{
	[TrackClipType(typeof(GentityActionClip))]
	[TrackBindingType(typeof(GentityEvent))]
	[TrackColor(.2f, .8f, .4f)]
	public class GentityActionTrack : TrackAsset
	{
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			foreach (var clip in GetClips())
			{
				var timeStateclip = clip.asset as GentityActionClip;
				if (timeStateclip)
				{
					timeStateclip.clipStart = clip.start;
					timeStateclip.clipEnd = clip.end;
				}
			}

			return base.CreateTrackMixer(graph, go, inputCount);
		}
	}
}