﻿using Gentity.Actions;
using Gentity.Lexic;
using System.Collections.Generic;
using UnityEngine;

namespace Gentity
{
	public class GentityBehaviour : ScriptableObject
	{
		public List<ScriptableAction> actions = new List<ScriptableAction>();
		public List<ExecutionCondition> conditions = new List<ExecutionCondition>();
		public ActionEvent startCondition;
		public bool loop;
		public float activationRange = 1f;

		public bool Running { get; set; }
		public bool Completed { get; set; }
	}
}
