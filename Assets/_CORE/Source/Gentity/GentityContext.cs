﻿using Character;
using UnityEngine;

namespace Gentity
{
	public class GentityContext
	{
		public GentityEvent caller;
		public GameObject itself;
		public AbstractCharacter character;
	}
}