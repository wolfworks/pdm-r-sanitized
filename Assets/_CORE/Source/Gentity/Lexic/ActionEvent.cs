﻿namespace Gentity.Lexic
{
	public enum ActionEvent
	{
		OnActionButton,
		OnContact,
		ImmediateStart,
		ByScript
	}
}