﻿namespace Gentity.Lexic
{
	public enum Operation
	{
		Equals,
		NotEquals,
		GreaterThan,
		LesserThan
	}
}