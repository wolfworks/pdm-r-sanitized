﻿namespace Gentity
{
	public struct BlockPosition
	{
		public GentityActionBlock block;
		public int position;
	}
}