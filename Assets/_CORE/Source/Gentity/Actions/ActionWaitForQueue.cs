﻿using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Wait For Queue", "Control")]
	public class ActionWaitForQueue : ScriptableAction
	{
		public override string GetDisplay()
		{
			return "<color=blue>Wait for all the actions in the queue to end</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			return new HoldForQueue(context.caller.HolderQueue);
		}
	}
}