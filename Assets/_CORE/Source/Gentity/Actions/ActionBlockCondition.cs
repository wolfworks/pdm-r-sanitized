﻿using Gentity.Lexic;
using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Condition", "Control")]
	public class ActionBlockCondition : ScriptableActionBlock
	{
		public string gameVariable;
		public bool localVariable;
		public Operation operation;
		public int testValue;

		private bool conditionMet = false;

		public override string GetDisplay()
		{
			var n = string.IsNullOrEmpty(gameVariable) ? "the previous value" : gameVariable;
			var l = !string.IsNullOrEmpty(gameVariable) && localVariable ? "local " : "";
			var o = "";

			switch (operation)
			{
				case Operation.Equals: o = " = "; break;
				case Operation.NotEquals: o = " != "; break;
				case Operation.GreaterThan: o = " > "; break;
				case Operation.LesserThan: o = " < "; break;
			}

			return "<color=blue>If "+l+n+o+testValue+" then</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			int v;

			if (string.IsNullOrEmpty(gameVariable))
			{
				v = context.caller.Memory;
			}
			else
			{
				if (localVariable)
				{
					if (!context.caller.LocalVariables.TryGetValue(gameVariable, out v))
						v = 0;
				}
				else
				{
					v = Game.Gamevars(gameVariable);
				}
			}

			switch (operation)
			{
				case Operation.Equals: conditionMet = v == testValue; break;
				case Operation.NotEquals: conditionMet = v != testValue; break;
				case Operation.GreaterThan: conditionMet = v > testValue; break;
				case Operation.LesserThan: conditionMet = v < testValue; break;
			}

			if (conditionMet)
				context.caller.StartBlock(block);

			return null;
		}

		public bool WasSuccessful()
		{
			return conditionMet;
		}
	}
}