﻿using Character;
using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Animate a Character (Trigger)", "Characters")]
	public class ActionAnimationTrigger : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public string animation;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Trigger "+display+"'s animation \""+animation+"\"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.AnimationHandler.SetTrigger(animation);

			return null;
		}
	}
}