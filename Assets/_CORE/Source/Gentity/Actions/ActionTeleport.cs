﻿using Character;
using Character.Lexic;
using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Teleport a character", "Characters")]
	public class ActionTeleport : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public Vector3 location;
		public TeleportMode mode;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Teleport " + display + " to " + location.ToString() + " using mode " + System.Enum.GetName(typeof(TeleportMode), mode) + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.Teleport(location, mode);

			return null;
		}
	}
}