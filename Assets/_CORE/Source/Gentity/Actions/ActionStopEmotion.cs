﻿using UnityEngine;
using Gentity.Holders;
using Character.Handler;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Stop Emotion Effect", "Characters")]
	public class ActionStopEmotion : ScriptableAction
	{
		public AbstractCharacter executeAs;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Stop effect for "+display+"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.EmotionHandler.StopEffect();

			return null;
		}
	}
}