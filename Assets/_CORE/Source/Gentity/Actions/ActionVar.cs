﻿using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Set a Gamevar", "Control")]
	public class ActionVar : ScriptableAction
	{
		public string variableName;
		public bool localVariable;
		public int value;

		public override string GetDisplay()
		{
			var local = localVariable ? "local " : "";

			return "<color=blue>Set " + local + variableName + " to " + value.ToString() + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			if (localVariable)
			{
				if (context.caller.LocalVariables.ContainsKey(variableName))
					context.caller.LocalVariables[variableName] = value;
				else
					context.caller.LocalVariables.Add(variableName, value);
			}
			else
			{
				Game.Gamevars(variableName, value);
			}

			return null;
		}
	}
}