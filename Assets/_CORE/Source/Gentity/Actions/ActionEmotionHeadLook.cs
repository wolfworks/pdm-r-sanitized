﻿using UnityEngine;
using Gentity.Holders;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character's Head Look", "Characters")]
	public class ActionEmotionHeadLook : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public GameObject lookTarget;

		public override string GetDisplay()
		{
			if (lookTarget == null)
				return "<color=red><i>Error: lookTarget is null!</i></color>";

			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Make "+display+"'s head look at "+lookTarget.name+"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			var target = lookTarget.transform;

			source.AnimationHandler.HeadLook(target);

			return null;
		}
	}
}