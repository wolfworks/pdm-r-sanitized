﻿using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Goto", "Control")]
	public class ActionGotoLabel : ScriptableAction
	{
		public string label;

		public override string GetDisplay()
		{
			return "<color=blue>Goto: "+label+"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			context.caller.GoToLabel(label);

			return null;
		}
	}
}