﻿using Character;
using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character Rotate", "Characters")]
	public class ActionAngle : ScriptableActionWaitable
	{
		public AbstractCharacter executeAs;
		public float angle;
		public float speed = 5f;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Make " + display + " look at " + angle + " degrees at speed " + speed.ToString() + WaitString + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			var point = new Vector3(-Mathf.Cos(Mathf.Deg2Rad * angle), 0f, Mathf.Sin(Mathf.Deg2Rad * angle));

			source.MovementHandler.LookTowards(point, speed);

			return HoldIfNecessary(new HoldUntil(() => source.MovementHandler.MotionEnded), context);
		}
	}
}