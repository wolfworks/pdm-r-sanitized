﻿using System.Collections.Generic;
using UnityEngine;

namespace Gentity.Actions
{
	public abstract class ScriptableActionBlock : ScriptableAction
	{
		[HideInInspector]
		public List<ScriptableAction> block = new List<ScriptableAction>();
	}
}