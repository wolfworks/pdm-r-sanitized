﻿using Gentity.Holders;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character's Head Reset", "Characters")]
	public class ActionEmotionHeadReset : ScriptableAction
	{
		public AbstractCharacter executeAs;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Reset "+display+"'s head position</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.AnimationHandler.ResetLook();

			return null;
		}
	}
}