﻿using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Else", "Control")]
	public class ActionBlockElse : ScriptableActionBlock
	{
		public override string GetDisplay()
		{
			return "<color=blue>Else</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			var previous = context.caller.Previous() as ActionBlockCondition;

			if (previous != null && previous.WasSuccessful())
				context.caller.StartBlock(block);

			return null;
		}
	}
}