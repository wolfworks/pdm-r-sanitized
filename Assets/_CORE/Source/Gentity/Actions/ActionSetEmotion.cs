﻿using Gentity.Holders;
using Character.Lexic;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Set Character Emotion", "Characters")]
	public class ActionSetEmotion : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public CharacterEmotion emotion;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Set emotion for " + display + " to " + emotion + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.EmotionHandler.CurrentEmotion = emotion;

			return null;
		}
	}
}