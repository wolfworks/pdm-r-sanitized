﻿using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Play an Animation Clip", "Motion")]
	public class ActionAnimationClip : ScriptableAction
	{
		public GameObject executeAs;
		public string clip;
		public float blendTime = 0f;

		public override string GetDisplay()
		{
			if (string.IsNullOrEmpty(clip))
				return "<color=red><i>Error: clip is null!</i></color>";

			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			string t = blendTime + " second" + (blendTime != 1 ? "s" : "");

			return "<color=#008883>Play animation clip " + clip + " on " + display + " blended in " + t + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			GameObject source;

			if (executeAs != null)
				source = executeAs;
			else
				source = context.itself.gameObject;

			var a = source.GetComponent<Animation>();

			if (blendTime == 0f)
				a.Play(clip);
			else
				a.Blend(clip, 1f, blendTime);

			return null;
		}
	}
}