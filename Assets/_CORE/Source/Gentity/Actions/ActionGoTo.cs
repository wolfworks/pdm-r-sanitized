﻿using UnityEngine;
using Gentity.Holders;
using Character.Handler;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character Go To", "Characters")]
	public class ActionGoTo : ScriptableActionWaitable
	{
		public AbstractCharacter executeAs;
		public GameObject target;
		public float speed = 2f;

		public override string GetDisplay()
		{
			if (target == null)
				return "<color=red><i>Error: target is null!</i></color>";

			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Make " + display + " go to " + target.name + " at speed " + speed + WaitString + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterMovementHandler source;

			if (executeAs != null)
				source = executeAs.GetManager().MovementHandler;
			else
				source = context.character.GetManager().MovementHandler;

			var t = target.transform.position;

			source.GoTo(t, speed);

			return HoldIfNecessary(new HoldUntil(() => source.MotionEnded), context);
		}
	}
}