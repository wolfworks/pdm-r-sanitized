﻿using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Wait", "Control")]
	public class ActionWait : ScriptableAction
	{
		public float waitDuration;

		public override string GetDisplay()
		{
			return "<color=blue>Wait for "+waitDuration+" second"+(waitDuration == 1 ? "" : "s")+"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			return new HoldForSeconds(waitDuration);
		}
	}
}