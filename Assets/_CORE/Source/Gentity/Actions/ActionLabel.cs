﻿using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Label", "Control")]
	public class ActionLabel : ScriptableAction
	{
		public string label;

		public override string GetDisplay()
		{
			return "<color=blue>Label: "+label+"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			context.caller.SetLabel(label);

			return null;
		}
	}
}