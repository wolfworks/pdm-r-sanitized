﻿using Gentity.Holders;
using Gentity.Lexic;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Conditional Else", "Control")]
	public class ActionBlockElseif : ActionBlockCondition
	{
		public override string GetDisplay()
		{
			string n = (string.IsNullOrEmpty(gameVariable) ? "the previous value" : gameVariable);
			string o = "";

			switch (operation)
			{
				case Operation.Equals: o = " = "; break;
				case Operation.NotEquals: o = " != "; break;
				case Operation.GreaterThan: o = " > "; break;
				case Operation.LesserThan: o = " < "; break;
			}

			return "<color=blue>Else If "+n+o+testValue+" then</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			var previous = context.caller.Previous() as ActionBlockCondition;

			if (previous != null && previous.WasSuccessful())
				base.Perform(context);

			return null;
		}
	}
}