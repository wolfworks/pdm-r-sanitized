﻿using Gentity.Holders;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character Nod", "Characters")]
	public class ActionEmotionNod : ScriptableAction
	{
		public AbstractCharacter executeAs;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Make "+display+" nod</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.AnimationHandler.Nod();

			return null;
		}
	}
}