﻿using UnityEngine;
using Gentity.Holders;
using Character.Handler;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character Look", "Characters")]
	public class ActionLookAt : ScriptableActionWaitable
	{
		public AbstractCharacter executeAs;
		public GameObject lookTarget;
		public float speed = 5f;

		public override string GetDisplay()
		{
			if (lookTarget == null)
				return "<color=red><i>Error: lookTarget is null!</i></color>";

			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Make " + display + " look at " + lookTarget.name + " at speed " + speed.ToString() + WaitString + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterMovementHandler source;

			if (executeAs != null)
				source = executeAs.GetManager().MovementHandler;
			else
				source = context.character.GetManager().MovementHandler;

			var target = lookTarget.transform.position;

			source.LookTowards(target - source.transform.position, speed);

			return HoldIfNecessary(new HoldUntil(() => source.MotionEnded), context);
		}
	}
}