﻿using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Debug Message", "Misc")]
	public class ActionDebug : ScriptableAction
	{
		public string message;

		public override string GetDisplay()
		{
			return "<color=#555555>Write \""+message+"\" in the console</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			Debug.Log(message);
			return null;
		}
	}
}