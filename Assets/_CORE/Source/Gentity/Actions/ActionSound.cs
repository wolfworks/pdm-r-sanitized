﻿using Gentity.Holders;
using FMODUnity;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Play a Sound", "Misc")]
	public class ActionSound : ScriptableAction
	{
		[EventRef]
		public string soundEvent;

		public override string GetDisplay()
		{
			if (string.IsNullOrEmpty(soundEvent))
				return "<color=red><i>Error: sound is null!</i></color>";

			return "<color=#440088>Play sound \"" + soundEvent + "\"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			RuntimeManager.PlayOneShot(soundEvent, context.itself.transform.position);

			return null;
		}
	}
}