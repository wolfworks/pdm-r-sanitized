﻿using Gentity.Holders;
using UnityEngine.Events;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Invoke a Unity Event", "Misc")]
	public class ActionUnityEvent : ScriptableAction
	{
		public UnityEvent onPerform;

		public override string GetDisplay()
		{
			return "<color=#440088>Invoke an event</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			onPerform.Invoke();
			return null;
		}
	}
}