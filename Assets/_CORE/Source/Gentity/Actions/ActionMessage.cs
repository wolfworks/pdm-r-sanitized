﻿using Gentity.Holders;
using UnityEngine;
using Pokemon.Data;
using System;

namespace Gentity.Actions
{
	[Serializable]
	[ActionCategory("Show Message", "Display")]
	public partial class ActionMessage : ScriptableActionWaitable
	{

		public string messageReference;
		public bool hasSound;
		public string pokemonPortrait;
		public PortraitEmotion portraitEmotion;
		public float pitch = 1f;

		public override string GetDisplay()
		{
			string sound = (hasSound ? " with sound pitched " + pitch : "");
			string portrait = (string.IsNullOrEmpty(pokemonPortrait) ? "" : " using " + portraitEmotion + " portrait of " + pokemonPortrait);

			return "<color=#007511>Display " + messageReference + portrait + sound + WaitString + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			PokemonDatabase database = Game.GetDatabase();
			Mes mes = GameText.GetEntry(messageReference);

			Sprite portrait = null;

			if (int.TryParse(pokemonPortrait, out int teamid))
				portrait = Game.TeamGetPokemonAt(teamid).Species.portraits[(int)portraitEmotion];
			else if (!string.IsNullOrEmpty(pokemonPortrait))
				portrait = database.Get<Species>(pokemonPortrait).portraits[(int)portraitEmotion];

			// Negative pitch in this context means that it's based on the gender of either the protagonist (-1) or the partner (-2)
			if (pitch == -1)
				pitch = Game.TeamGetPokemonAt(Game.Protagonist).gender == Pokemon.Lexic.Gender.Female ? 1.25f : 1f;
			else if (pitch == -2)
				pitch = Game.TeamGetPokemonAt(Game.Partner).gender == Pokemon.Lexic.Gender.Female ? 1.25f : 1f;

			var dialogue = GameObject.FindWithTag("Textbox").GetComponent<Dialogue>();

			dialogue.PopDialogue(GameText.ParseGamevars(mes.message),
				context.caller.Next() is ActionMessage,
				hasSound,
				mes.choices ?? (Array.Empty<string>()),
				a => context.caller.Memory = a,
				portrait,
				GameText.ParseGamevars(mes.displayName),
				pitch
			);

			return HoldIfNecessary(new HoldUntil(dialogue.HasDialogueEnded), context);
		}
	}
}