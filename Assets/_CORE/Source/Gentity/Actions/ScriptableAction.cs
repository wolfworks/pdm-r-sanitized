﻿using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	public abstract class ScriptableAction : ScriptableObject
	{
		public abstract string GetDisplay();
		public abstract IHolder Perform(GentityContext context);
	}
}