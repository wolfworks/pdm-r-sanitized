﻿using Character;
using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Animate a Character (Int)", "Characters")]
	public class ActionAnimationInt : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public string animation;
		public int setTo;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Set "+display+"'s animation \""+animation+"\" to "+setTo+"</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.AnimationHandler.SetInteger(animation, setTo);

			return null;
		}
	}
}