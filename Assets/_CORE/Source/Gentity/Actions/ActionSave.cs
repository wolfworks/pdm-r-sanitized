﻿using Gentity.Holders;
using UnityEngine;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Trigger Autosave", "Misc")]
	public class ActionSave : ScriptableAction
	{
		public override string GetDisplay()
		{
			return "<color=#440088>Trigger autosave</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			var gameSave = GameObject.FindWithTag("DataManager").GetComponent<GameSave>();

			gameSave.Save();

			return new HoldUntil(gameSave.HasSavingEnded);
		}
	}
}