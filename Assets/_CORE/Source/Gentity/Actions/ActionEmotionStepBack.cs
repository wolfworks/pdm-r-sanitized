﻿using Gentity.Holders;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Make a Character Step Back", "Characters")]
	public class ActionEmotionStepBack : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public float distance;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Make "+display+" step back "+distance+" units</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.AnimationHandler.StepBack(distance);

			return null;
		}
	}
}