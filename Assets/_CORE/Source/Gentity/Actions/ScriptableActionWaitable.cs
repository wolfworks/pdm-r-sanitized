﻿using Gentity.Holders;
using System;

namespace Gentity.Actions
{
	public abstract class ScriptableActionWaitable : ScriptableAction
	{
		[Serializable]
		public enum WaitMethod
		{
			Wait,
			Enqueue,
			DoNotWait
		}

		public WaitMethod waitMethod;

		protected string WaitString
		{
			get
			{
				switch (waitMethod)
				{
					case WaitMethod.Wait:
						return " and wait";

					case WaitMethod.Enqueue:
						return " and enqueue";

					default:
						return "";
				}
			}
		}

		protected IHolder HoldIfNecessary(IHolder holder, GentityContext context)
		{
			switch (waitMethod)
			{
				case WaitMethod.Wait:
					return holder;

				case WaitMethod.Enqueue:
					context.caller.HolderQueue.Enqueue(holder);
					return null;

				default:
					return null;
			}
		}
	}
}
