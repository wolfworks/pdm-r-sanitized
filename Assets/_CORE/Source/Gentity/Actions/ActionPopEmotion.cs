﻿using Gentity.Holders;
using Character;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Pop Emotion Effect", "Characters")]
	public class ActionPopEmotion : ScriptableAction
	{
		public AbstractCharacter executeAs;
		public EmotionEffectType emotion;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>Pop effect " + emotion + " for " + display + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			CharacterManager source;

			if (executeAs != null)
				source = executeAs.GetManager();
			else
				source = context.character.GetManager();

			source.EmotionHandler.PopEffect(emotion);

			return null;
		}
	}
}