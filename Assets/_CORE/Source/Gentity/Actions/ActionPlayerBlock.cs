﻿using UnityEngine;
using Gentity.Holders;
using Character.Controllers;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Block the Player", "Misc")]
	public class ActionPlayerBlock : ScriptableAction
	{
		public bool block;

		public override string GetDisplay()
		{
			return "<color=#440088>" + (block ? "Block" : "Unblock") + " the Player</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			GameObject.FindWithTag("Player Controller").GetComponent<PlayerCharacterController>().controllable = !block;
			return null;
		}
	}
}