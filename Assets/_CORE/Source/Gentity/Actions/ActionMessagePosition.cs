﻿using Gentity.Holders;
using UnityEngine;
using System;

using static Dialogue;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Set Message Position", "Display")]
	public class ActionMessagePosition : ScriptableAction
	{
		public DialoguePosition position;
		public bool mirroredPortrait;

		public override string GetDisplay()
		{
			return "<color=#007511>Set message position to \"" + Enum.GetName(typeof(DialoguePosition), position) + "\" and " + (mirroredPortrait ? "mirror" : "reset") + " portrait</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			var dialogue = GameObject.FindWithTag("Textbox").GetComponent<Dialogue>();

			dialogue.position = position;
			dialogue.mirroredPortrait = mirroredPortrait;

			return null;
		}
	}
}