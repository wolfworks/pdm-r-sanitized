﻿using UnityEngine;
using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Enable or Disable a Character", "Characters")]
	public class ActionEnableDisable : ScriptableAction
	{
		public GameObject executeAs;
		public bool enable = true;

		public override string GetDisplay()
		{
			string display;

			if (executeAs == null)
				display = "this entity";
			else
				display = executeAs.name;

			return "<color=#881100>" + (enable ? "Enable " : "Disable ") + display + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			GameObject source;

			if (executeAs != null)
				source = executeAs;
			else
				source = context.itself.gameObject;

			source.SetActive(enable);

			return null;
		}
	}
}
