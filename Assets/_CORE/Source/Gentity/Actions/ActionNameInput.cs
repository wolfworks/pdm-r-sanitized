﻿using UnityEngine;
using Gentity.Holders;

namespace Gentity.Actions
{
	[System.Serializable]
	[ActionCategory("Ask For Name", "Display")]
	public class ActionNameInput : ScriptableActionWaitable
	{
		public string messageReference;
		public int partyMemberIndex;
		public bool isTeamName;

		private bool endEdit;

		public override string GetDisplay()
		{
			var index = isTeamName ? "the team name" : partyMemberIndex.ToString();

			return "<color=#007511>Ask for a name with message " + messageReference + " for " + index + WaitString + "</color>";
		}

		public override IHolder Perform(GentityContext context)
		{
			var nameEdit = GameObject.FindWithTag("Name Edit").GetComponent<NameField>();

			nameEdit.AskName(messageReference, isTeamName ? (int?)null : partyMemberIndex, _ => endEdit = true);

			return HoldIfNecessary(new HoldUntil(() => endEdit), context);
		}
	}
}
