﻿using System.Collections.Generic;

namespace Gentity.Holders
{
	public class HoldForQueue : IHolder
	{
		private readonly Queue<IHolder> queue;
		private IHolder currentHolder;

		public HoldForQueue(Queue<IHolder> q)
		{
			queue = q;
		}

		public bool Hold()
		{
			if (currentHolder != null && currentHolder.Hold())
				return true;

			do
			{
				if (queue.Count == 0)
					return false;

				currentHolder = queue.Dequeue();
			}
			while (currentHolder == null || !currentHolder.Hold());

			return true;
		}
	}
}
