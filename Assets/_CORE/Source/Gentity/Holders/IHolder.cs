﻿namespace Gentity.Holders
{
	public interface IHolder
	{
		bool Hold();
	}
}
