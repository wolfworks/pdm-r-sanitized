﻿using System;

namespace Gentity.Holders
{
	public class HoldUntil : IHolder
	{
		private readonly Func<bool> predicate;

		public HoldUntil(Func<bool> p)
		{
			predicate = p;
		}

		public bool Hold()
		{
			return !predicate();
		}
	}
}
