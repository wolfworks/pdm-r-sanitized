﻿using UnityEngine;

namespace Gentity.Holders
{
	public class HoldForSeconds : IHolder
	{
		private readonly float waitDuration;
		private readonly float timeStart;

		public HoldForSeconds(float d)
		{
			waitDuration = d;
			timeStart = Time.time;
		}

		public bool Hold()
		{
			return Time.time - timeStart < waitDuration;
		}
	}
}
