﻿using System;

namespace Gentity
{
	[Serializable]
	public struct ExecutionCondition
	{
		public string gamevar;
		public int val;
		public bool not;
		public bool local;

		public override bool Equals(object obj)
		{
			if (obj is ExecutionCondition other)
				return GetHashCode() == other.GetHashCode();

			return false;
		}

		public override int GetHashCode()
		{
			return gamevar.GetHashCode() * val.GetHashCode() * not.GetHashCode() * 11;
		}

		public static bool operator ==(ExecutionCondition left, ExecutionCondition right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(ExecutionCondition left, ExecutionCondition right)
		{
			return !(left == right);
		}
	}
}
