﻿using System;

namespace Gentity
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public sealed class ActionCategoryAttribute : Attribute
	{
		public string name;
		public string category;

		public ActionCategoryAttribute(string n, string c = "")
		{
			name = n;
			category = c;
		}
	}
}