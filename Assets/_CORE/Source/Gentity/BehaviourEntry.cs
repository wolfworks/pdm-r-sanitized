﻿using System;

namespace Gentity
{
	[Serializable]
	public struct BehaviourEntry
	{
		public GentityBehaviour behaviour;
		public int weight;
	}
}