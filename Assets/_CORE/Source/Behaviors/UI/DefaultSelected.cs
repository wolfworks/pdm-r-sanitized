﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DefaultSelected : MonoBehaviour
{
	private void OnEnable()
	{
		var current = EventSystem.current.currentSelectedGameObject;

		if (current == null || !ReferenceEquals(current, gameObject))
			StartCoroutine(Select());
	}

	private IEnumerator Select()
	{
		EventSystem.current.SetSelectedGameObject(null);

		yield return new WaitForEndOfFrame();

		GetComponent<Selectable>().Select();

		if (TryGetComponent<InputField>(out var inputField))
			inputField.OnPointerClick(new PointerEventData(EventSystem.current));
	}
}
