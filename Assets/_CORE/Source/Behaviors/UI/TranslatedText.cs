﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TranslatedText : MonoBehaviour
{
	void Start()
	{
		Text t = GetComponent<Text>();

		foreach(Match m in Regex.Matches(t.text, "\\[(.+)\\]"))
		{
			string translated = GameText.GetString(m.Groups[1].Value);
			t.text = t.text.Replace(m.Value, translated);
		}
	}
}