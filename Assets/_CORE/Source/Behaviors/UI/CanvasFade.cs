﻿using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasFade : AbstractFader
{
	private CanvasGroup group;

	protected override void Start()
	{
		base.Start();
		group = GetComponent<CanvasGroup>();
	}

	protected override void ApplyProperty(float value)
	{
		group.alpha = value;
	}
}