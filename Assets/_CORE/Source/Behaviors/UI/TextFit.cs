﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TextFit : MonoBehaviour
{
	public bool width;
	public bool height;

	void OnGUI()
	{
		RectTransform rectTransform = gameObject.GetComponent<RectTransform>();

		float w = (width ? gameObject.GetComponent<Text>().preferredWidth : rectTransform.sizeDelta.x);
		float h = (height ? gameObject.GetComponent<Text>().preferredHeight : rectTransform.sizeDelta.y);

		rectTransform.sizeDelta = new Vector2(w, h);
	}
}
