﻿using UnityEngine;
using UnityEngine.UI;

public class ScreenFade : AbstractFader
{
	public Color fadeColor;

	private RawImage image;

	protected override void Start()
	{
		base.Start();
		image = GameObject.FindWithTag("Fade Layer").GetComponent<RawImage>();
	}

	public void SetColor(Color color)
	{
		fadeColor = color;
	}

	protected override void ApplyProperty(float value)
	{
		image.color = new Color(fadeColor.r, fadeColor.g, fadeColor.b, value);
	}
}
