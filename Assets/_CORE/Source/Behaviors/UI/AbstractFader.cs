﻿using UnityEngine;

public abstract class AbstractFader : MonoBehaviour
{
	public enum StartBehavior
	{
		Unfade,
		Opaque,
		Transparent
	}

	#region Component Fields
	public float fadeSpeed = 1.33f;
	public StartBehavior startBehavior = StartBehavior.Unfade;
	#endregion

	private float alpha = 1f;
	private int direction = 1;

	protected abstract void ApplyProperty(float value);

	#region Component Methods
	protected virtual void Start()
	{
		if (startBehavior == StartBehavior.Unfade)
		{
			direction = -1;
		}
		else if (startBehavior == StartBehavior.Transparent)
		{
			direction = -1;
			alpha = 0f;
		}
	}

	private void Update()
	{
		alpha = Mathf.Clamp01(alpha + direction * fadeSpeed * Time.deltaTime);

		ApplyProperty(alpha);
	}
	#endregion

	public void FadeOut()
	{
		direction = -1;
	}

	public void FadeIn()
	{
		direction = 1;
	}

	public void Flash()
	{
		alpha = 1f - alpha;
	}

	public void SetSpeed(float speed)
	{
		fadeSpeed = speed;
	}
}