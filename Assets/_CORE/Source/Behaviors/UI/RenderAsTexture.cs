﻿using UnityEngine;
using UnityEngine.UI;

public class RenderAsTexture : MonoBehaviour
{
	[Range(0f, 1f)]
	public float maximumTransparency = 1f;
	[Range(0f, 1f)]
	public float startingTransparency = 1f;
	
	private RawImage destination;
	private RenderTexture texture;
	private float currentAlpha = 1.0f;
	private int direction = 1;

	void Start()
	{
		texture = new RenderTexture(Screen.width, Screen.height, 0)
		{
			antiAliasing = 2,
			anisoLevel = 2,
			filterMode = FilterMode.Bilinear
		};

		GetComponent<Camera>().targetTexture = texture;

		destination = GameObject.FindWithTag("Noclip Layer").GetComponent<RawImage>();
		destination.texture = texture;

		if (startingTransparency < maximumTransparency)
			direction = -1;

		currentAlpha = startingTransparency;
		destination.color = new Color(destination.color.r, destination.color.g, destination.color.b, currentAlpha);
	}

	void OnGUI()
	{
		currentAlpha += direction * 1.33f * Time.deltaTime;
		currentAlpha = Mathf.Clamp(currentAlpha, 0f, maximumTransparency);

		destination.color = new Color(destination.color.r, destination.color.g, destination.color.b, currentAlpha);
	}

	public void FadeOut()
	{
		direction = -1;
	}

	public void FadeIn()
	{
		direction = 1;
	}
}
