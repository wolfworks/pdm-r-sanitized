﻿#if UNITY_EDITOR
using Dungeon.Core;
using UnityEngine;
using UnityEngine.UI;

public class DebugDrawDungeon : MonoBehaviour
{
	const int SIZE = 40;
	
	private void Start()
	{
		var image = GetComponent<Image>();

		var dungeon = new Dungeon.Dungeon(SIZE, SIZE, new[]
		{
			new RoomProfile {deformation = 0f, edges = 4, margin = 6, maxSize = 6, minSize = 4},
			new RoomProfile {deformation = .5f, edges = 5, margin = 6, maxSize = 8, minSize = 6}
		});

		dungeon.Generate(7, 0, 0.5f, 1f, Tile.Water);

		var map = new Texture2D(SIZE, SIZE, TextureFormat.RGBA32, false)
		{
			filterMode = FilterMode.Point
		};

		for (int y = 0; y < SIZE; ++y)
		{
			for (int x = 0; x < SIZE; ++x)
			{
				if (dungeon.GetTile(x, y) == Tile.Room)
					map.SetPixel(x, y, Color.red);
				else if (dungeon.GetTile(x, y) == Tile.Corridor)
					map.SetPixel(x, y, Color.cyan);
				else if (dungeon.GetTile(x, y) == Tile.Water)
					map.SetPixel(x, y, Color.blue);
				else
					map.SetPixel(x, y, Color.clear);
			}
		}

		map.Apply();
		image.sprite = Sprite.Create(map, new Rect(0, 0, SIZE, SIZE), new Vector2(0.5f, 0.5f));
		image.rectTransform.sizeDelta = new Vector2(SIZE * DungeonManager.MAP_PIXEL, SIZE * DungeonManager.MAP_PIXEL);
	}
}
#endif