﻿#if UNITY_EDITOR
using Pokemon;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Pokemon.Data;
using Pokemon.Exceptions;
using System.Linq;
using System.Text;

public class DebugController : MonoBehaviour
{
	private bool gameLoaded;
	private bool loading = false;
	private string specificScene;

	void Start()
	{
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;

		if (Game.Load() == 0)
		{
			gameLoaded = true;
		}
		else
		{
			gameLoaded = false;

			SetDefaultTeam();
			GameObject.Find("teamset_status").GetComponent<Text>().text = "";
		}
	}

	void LateUpdate()
	{
		
		if (loading) return;
		
		var gamevars = new StringBuilder();

		foreach (var variable in Game.Gamevars())
			gamevars.AppendLine($"{variable}: {Game.Gamevars(variable)}");

		GameObject.Find("gamestate_text").GetComponent<Text>().text =
$@"Game was started: {(gameLoaded ? "Yes" : "No")}

Protagonist: {Game.Party(Game.Protagonist).nickname} (lv {Game.Party(Game.Protagonist).Level})
Partner: {Game.Party(Game.Partner).nickname} (lv {Game.Party(Game.Partner).Level})
Postgame partner: {(Game.PostgamePartner == null ? "<not set>" : Game.PostgamePartner.Species.name.CorrectTranslation())}
Party size: {Game.PartyCount()}
Team size: {Game.Team.Count(x => x != -1)}

Money: {Game.Money}
Items in bag: {Game.Bag.Count + (Game.IsBagFull() ? " (full)" : "")}
Items in storage: {Game.Storage.Count}

Chapter: {Game.CurrentChapter}
Current Level: {(Game.CurrentLevel == -1 ? "None" : SceneManager.GetSceneByBuildIndex(Game.CurrentLevel).name)}
In a dungeon: {(Game.Dungeon.HasValue ? "Yes, floor #" + Game.Dungeon?.floor : "No")}

Language: {Game.Lang}
Passed tutorial: {(Game.Tutorial ? "No" : "Yes")}
Veteran mode: {(Game.Veteran ? "Yes" : "No")}

Game Flags
{gamevars}";
	}

	public void SetSpecific(string specific)
	{
		specificScene = specific;
	}

	public void LoadSpecific()
	{
		LoadScene(specificScene);
	}

    public void LoadScene(string name)
	{
		loading = true;
		SceneManager.LoadScene(name);
	}

	public void Discard()
	{
		if (!gameLoaded)
		{
			Game.PartyRemove(0);
			Game.PartyRemove(1);
		}

		LoadScene("Titlescreen");
	}

	public void Erase()
	{
		if (!Game.Erase())
			return;

		SetDefaultTeam();
		GameObject.Find("teamset_status").GetComponent<Text>().text = "";

		gameLoaded = false;
	}

	public void SetDefaultTeam()
	{
		GameObject.Find("teamset_leader_id").GetComponent<InputField>().text = "ROCKRUFF";
		GameObject.Find("teamset_leader_level").GetComponent<InputField>().text = "5";
		GameObject.Find("teamset_partner_id").GetComponent<InputField>().text = "AXEW";
		GameObject.Find("teamset_partner_level").GetComponent<InputField>().text = "5";

		SetTeam();
	}

	public void SetTeam()
	{
		var status = GameObject.Find("teamset_status").GetComponent<Text>();

		try
		{
			var leader = GameObject.Find("teamset_leader_id").GetComponent<InputField>().text;

			if (!byte.TryParse(GameObject.Find("teamset_leader_level").GetComponent<InputField>().text, out var level))
			{
				status.text = TextColor.Colorize("The leader starting level could not be parsed.", TextColor.darkOrange);
				return;
			}

			var partner = GameObject.Find("teamset_partner_id").GetComponent<InputField>().text;

			PokemonState state = new PokemonState(leader.As<Species>(), level);

			Game.PartyRemove(0);
			Game.PartyAdd(state);
			Game.Protagonist = 0;

			Game.TeamAddIndex(0);

			if (!string.IsNullOrWhiteSpace(partner))
			{
				if (!byte.TryParse(GameObject.Find("teamset_partner_level").GetComponent<InputField>().text, out var partnerLevel))
				{
					status.text = TextColor.Colorize("The partner starting level could not be parsed.", TextColor.darkOrange);
					return;
				}

				state = new PokemonState(partner.As<Species>(), partnerLevel);

				Game.PartyRemove(1);
				Game.PartyAdd(state);
				Game.Partner = 1;

				Game.TeamAddIndex(1);
			}

			status.text = "Starting team was changed.";
		}
		catch (DataNotFoundException)
		{
			status.text = TextColor.Colorize("One of the IDs does not exist in the database.", TextColor.darkOrange);
		}
	}
}
#endif