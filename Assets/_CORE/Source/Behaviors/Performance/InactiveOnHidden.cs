﻿using System;
using UnityEngine;

public class InactiveOnHidden : MonoBehaviour
{
	[Serializable]
	public enum ComponentCategory
	{
		Renderers,
		RenderersAndPhysics
	}

	#region Component fields
	public ComponentCategory concernedComponents = ComponentCategory.RenderersAndPhysics;
	[Range(0f, 1f)] public float threshold = 0.3f;
	#endregion

	public bool Visible { get; private set; }

	void Awake()
	{
		Visible = true;
	}

	void Update()
    {
		var camVector = Camera.main.WorldToViewportPoint(transform.position);
		var v = camVector.x >= -threshold && camVector.x <= 1f + threshold && camVector.y >= -threshold && camVector.y <= 1f + threshold && camVector.z >= -threshold;

		if (Visible != v)
		{
			Visible = v;

			foreach (var c in GetComponentsInChildren<Renderer>())
				c.enabled = v;

			if (concernedComponents == ComponentCategory.RenderersAndPhysics)
			{
				foreach (var c in GetComponentsInChildren<Collider>())
					c.enabled = v;
			}
		}
	}
}
