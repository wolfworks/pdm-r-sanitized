﻿using UnityEngine;
using UnityEngine.Events;

public class TimeTrigger : MonoBehaviour
{
	public UnityEvent method;
	public float time;
	public bool startImmediately = true;

	void Start()
	{
		if (startImmediately)
			Begin();
	}

	public void Begin()
	{
		Invoke("Execute", time);
	}

	private void Execute()
	{
		method.Invoke();
	}
}