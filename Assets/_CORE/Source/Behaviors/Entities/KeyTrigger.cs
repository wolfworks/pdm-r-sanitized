﻿using UnityEngine;
using UnityEngine.Events;

public class KeyTrigger : MonoBehaviour
{
	public string[] buttons;
	public bool OR;
	public UnityEvent method;
	public bool once;

	private bool end;

	void Start()
	{
		end = false;
	}

	void Update()
	{
		if ((once && !end) || !once)
		{
			bool trigger = !OR;

			foreach (string s in buttons)
			{
				if (OR)
					trigger = (trigger || Input.GetButtonDown(s));
				else
					trigger = (trigger && Input.GetButtonDown(s));
			}

			if (trigger)
			{
				end = true;
				Execute();
			}
		}
	}

	private void Execute()
	{
		method.Invoke();
	}
}
