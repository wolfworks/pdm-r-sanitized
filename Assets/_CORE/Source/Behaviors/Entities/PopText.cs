﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PopText : MonoBehaviour
{
	[Serializable]
	public struct MesRef
	{
		public string reference;
		public bool hasSound;
	}

	[Serializable]
	public class SendChoice : UnityEvent<int,int> {}

	public Dialogue dialogue;

	public MesRef[] messages;

	public UnityEvent onEnd;
	public SendChoice onChoose;

	private int current = 0;
	private bool start = false;

	void Update()
	{
		if (start && current < messages.Length && dialogue.HasDialogueEnded())
		{
			Mes mes = GameText.GetEntry(messages[current].reference);

			dialogue.PopDialogue(GameText.ParseGamevars(mes.message),
				(current != messages.Length - 1),
				messages[current].hasSound,
				(mes.choices == null ? (new string[0]) : mes.choices),
				SelectChoice);

			current++;
		}
		else if (start && current == messages.Length && dialogue.HasDialogueEnded())
		{
			start = false;
			onEnd.Invoke();
		}
	}

	public int GetCurrent()
	{
		return current;
	}

	public void Pop()
	{
		current = 0;
		start = true;
	}

	public void SelectChoice(int id)
	{
		onChoose.Invoke(id, current-1);
	}
}