﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
	public string level;

	public void ChangeLevel()
	{
		SceneManager.LoadScene(level);
	}
}