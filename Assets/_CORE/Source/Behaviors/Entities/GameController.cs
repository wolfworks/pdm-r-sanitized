﻿using UnityEngine;

public class GameController : MonoBehaviour
{
	public void ResetGame()
	{
		Game.Reset();
	}
 
	public void LoadGame()
	{
		Game.Load();
	}

	public void SaveGame(int sceneIndex)
	{
		Game.Save(sceneIndex);
	}

	public void EraseGame()
	{
		Game.Erase();
	}

	public void ExitGame(float time = 0f)
	{
		Invoke("Quit", time);
	}

	public void Quit()
	{
		Application.Quit();
	}
}