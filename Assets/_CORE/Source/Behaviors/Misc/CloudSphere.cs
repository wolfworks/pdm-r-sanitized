﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

public class CloudSphere : MonoBehaviour
{
	[Serializable]
	public struct Cloud
	{
		public GameObject gameObject;
		public float speedMultiplier;
		public float speedWobble;
		public float wobbleDelay;
		public float wobbleFrequency;
	}

	#region Component Fields
	public Vector3 center = Vector2.zero;
	public float radius = 1000f;
	public int amount = 10;
	public float spreadDistance = 200f;
	public float centerAvoidance = 200f;

	public Vector2 baseSize = Vector2.one * 300;
	public float sizeVariation = 50;

	public float moveSpeed = 2;
	public float minSpeedMultiplier = 0.9f;
	public float maxSpeedMultiplier = 1.1f;
	public float minWobbleFrequency = 0.2f;
	public float maxWobbleFrequency = 0.8f;

	public List<Material> materials = new List<Material>();
	#endregion

	public List<Cloud> clouds = new List<Cloud>();

	public void AddCloud(GameObject cloud)
	{
		clouds.Add(new Cloud
		{
			gameObject = cloud,
			speedMultiplier = Random.Range(minSpeedMultiplier, maxSpeedMultiplier),
			speedWobble = 1f,
			wobbleDelay = Random.Range(0f, 5f),
			wobbleFrequency = Random.Range(minWobbleFrequency, maxWobbleFrequency)
		});
	}

	#region Component Messages
	private void Update()
	{
		if (moveSpeed == 0)
			return;

		for (var i = 0; i < clouds.Count; ++i)
		{
			var cloud = clouds[i];

			cloud.speedWobble = (Mathf.Cos(Time.time + cloud.wobbleDelay * cloud.wobbleFrequency) + 9) / 10;

			var finalSpeed = moveSpeed * cloud.speedMultiplier * cloud.speedWobble * Time.deltaTime;

			cloud.gameObject.transform.RotateAround(center, Vector3.up, finalSpeed / radius * Mathf.PI * Mathf.Rad2Deg);
		}
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(center, radius);
	}
	#endregion
}