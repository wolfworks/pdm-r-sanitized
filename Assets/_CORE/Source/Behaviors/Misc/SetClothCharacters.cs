﻿using Character;
using System.Collections.Generic;
using UnityEngine;

public class SetClothCharacters : MonoBehaviour
{
	private const float FADE_SPEED = 2f;
	private const float MAX_FADE = 0.75f;
	public AbstractCharacter[] characters;

	private Transform playerTransform;
	private Transform cameraTransform;
	private new Renderer renderer;

	private int dir = -1;

	void Start()
	{
		playerTransform = GameObject.FindWithTag("Player").transform;
		cameraTransform = Camera.main.transform;

		renderer = GetComponent<Renderer>();

		var cloth = GetComponent<Cloth>();
		var list = new List<CapsuleCollider>(cloth.capsuleColliders);

		foreach (var c in characters)
			list.Add(c.GetManager().Collider as CapsuleCollider);

		cloth.capsuleColliders = list.ToArray();
	}

	void Update()
	{

		if (Physics.Raycast(cameraTransform.position, playerTransform.position - cameraTransform.position, out var hit))
		{
			if (hit.transform == transform)
				dir = 1;
			else
				dir = -1;
		}
		else
		{
			dir = -1;
		}

		var val = renderer.material.GetFloat("_Transparency");
		val = Mathf.Clamp(val + (FADE_SPEED * dir * Time.deltaTime), 0f, MAX_FADE);
		renderer.material.SetFloat("_Transparency", val);
	}
}