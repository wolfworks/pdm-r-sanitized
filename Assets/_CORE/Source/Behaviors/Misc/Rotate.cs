﻿using UnityEngine;

public class Rotate : MonoBehaviour
{
	public Vector3 axis;

	void Update()
	{
		transform.Rotate(axis * Time.deltaTime * 10);
	}
}