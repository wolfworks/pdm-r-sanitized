﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class NightLight : MonoBehaviour
{
	public Vector3 nightSunRotation;
	public Material daySky;
	public Material nightSky;
	public bool isSun;

	private Light lightComponent;
	private Quaternion startRotation;
	private bool nightMemory;

	private void Start()
	{
		lightComponent = GetComponent<Light>();
		startRotation = transform.rotation;
		nightMemory = Game.NightTime;

		UpdateLight();
	}

	private void Update()
	{
		if (Game.NightTime != nightMemory)
		{
			UpdateLight();
			nightMemory = Game.NightTime;
		}
	}

	private void UpdateLight()
	{
		if (Game.NightTime)
		{
			lightComponent.enabled = !isSun;
			transform.rotation = Quaternion.Euler(nightSunRotation);

			if (isSun)
				RenderSettings.skybox = nightSky;
		}
		else
		{
			lightComponent.enabled = isSun;
			transform.rotation = startRotation;

			if (isSun)
				RenderSettings.skybox = daySky;
		}
	}
}