﻿using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class NightMaterial : MonoBehaviour
{
	public Material dayMat;
	public Material nightMat;

	private Renderer rendererComponent;
	private bool nightMemory;

	private void Start()
	{
		rendererComponent = GetComponent<Renderer>();
		nightMemory = Game.NightTime;

		UpdateMaterials();
	}

	private void Update()
	{
		if (Game.NightTime != nightMemory)
		{
			UpdateMaterials();
			nightMemory = Game.NightTime;
		}
	}

	private void UpdateMaterials()
	{
		if (Game.NightTime)
			rendererComponent.material = nightMat;
		else
			rendererComponent.material = dayMat;
	}
}