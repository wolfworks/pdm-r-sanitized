﻿using UnityEngine;

public class ParticleHandler : MonoBehaviour
{
	public ParticleSystem ps;

	public void Play(float speed)
	{
		var main = ps.main;
		main.simulationSpeed = speed;

		ps.Play();
	}

	public void Stop()
	{
		ps.Stop();
	}

	public void Pause()
	{
		ps.Pause();
	}
}