﻿using System;
using UnityEngine;

public class AnimTriggerRandom : StateMachineBehaviour
{
	#region Component fields
	public string triggerName;
	[Range(0.01f, 1f)]
	public float frequency = 0.4f;
	public float minimumCooldown;
	#endregion

	private DateTime LastAnimationTime { get; set; }

	public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
	{
		if ((DateTime.Now - LastAnimationTime).TotalSeconds < minimumCooldown) return;

		var r = RandomFromDistribution.RandomFromExponentialDistribution(2f, RandomFromDistribution.Direction_e.Right);

		if (r <= frequency)
		{
			animator.SetTrigger(triggerName);
			LastAnimationTime = DateTime.Now;
		}
	}
}