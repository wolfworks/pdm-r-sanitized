﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class FrozenParticles : MonoBehaviour
{
	public float spawnDelay = 0.1f;
	public bool withChildren = true;
	
	private ParticleSystem ps;

	private void Start()
	{
		ps = GetComponent<ParticleSystem>();
		StartCoroutine(FreezeParticles());
	}

	private IEnumerator FreezeParticles()
	{
		yield return new WaitForSeconds(spawnDelay);

		ps.Pause(withChildren);
	}
}