﻿using UnityEngine;

public class ChestColor : MonoBehaviour
{
	public Color color;

	void Start()
	{
		transform.GetChild(1).GetComponent<MeshRenderer>().material.SetColor("_Color", color);
	}
}