﻿using Character.Handler;
using Character.Lexic;
using UnityEngine;

public class EmotionSynchronizer : StateMachineBehaviour
{
	public CharacterEmotion emotion;

	private CharacterEmotion emotionBeforeEnter;

	override public void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
	{
		var emotionHandler = animator.transform.parent.GetComponent<CharacterEmotionHandler>();

		emotionBeforeEnter = emotionHandler.CurrentEmotion;
		emotionHandler.CurrentEmotion = emotion;
	}

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		var emotionHandler = animator.transform.parent.GetComponent<CharacterEmotionHandler>();

		emotionHandler.CurrentEmotion = emotionBeforeEnter;
	}
}