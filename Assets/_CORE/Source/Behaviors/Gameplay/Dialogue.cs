﻿using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using FMODUnity;
using FMOD.Studio;

public class Dialogue : MonoBehaviour
{
	[Serializable]
	public enum DialoguePosition
	{
		Bottom,
		Top
	}

	public GameObject textBox;
	public Text textObject;
	public GameObject endIcon;
	public DialoguePosition position;
	public bool mirroredPortrait;

	[Header("Choice Window")]
	public CanvasGroup choicePane;
	public GameObject button;

	[Header("Portrait Window")]
	public GameObject portraitWindow;
	public Image portraitImage;

	[Header("Sound")]
	[EventRef]
	public string speechSound;

	public delegate void ChoiceCallback(int id);

	private string text;
	private bool serie = false;
	private bool endDisplay = false;
	private bool dialogueEnded = true;
	private bool firstTime = true;

	private bool hasChoice = false;
	private string[] choiceOptions;
	private ChoiceCallback choiceCallback;

	private bool hasPortrait = false;
	private Sprite portrait;

	private bool hasName = false;
	private string dname;

	private bool hasSound = false;

	private DialoguePosition previousPosition;
	private bool previousPortraitState;

	private EventInstance speechState;

	void Start()
	{
		textBox.SetActive(false);
		textObject.text = "";

		choicePane.alpha = 0;

		previousPosition = position;
		previousPortraitState = mirroredPortrait;

		speechState = RuntimeManager.CreateInstance(speechSound);
	}

	void Update()
	{
		if ((hasChoice || Input.GetButton("Submit")) && endDisplay)
		{
			textObject.text = "";

			hasChoice = false;
			hasPortrait = false;
			hasName = false;

			if (!serie)
			{
				CloseDialogue();
			}
			else
			{
				endDisplay = false;
				dialogueEnded = true;
			}
		}

		if (position != previousPosition || previousPortraitState != mirroredPortrait)
		{
			previousPosition = position;
			previousPortraitState = mirroredPortrait;

			var tBox = textBox.GetComponent<RectTransform>();
			var tPortrait = portraitWindow.GetComponent<RectTransform>();
			var tChoice = choicePane.GetComponent<RectTransform>();

			tPortrait.localScale = new Vector3(mirroredPortrait ? -1f : 1f, 1f, 1f);

			switch (position)
			{
				case DialoguePosition.Bottom:
					tBox.anchorMin = new Vector2(.5f, 0f);
					tBox.anchorMax = new Vector2(.5f, 0f);
					tBox.anchoredPosition = new Vector2(0f, 70f);

					tPortrait.anchorMin = new Vector2(mirroredPortrait ? 1f : 0f, 1f);
					tPortrait.anchorMax = new Vector2(mirroredPortrait ? 1f : 0f, 1f);
					tPortrait.anchoredPosition = new Vector2(0f, 8f);
					tPortrait.pivot = new Vector2(0f, 0f);

					tChoice.anchorMin = new Vector2(1f, 1f);
					tChoice.anchorMax = new Vector2(1f, 1f);
					tChoice.pivot = new Vector2(1f, 0f);
					break;

				case DialoguePosition.Top:
					tBox.anchorMin = new Vector2(.5f, 1f);
					tBox.anchorMax = new Vector2(.5f, 1f);
					tBox.anchoredPosition = new Vector2(0f, -70f);

					tPortrait.anchorMin = new Vector2(mirroredPortrait ? 1f : 0f, 0f);
					tPortrait.anchorMax = new Vector2(mirroredPortrait ? 1f : 0f, 0f);
					tPortrait.anchoredPosition = new Vector2(0f, -8f);
					tPortrait.pivot = new Vector2(0f, 1f);

					tChoice.anchorMin = new Vector2(1f, 0f);
					tChoice.anchorMax = new Vector2(1f, 0f);
					tChoice.pivot = new Vector2(1f, 1f);
					break;
			}
		}
	}

	public void PopDialogue(string message, bool nextIncoming = false, bool withSound = true, string[] choices = null, ChoiceCallback callback = null, Sprite image = null, string n = "", float soundPitch = 1f)
	{
		dialogueEnded = false;

		endIcon.SetActive(false);
		portraitWindow.SetActive(false);
		textBox.SetActive(true);
		text = message;

		serie = nextIncoming;

		speechState.setParameterByName("Speech Pitch", soundPitch);

		if (choices.Length > 0)
		{
			hasChoice = true;
			choiceOptions = choices;
			choiceCallback = callback;
		}

		if (image != null)
		{
			hasPortrait = true;
			portrait = image;
		}

		if (!string.IsNullOrEmpty(n))
		{
			hasName = true;
			dname = n;
		}

		hasSound = withSound;

		StartCoroutine(DisplayMessage());

		if (hasSound)
			speechState.start();
	}

	public bool HasDialogueEnded()
	{
		return dialogueEnded;
	}

	private void SetChoice(string[] choices, ChoiceCallback callback)
	{
		GameObject first = null;

		for (int i = 0; i < choices.Length; ++i)
		{
			GameObject obj = Instantiate(button, choicePane.transform);

			if (first == null)
				first = obj;

			obj.GetComponent<Text>().text = choices[i];

			int index = i;
			obj.GetComponent<Button>().onClick.AddListener(() =>
			{
				endDisplay = true;

				foreach (Transform child in choicePane.transform)
					Destroy(child.gameObject, 0.04f);

				choicePane.alpha = 0;

				callback(index);
			});
		}

		EventSystem.current.SetSelectedGameObject(first);
		choicePane.alpha = 1;
	}

	private IEnumerator DisplayMessage()
	{
		int pos = 0;
		bool usingColor = false; // is the text colored?

		if (hasName)
		{
			text = text.Insert(0, dname + " : ");
			textObject.text = "<color=" + TextColor.blue + ">" + dname + "</color> : ";
		}

		// check if we need to insert newlines
		float pixToPoints = textObject.fontSize * GetComponentInParent<CanvasScaler>().referencePixelsPerUnit / Screen.dpi;
		pixToPoints /= 1.9f;

		int currentPos = 0;

		foreach (string l in text.Split('#'))
		{
			float currentWidth = 0f;

			foreach (string s in l.Split(' '))
			{
				Regex rxp = new Regex("\\|\\d|~.");
				string word = rxp.Replace(s, "");

				currentWidth += word.Length * pixToPoints;

				if (currentWidth > textObject.rectTransform.rect.width)
				{
					int index = text.GetNthIndex(' ', currentPos);

					if (index != -1)
					{
						text = text.ReplaceAt(index, '#');
						currentWidth = 0f;
					}
				}

				++currentPos;
			}
		}

		if (firstTime)
		{
			textBox.GetComponent<Animator>().Play("DialogueBoxPop");

			yield return new WaitForSeconds(0.1f);

			firstTime = false;
		}

		if (hasPortrait)
		{
			portraitImage.sprite = portrait;
			portraitWindow.SetActive(true);
		}

		for (int i = (hasName ? dname.Length + 3 : 0); i < text.Length; ++i)
		{
			pos = (usingColor ? textObject.text.LastIndexOf("</color>") : textObject.text.Length);

			switch (text[i])
			{
				case ' ': // dont wait for space
					textObject.text = textObject.text.Insert(pos, " ");
					break;

				case '#': // new line
					textObject.text = textObject.text.Insert(pos, "\n");
					break;

				case '|': // wait a bit (number / 10)
					if (hasSound) speechState.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

					yield return new WaitForSeconds((float)(char.GetNumericValue(text[i + 1]) / 10));
					i++;

					if (hasSound) speechState.start();
					break;

				case '~': // color
					switch (text[i + 1])
					{
						case 'w': // white (clears color)
							usingColor = false;
							break;

						case 'c': // cyan
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.cyan + "></color>");
							usingColor = true;
							break;

						case 'r': // red
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.red + "></color>");
							usingColor = true;
							break;

						case 'b': // blue
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.blue + "></color>");
							usingColor = true;
							break;

						case 'g': // green
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.green + "></color>");
							usingColor = true;
							break;

						case 'o': // orange
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.orange + "></color>");
							usingColor = true;
							break;

						case 'm': // magenta
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.magenta + "></color>");
							usingColor = true;
							break;

						case 'y': // yellow
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.yellow + "></color>");
							usingColor = true;
							break;

						case 'p': // pink
							textObject.text = textObject.text.Insert(pos, "<color=" + TextColor.pink + "></color>");
							usingColor = true;
							break;
					}

					i++;
					break;

				default: // regular letter
					yield return new WaitForSeconds(Input.GetButton("Submit") ? 0.01f : 0.02f);
					textObject.text = textObject.text.Insert(pos, text[i].ToString());
					break;
			}
		}

		speechState.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

		if (hasChoice)
		{
			SetChoice(choiceOptions, choiceCallback);
		}
		else
		{
			endIcon.SetActive(true);
			endDisplay = true;
		}
	}

	private void CloseDialogue()
	{
		endDisplay = false;

		textBox.SetActive(false);
		portraitWindow.SetActive(false);

		choicePane.alpha = 0;

		endIcon.SetActive(false);
		textObject.text = "";

		dialogueEnded = true;
		firstTime = true;
	}
}
