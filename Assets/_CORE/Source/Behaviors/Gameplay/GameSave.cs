﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

public class GameSave : MonoBehaviour
{
	private bool saveCompleted;
	private Animator icon;
	
	public void Save()
	{
		saveCompleted = false;
		icon = GameObject.FindWithTag("Autosave").GetComponent<Animator>();

		StartCoroutine(SaveCoroutine());
	}

	public bool HasSavingEnded()
	{
		return saveCompleted;
	}
	
	private IEnumerator SaveCoroutine()
	{
		icon.SetBool("Visible", true);

		yield return new WaitForSeconds(1f);

		var saveResult = Game.Save();
		
		if (saveResult != 0)
			icon.SetTrigger("Error");

		icon.SetBool("Visible", false);

		yield return new WaitForSeconds(0.33f);

		saveCompleted = true;
	}
}