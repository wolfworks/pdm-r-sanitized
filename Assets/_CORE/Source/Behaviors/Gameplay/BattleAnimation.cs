﻿using Dungeon.Entity;
using System;
using UnityEngine;

using Object = UnityEngine.Object;

public class BattleAnimation : MonoBehaviour
{
	private Animation anim;
	private DungeonCharacter user;
	private DungeonCharacter target;
	private Action<DungeonCharacter, DungeonCharacter> mark;
	private bool isAlternative;

	#region Events
	public void AnimationMark()
	{
		mark?.Invoke(user, target);
	}

	public void TriggerUser(string trigger)
	{
		if (user == null) return;

		if (isAlternative && trigger.Contains("Attack"))
			trigger = "Attack Alternative";

		user.character.AnimationHandler.SetTrigger(trigger);
	}

	public void TriggerTarget(string trigger)
	{
		if (target == null) return;
		target.character.AnimationHandler.SetTrigger(trigger);
	}

	public void SummonUser(Object obj)
	{
		if (user == null) return;
		var pos = user.character.Collider.bounds.center;
		Instantiate(obj, pos, Quaternion.identity);
	}

	public void SummonTarget(Object obj)
	{
		if (target == null) return;
		var pos = target.character.Collider.bounds.center;
		Instantiate(obj, pos, Quaternion.identity);
	}

	public void SummonUserFeet(Object obj)
	{
		if (user == null) return;
		var pos = user.character.transform.position;
		Instantiate(obj, pos, Quaternion.identity);
	}

	public void SummonTargetFeet(Object obj)
	{
		if (target == null) return;
		var pos = target.character.transform.position;
		Instantiate(obj, pos, Quaternion.identity);
	}

	public void SummonUserHead(Object obj)
	{
		if (user == null) return;
		var head = user.character.head;
		Instantiate(obj, head.position, Quaternion.LookRotation(head.forward.normalized, Vector3.up));
	}

	public void SummonTargetHead(Object obj)
	{
		if (target == null) return;
		var head = target.character.head;
		Instantiate(obj, head.position, Quaternion.LookRotation(head.forward.normalized, Vector3.up));
	}

	public void PlaySound(Object sound)
	{
		// TODO: Use FMOD
		
		if (sound is AudioClip clip)
			GameObject.FindWithTag("DungeonManager").GetComponent<AudioSource>().PlayOneShot(clip);
	}
	#endregion

	#region Component Methods
	void Awake()
	{
		anim = gameObject.AddComponent<Animation>();
	}

	void LateUpdate()
	{
		var userDone = user == null || !user.character.AnimationHandler.IsInImportantAnimation;
		var targetDone = target == null || !target.character.AnimationHandler.IsInImportantAnimation;

		if (!anim.isPlaying && userDone && targetDone)
		{
			Destroy(anim);
			Destroy(this);
		}
	}
	#endregion

	#region Static Methods
	public static BattleAnimation Animate(DungeonCharacter _user, DungeonCharacter _target, AnimationClip _clip, Action<DungeonCharacter, DungeonCharacter> _mark = null, bool _isAlternative = false)
	{
		if (_user.character.GetComponent<BattleAnimation>() != null) return null;
	
		var ba = _user.Transform.GetChild(0).gameObject.AddComponent<BattleAnimation>();
		ba.user = _user;
		ba.target = _target;
		ba.mark = _mark;
		ba.isAlternative = _isAlternative;

		ba.anim.AddClip(_clip, "clip");
		ba.anim.Play("clip");

		return ba;
	}
	#endregion
}