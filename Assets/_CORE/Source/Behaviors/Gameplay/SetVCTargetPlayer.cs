﻿using UnityEngine;
using Cinemachine;

public class SetVCTargetPlayer : MonoBehaviour
{
	public bool bindOnStart = true;

	private CinemachineVirtualCamera virtualCamera;
	private CinemachineOrbitalTransposer orbital;
	private PlayerController_old player;

	void Start()
	{
		Cursor.visible = false;

		virtualCamera = GetComponent<CinemachineVirtualCamera>();
		orbital = virtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();

		player = GameObject.FindWithTag("Player").GetComponent<PlayerController_old>();

		if (bindOnStart) BindTarget();
	}

	public void BindTarget()
	{
		if (player == null) return;

		var target = player.GetController();

		virtualCamera.Follow = target.transform;
		virtualCamera.LookAt = target.transform;

		orbital.m_FollowOffset = new Vector3(0f, 1f + target.radius, -10f * target.radius);
	}

	public void UnbindTarget()
	{
		virtualCamera.Follow = null;
		virtualCamera.LookAt = null;
	}
}