﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NameField : MonoBehaviour
{
	#region Component Fields
	public GameObject panel;
	public Text messageText;
	public InputField field;
	#endregion

	public void AskName(string messageReference, int? partyIndex = null, UnityAction<string> callback = null)
	{
		messageText.text = GameText.GetString(messageReference);

		if (callback != null)
			field.onEndEdit.AddListener(callback);

		field.onEndEdit.AddListener(name =>
		{
			if (partyIndex.HasValue)
				Game.Party(partyIndex.Value).nickname = name;
			else
				Game.TeamName = name;

			panel.SetActive(false);
		});

		panel.SetActive(true);
	}
}