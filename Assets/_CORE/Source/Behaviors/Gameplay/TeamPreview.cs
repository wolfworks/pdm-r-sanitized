﻿using Character;
using Pokemon.Data;
using UnityEngine;

public class TeamPreview : MonoBehaviour
{
	public Transform spawner;

	private CharacterManager currentEntity;
	private bool created = false;

	private void Awake()
	{
		if (!created)
		{
			DontDestroyOnLoad(gameObject);
			created = true;
		}
	}

	public void SpawnEntity(string id, bool female = false)
	{
		if (currentEntity != null)
			Destroy(currentEntity.gameObject);

		var species = id.As<Species>();

		GameObject model;

		if (female)
			model = species.femaleModel != null ? species.femaleModel : species.maleModel;
		else
			model = species.maleModel;

		var radius = model.GetComponent<CapsuleCollider>().radius;

		spawner.localPosition = new Vector3(0f, -radius, radius * 5f);

		currentEntity = Instantiate(model, spawner).GetComponent<CharacterManager>();

		currentEntity.Rigidbody.useGravity = false;
		currentEntity.Rigidbody.isKinematic = true;
		currentEntity.MovementHandler.UseGravity = false;
		currentEntity.handleAnimation = false;
		currentEntity.alwaysVisible = true;

		currentEntity.transform.SetLayerRecursively(13);
	}

	public void DespawnEntity()
	{
		if (currentEntity != null)
			Destroy(currentEntity.gameObject);
	}
}