﻿using UnityEngine;

public class CharacterWanderer : AbstractCharacterManager
{
	public bool active = true;
	public float speed;
	[Range(0.01f, 1f)]
	public float frequency = 0.4f;
	public bool waitUntilReached = true;
	public Bounds region;

	private Bounds realBounds;
	private Vector3 currentObjective;

	public void StartWandering()
	{
		if (active)
			return;

		active = true;
		MakeNewObjective();
	}

	public void StopWandering()
	{
		active = false;
		Controller.Stop();
	}

	void Start()
	{
		realBounds = new Bounds(transform.position + region.center, region.extents);
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0f, 1f, 1f, 0.5f);
		Gizmos.DrawWireCube(transform.position + region.center, region.extents);
	}

	void Update()
	{
		if (!active)
			return;

		if (!waitUntilReached || Controller.MoveEnded)
		{
			var r = RandomFromDistribution.RandomFromExponentialDistribution(2f, RandomFromDistribution.Direction_e.Right);

			if (r <= frequency)
				MakeNewObjective();
		}
	}

	private void MakeNewObjective()
	{
		currentObjective = new Vector3
		(
			Random.Range(realBounds.min.x, realBounds.max.x),
			Random.Range(realBounds.min.y, realBounds.max.y),
			Random.Range(realBounds.min.z, realBounds.max.z)
		);

		Controller.GoTo(currentObjective, speed);
	}
}