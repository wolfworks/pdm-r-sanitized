﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharController : MonoBehaviour
{
	const float DIST_CHECK_GROUND = 0.25f;
	const float PROXIMITY_THRESHOLD = 0.1f;
	const float DIRECTION_THRESHOLD = 5f;
	const float MAX_LOOK_ANGLE = 45f;
	const float WALKING_THRESHOLD = 0.2f;
	const float RUNNING_MULTIPLIER = 10f;
	const float MAX_ALTITUDE = 0.2f;
	const float ALTITUDE_SPEED = 0.05f;

	#region Component Fields
	public float turnSmoothing = 15f;
	public bool alwaysVisible = false;
	public bool gravity = true;
	public bool handleAnimation = true;
	public float dungeonScale = 1f;
	public bool lookAtPlayer = false;
	public bool flyWhenWalking = false;
	public bool flyWhenRunning = false;

	public AnimationClip[] animationClips;
	#endregion

	private CharacterController controller;
	private AnimatorOverrideController animatorOverride;
	private PlayerController_old player;

	private Vector3 movement;
	private Vector3 lookDirection;
	private bool onGround;
	private bool landing;
	private float altitude;

	// Movement control from outside
	private Vector3? goTo;
	private float goToSpeed;
	private Quaternion? lookTowards;
	private float lookTowardsSpeed;

	public Animator Anim { get; private set; }
	public bool Visible { get; private set; }
	public float FaintTime { get; private set; }

	#region Component Methods
	void Start()
	{
		var playerExists = GameObject.FindWithTag("Player");

		if (playerExists)
			player = playerExists.GetComponent<PlayerController_old>();

		controller = GetComponent<CharacterController>();
		Anim = GetComponentInChildren<Animator>();

		animatorOverride = new AnimatorOverrideController(Anim.runtimeAnimatorController);
		Anim.runtimeAnimatorController = animatorOverride;

		LoadClips();

		movement = Vector3.zero;
		onGround = true;
		landing = false;
		goTo = null;
		lookTowards = null;

		CheckVisibility(true);
	}

	void FixedUpdate()
	{
		var move = Vector3.zero;

		if (gravity)
		{
			var rayStart = controller.transform.position + (Vector3.up * (DIST_CHECK_GROUND / 2f));
   
			onGround = Physics.Raycast(rayStart, Vector3.down, DIST_CHECK_GROUND);
			Debug.DrawRay(rayStart, Vector3.down * DIST_CHECK_GROUND);

			if (!onGround && !landing)
				landing = true;
			else if (onGround && landing)
				StartCoroutine(LandingCooldown());
		}

		if (!landing || !onGround || !gravity)
		{
			move = movement;

			if (!onGround && gravity)
				move /= 2f;

			if (move != Vector3.zero)
				LookDirection(move);

			if (lookTowards == null && lookDirection.magnitude > 0)
			{
				var targetRotation = Quaternion.LookRotation(lookDirection, Vector3.up);
				var newRotation = Quaternion.Lerp(transform.rotation, targetRotation, turnSmoothing * Time.fixedDeltaTime);

				transform.rotation = newRotation;
			}
		}

		controller.Move((move + (gravity ? Vector3.down * 5f : Vector3.zero)) * Time.fixedDeltaTime);

		if (handleAnimation)
			Anim.SetBool("Grounded", onGround || !gravity);

		movement = Vector3.zero;
	}

	void Update()
	{
		if (goTo != null)
		{
			movement = (goTo.Value - transform.position).normalized * goToSpeed;
			Anim.SetBool("Walking", IsWalking());
			Anim.SetBool("Running", IsRunning());

			if (Vector3.Distance(goTo.Value, transform.position) < PROXIMITY_THRESHOLD)
			{
				goTo = null;
				Anim.SetBool("Walking", false);
				Anim.SetBool("Running", false);
			}
		}
		else if (lookTowards != null)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, lookTowards.Value, lookTowardsSpeed * Time.deltaTime);
			Anim.SetBool("Walking", true);

			if (Quaternion.Angle(transform.rotation, lookTowards.Value) < DIRECTION_THRESHOLD)
			{
				lookDirection = transform.forward;
				lookTowards = null;
				Anim.SetBool("Walking", false);
			}
		}

		if (lookAtPlayer && !CompareTag("Player"))
			LookAtPlayer();

		CheckVisibility();
	}

	private void LateUpdate()
	{
		var flying = (flyWhenWalking && IsWalking()) || (flyWhenRunning && IsRunning());

		if (flying)
		{
			altitude = Mathf.Lerp(altitude, controller.radius + MAX_ALTITUDE, ALTITUDE_SPEED);
			Anim.transform.position += transform.up * altitude;
		}
		else
		{
			altitude = Mathf.Lerp(altitude, 0f, ALTITUDE_SPEED);
			Anim.transform.position += transform.up * altitude;
		}
	}
	#endregion

	public void Move(Vector3 vect)
	{
		if (!MoveEnded)
			return;

		movement = vect;

		if (handleAnimation && Anim != null)
		{
			Anim.SetBool("Walking", IsWalking());
			Anim.SetBool("Running", IsRunning());
		}
	}

	public bool MoveEnded => goTo == null && lookTowards == null;

	public void LookDirection(Vector3 direction)
	{
		direction.y = 0f;
		lookDirection = direction;
	}

	public void GoTo(Vector3 to, float speed)
	{
		goTo = to;
		goToSpeed = speed;
	}

	public void LookTowards(Vector3 direction, float speed)
	{
		direction.y = 0f;
		lookTowards = Quaternion.LookRotation(direction, Vector3.up);
		lookTowardsSpeed = speed;
	}

	public void Stop()
	{
		goTo = null;
		lookTowards = null;

		if (handleAnimation && Anim != null)
		{
			Anim.SetBool("Walking", false);
			Anim.SetBool("Running", false);
		}
	}

	public void InADungeon(bool on)
	{
		Anim.SetBool("In Combat", on);

		if (on)
		{
			transform.localScale = Vector3.one * dungeonScale;

			gameObject.layer = 8;
			transform.GetChild(0).SetLayerRecursively(8);
		}
		else
		{
			transform.localScale = Vector3.one;

			gameObject.layer = 0;
			transform.GetChild(0).SetLayerRecursively(0);
		}
	}

	#region Private Methods
	private void LoadClips()
	{
		var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();

		foreach (AnimationClip clip in Anim.runtimeAnimatorController.animationClips)
		{
			var newclip = animationClips.FirstOrDefault(x => x.name == clip.name);

			if (newclip != null)
			{
				overrides.Add(new KeyValuePair<AnimationClip, AnimationClip>(clip, newclip));

				if (newclip.name == "Hurt" || newclip.name == "Faint")
					FaintTime += newclip.length;
			}
		}

		animatorOverride.ApplyOverrides(overrides);
	}

	private void CheckVisibility(bool force = false)
	{
		var v = alwaysVisible;

		if (!v)
		{
			var camVector = Camera.main.WorldToViewportPoint(transform.position);

			v = camVector.x >= -0.3f
				&& camVector.x <= 1.3f
				&& camVector.y >= -0.3f
				&& camVector.y <= 1.3f
				&& camVector.z >= -0.3f;
		}

		if (Visible != v || force)
		{
			Visible = v;
			foreach (var r in GetComponentsInChildren<Renderer>())
				r.enabled = v;
		}
	}

	private void LookAtPlayer()
	{
		var playerPos = player.GetController().transform.position;

		var angle = Vector3.Angle(transform.forward, player.GetController().transform.position - transform.position);

		var wanderer = transform.GetComponent<CharacterWanderer>();

		if (Vector3.Distance(playerPos, transform.position) <= PlayerController_old.MAX_LOOKAHEAD && angle <= MAX_LOOK_ANGLE)
		{
			transform.GetComponent<EmotionHandler>().HeadLook(player.GetController().GetComponent<EmotionHandler>().head);

			if (wanderer)
				wanderer.enabled = false;

			Stop();
		}
		else
		{
			transform.GetComponent<EmotionHandler>().ResetLook();

			if (wanderer)
				wanderer.enabled = true;
		}
	}

	private bool IsWalking()
	{
		return movement.magnitude > WALKING_THRESHOLD;
	}

	private bool IsRunning()
	{
		return movement.magnitude > (RUNNING_MULTIPLIER * controller.radius + 0.1f);
	}

	private IEnumerator LandingCooldown()
	{
		yield return new WaitForSeconds(0.75f);
		landing = false;
	}
	#endregion
}