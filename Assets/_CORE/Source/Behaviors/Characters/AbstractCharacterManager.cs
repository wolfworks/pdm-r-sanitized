﻿using UnityEngine;

public abstract class AbstractCharacterManager : MonoBehaviour
{
	protected CharController Controller
	{
		get
		{
			var c = GetComponent<CharController>();

			if (c != null)
				return c;

			var potentialFollower = GetComponent<FollowerController_old>();

			if (potentialFollower != null)
			{
				return potentialFollower.GetController().GetComponent<CharController>();
			}
			else
			{
				var potentialPlayer = GetComponent<PlayerController_old>();

				if (potentialPlayer != null)
				{
					return potentialPlayer.GetController().GetComponent<CharController>();
				}
			}

			return null;
		}
	}
}