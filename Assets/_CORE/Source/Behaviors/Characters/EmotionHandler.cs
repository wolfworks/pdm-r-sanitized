﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public enum Emotion_old
{
	Neutral,
	Smiling,
	Happy,
	Serious,
	Angry,
	Shocked,
	Shouting,
	Enduring,
	Sleeping,
	Sad
}

public class EmotionHandler : MonoBehaviour
{
	const float HEAD_RESET_THRESHOLD = 2f;

	public Transform head;
	public Vector3 headInteractPoint;
	public float headOffset;

	public SkinnedMeshRenderer eyes;
	public SkinnedMeshRenderer mouth;

	public EmotionEffects effects;

	private Vector2[] eyesCoords;
	private Vector2[] mouthCoords;
	private Vector2 eyesTexStart;
	private Vector2 mouthTexStart;
	private bool eyesMirrored;
	private bool mouthMirrored;

	private Quaternion desiredHeadRot;
	private Quaternion realHeadRot;
	private Transform headLook;
	private float headSpeed = 1f;
	private int nod;

	private Vector3? goTo;

	private Emotion_old currentEmotion;
	private GameObject currentEmotionEffect;

	public Vector3 InteractPoint => head.TransformPoint(headInteractPoint);

	void Awake()
	{
		if (eyes != null)
		{
			eyesTexStart = eyes.material.GetTextureOffset("_MainTex");
			eyesMirrored = eyes.material.mainTexture.wrapMode == TextureWrapMode.Mirror;
		}

		if (mouth != null)
		{
			mouthTexStart = mouth.material.GetTextureOffset("_MainTex");
			mouthMirrored = mouth.material.mainTexture.wrapMode == TextureWrapMode.Mirror;
		}

		eyesCoords = new Vector2[]
		{
			new Vector2(0f, 0f),     // Neutral
			new Vector2(!eyesMirrored ? .5f : 1f, -.25f),  // Smiling
			new Vector2(!eyesMirrored ? .5f : 1f, -.25f),  // Happy
			new Vector2(0f, 0f),     // Serious
			new Vector2(!eyesMirrored ? .5f : 1f, 0f),     // Angry
			new Vector2(0f, 0f),     // Shocked
			new Vector2(0f, -.75f),  // Shouting
			new Vector2(0f, -.75f),  // Enduring
			new Vector2(0f, -.5f),   // Sleeping
			new Vector2(!eyesMirrored ? .5f : 1f, -.5f)    // Sad
		};

		mouthCoords = new Vector2[]
		{
			new Vector2(0f, 0f),     // Neutral
			new Vector2(0f, -.25f),  // Smiling
			new Vector2(0f, -.5f),   // Happy
			new Vector2(0f, -.75f),  // Serious
			new Vector2(0f, -.75f),  // Angry
			new Vector2(!mouthMirrored ? .5f : 1f, 0f),     // Shocked
			new Vector2(!mouthMirrored ? .5f : 1f, 0f),     // Shouting
			new Vector2(!mouthMirrored ? .5f : 1f, -.25f),  // Enduring
			new Vector2(0f, -.75f),  // Sleeping
			new Vector2(!mouthMirrored ? .5f : 1f, -.25f)   // Sad
		};

		headLook = null;
		nod = 0;
		goTo = null;
	}

	void LateUpdate()
	{
		desiredHeadRot = head.rotation;

		if (eyes != null)
		{
			var offset = eyesCoords[(int)currentEmotion];

			eyes.material.SetTextureOffset("_MainTex", eyesTexStart + offset);
		}

		if (mouth != null)
		{
			var offset = mouthCoords[(int)currentEmotion];

			mouth.material.SetTextureOffset("_MainTex", mouthTexStart + offset);
		}

		if (headLook != null && nod == 0)
		{
			var multiplier = Quaternion.Euler(0f, headOffset, 0f);

			headSpeed = 0.1f;
			desiredHeadRot = Quaternion.LookRotation(headLook.position - head.position, head.up) * multiplier;
		}

		if (nod != 0)
		{
			var multiplier = Quaternion.Euler(0f, nod, 0f);

			headSpeed = 1f;
			desiredHeadRot = head.rotation * multiplier;
		}

		if (goTo != null)
		{
			transform.position = Vector3.Lerp(transform.position, (Vector3)goTo, Time.deltaTime * 5f);
			if (Vector3.Distance(transform.position, (Vector3)goTo) < 0.1f) goTo = null;
		}

		head.rotation = Quaternion.Lerp(realHeadRot, desiredHeadRot, headSpeed);
		realHeadRot = head.rotation;

		if (Quaternion.Angle(realHeadRot, desiredHeadRot) < HEAD_RESET_THRESHOLD)
			headSpeed = 1f;
	}

	void OnDrawGizmosSelected()
	{
		if (head != null)
		{
			var multiplier = Quaternion.Euler(0f, -headOffset, 0f);
			var finalVector = Quaternion.LookRotation(head.forward, head.up) * multiplier * Vector3.forward;

			Gizmos.color = Color.cyan;
			Gizmos.DrawLine(head.position, head.position + finalVector);
			Gizmos.DrawIcon(InteractPoint, "InteractArrow");

			Gizmos.color = Color.red;
			Gizmos.DrawLine(head.position, head.position + head.right);

			Gizmos.color = Color.green;
			Gizmos.DrawLine(head.position, head.position + head.up);

			Gizmos.color = Color.blue;
			Gizmos.DrawLine(head.position, head.position + head.forward);
		}
	}

	IEnumerator StartNodSequence()
	{
		while (nod < 45)
		{
			nod += 5;
			yield return new WaitForFixedUpdate();
		}

		while (nod > 0)
		{
			nod -= 5;
			yield return new WaitForFixedUpdate();
		}

		nod = 0;
	}

	public void SetEmotion(Emotion_old e)
	{
		currentEmotion = e;
	}

	public void PopEffect(EmotionEffectType effect)
	{
		StopEffect();

		currentEmotionEffect = Instantiate(effects[effect].effect, InteractPoint, Quaternion.identity);
	}

	public void StopEffect()
	{
		if (currentEmotionEffect == null)
			return;

		Destroy(currentEmotionEffect);
		currentEmotionEffect = null;
	}

	public void HeadLook(Transform look)
	{
		headLook = look;
	}

	public void StepBack(float distance)
	{
		goTo = transform.position - transform.forward * distance;
	}

	public void ResetLook()
	{
		headLook = null;
	}

	public void Nod()
	{
		StartCoroutine(StartNodSequence());
	}
}
