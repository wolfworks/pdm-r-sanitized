﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenshotController : MonoBehaviour
{
	private const KeyCode SCREEN_KEY = KeyCode.F2;

	private static bool created = false;

	private void Awake()
	{
		if (!created)
		{
			DontDestroyOnLoad(gameObject);
			created = true;
		}
	}

	private void Update()
	{
		if (Input.GetKeyDown(SCREEN_KEY))
		{
			var now = DateTime.Now;
			var scene = SceneManager.GetActiveScene();

			ScreenCapture.CaptureScreenshot($"{Application.persistentDataPath}/screens/{now:yyyyMMddhhmmss}_{scene.name}.png");
		}
	}
}
