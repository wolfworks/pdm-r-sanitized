﻿using UnityEngine;
using Pokemon.Data;
using Gentity;
using UnityEngine.InputSystem;

public class PlayerController_old : MonoBehaviour
{
	public const float MAX_LOOKAHEAD = 2f;
	public const float SPEED_MULTIPLIER = 4f;
	public const float RUNNING_MULTIPLIER = 2f;

	#region Component Fields
	public float walkSpeed = 3f;
	public bool controllable = true;
	public bool hasGravity = true;
	public GameObject interactEffect;

	[Header("Appearance")]
	public int teamNumber = 0;
	public string overridePokemon = "";
	#endregion

	private PokemonDatabase dataManager;
	private CharController controller;
	private RaycastHit hitFront;
	private GameObject currentEffect;
	private bool hasHitFront = false;
	private Vector2 movementInput;
	private bool runInput;

	#region Component Messages
	private void Start()
	{
		dataManager = Game.GetDatabase();

		GameObject modelReference;

		if (!string.IsNullOrEmpty(overridePokemon))
			modelReference = dataManager.Get<Species>(overridePokemon).maleModel;
		else
			modelReference = Game.TeamGetPokemonAt(teamNumber).Model;

		GameObject character = Instantiate(modelReference, transform.position, transform.rotation);
		character.transform.SetLayerRecursively(gameObject.layer);

		controller = character.GetComponent<CharController>();
		controller.alwaysVisible = true;

		SetGravity(hasGravity);
	}

	private void Update()
	{
		if (controllable)
		{
			var dir = new Vector3(movementInput.x, 0f, movementInput.y);
			
			dir = GameObject.FindWithTag("MainCamera").transform.TransformDirection(dir);
			dir.y = 0f;
			dir = Vector3.ClampMagnitude(dir, 1f);

			var speed = walkSpeed * GetController().radius * SPEED_MULTIPLIER;

			if (runInput)
				speed *= RUNNING_MULTIPLIER;

			dir *= speed;
			controller.Move(dir);
		}

		hasHitFront = Physics.Raycast(controller.transform.position + Vector3.up * 0.5f, controller.transform.forward, out hitFront, MAX_LOOKAHEAD);

		if (hasHitFront)
		{
			var gentity = hitFront.transform.GetComponent<GentityEvent>();

			if (gentity != null)
			{
				if (currentEffect == null)
				{
					Vector3 finalPos;

					var isCharacter = hitFront.transform.GetComponent<EmotionHandler>();

					if (isCharacter == null)
					{
						var bounds = hitFront.collider.bounds;
						finalPos = bounds.center + Vector3.up * bounds.extents.y;
					}
					else
					{
						finalPos = isCharacter.InteractPoint;
					}

					currentEffect = Instantiate(interactEffect, finalPos, Quaternion.identity, hitFront.transform);

					if (isCharacter != null)
						controller.GetComponent<EmotionHandler>().HeadLook(isCharacter.head.transform);
				}
			}
			else
			{
				if (currentEffect != null)
				{
					Destroy(currentEffect);
					currentEffect = null;

					controller.GetComponent<EmotionHandler>().ResetLook();
				}
			}
		}
		else
		{
			if (currentEffect != null)
			{
				Destroy(currentEffect);
				currentEffect = null;

				controller.GetComponent<EmotionHandler>().ResetLook();
			}
		}
	}

	[System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "Input Message")]
	private void OnMove(InputValue value)
	{
		movementInput = value.Get<Vector2>();
	}

	[System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "Input Message")]
	private void OnRun(InputValue value)
	{
		runInput = value.isPressed;
	}
	#endregion

	public CharacterController GetController()
	{
		if (controller != null)
			return controller.GetComponent<CharacterController>();
		else
			return null;
	}

	public RaycastHit? GetHitFront()
	{
		if (!hasHitFront)
			return null;

		return hitFront;
	}

	public void SetGravity(bool gravity)
	{
		if (!gravity)
		{
			controller.gravity = false;
			controller.transform.position = transform.position;
			controller.transform.rotation = transform.rotation;
		}
		else
		{
			controller.gravity = true;
			controller.transform.rotation = Quaternion.Euler(0f, controller.transform.eulerAngles.y, 0f);
		}

		hasGravity = gravity;
	}

	public void SetLayer(int layer)
	{
		controller.transform.SetLayerRecursively(layer);
	}
}