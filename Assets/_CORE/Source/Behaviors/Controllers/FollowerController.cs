﻿using UnityEngine;
using Gentity;

using Pokemon.Data;

public class FollowerController_old : MonoBehaviour
{
	public float maxSpeed = 6f;
	public bool follow = true;
	public bool hasGravity = true;
	public bool startDisabled = false;
	public float stayAwayDistance = 2f;
	public FollowerController_old overrrideTarget;

	[Header("Appearance")]
	public int teamNumber = 1;
	public string overridePokemon = "";

	private PokemonDatabase dataManager;
	private CharController controller;
	private CharacterController target;
	private Vector3 velocity;
	private bool close;
	private float minDistance;

	void Start()
	{
		dataManager = Game.GetDatabase();

		GameObject modelReference;

		if (!string.IsNullOrEmpty(overridePokemon))
			modelReference = dataManager.Get<Species>(overridePokemon).maleModel;
		else
			modelReference = Game.TeamGetPokemonAt(teamNumber).Model;

		GameObject character = Instantiate(modelReference, transform.position, transform.rotation);
		if (startDisabled) character.SetActive(false);
		controller = character.GetComponent<CharController>();
		controller.alwaysVisible = true;

		SetGravity(hasGravity);

		velocity = Vector3.zero;
		close = false;

		var gentity = GetComponent<GentityEvent>();

		if (gentity)
			gentity.target = character;
	}

	void Update()
	{
		velocity = Vector3.zero;

		if (target == null)
		{
			if (overrrideTarget == null)
				target = GameObject.FindWithTag("Player").GetComponent<PlayerController_old>().GetController();
			else
				target = overrrideTarget.GetController();

			minDistance = controller.GetComponent<CharacterController>().radius + target.radius + 0.1f;
		}

		float distance = Vector3.Distance(controller.transform.position, target.transform.position);

		close = (distance <= (minDistance + (close ? 0.1f : 0f)));

		if (follow && target != null && !close)
		{
			Vector3 desiredVelocity = target.transform.position - controller.transform.position;

			float d = desiredVelocity.magnitude;

			var actualSpeed = maxSpeed * GetController().radius * 2f;
			var actualDistance = stayAwayDistance + minDistance;

			desiredVelocity = desiredVelocity.normalized * actualSpeed;

			if (d < actualDistance)
				desiredVelocity *= d / actualDistance;

			Vector3 steering = desiredVelocity - velocity;

			velocity = Vector3.ClampMagnitude(velocity + steering, actualSpeed);
			velocity.y = 0f;
		}

		if (controller.gameObject.activeSelf)
			controller.Move(velocity);
	}

	public CharacterController GetController()
	{
		if (controller != null)
			return controller.GetComponent<CharacterController>();
		else
			return null;
	}

	public void SetGravity(bool gravity)
	{
		if (!gravity)
		{
			controller.gravity = false;
			controller.transform.position = transform.position;
			controller.transform.rotation = transform.rotation;
		}
		else
		{
			controller.gravity = true;
			controller.transform.rotation = Quaternion.Euler(0f, controller.transform.eulerAngles.y, 0f);
		}

		hasGravity = gravity;
	}
}
