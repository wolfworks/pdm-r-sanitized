﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform target;
	public Vector3 offset;
	public bool follow = true;

	private void FixedUpdate()
	{
		if (target != null && follow)
		{
			transform.position = target.position + offset;
			transform.LookAt(target);
		}
	}
}