﻿using Character;
using UnityEngine;

public class BackCameraController : MonoBehaviour
{
	public AbstractCharacter target;
	public Vector3 offset;
	public float maxSpeed;
	public float minDistance;
	public float maxDistance;
	public float dampening;
	[Range(0f, 90f)]
	public float minVerticalAngle;
	[Range(0f, 90f)]
	public float maxVerticalAngle;
	public float collisionRadius;
	public LayerMask collisionLayerMask;
	public bool follow = true;

	private Transform targetTransform;
	private Vector3 rotateValue;
	private Vector3 currentAngle;
	private float currentDistance;
	private Vector3 posDest;
	private Quaternion rotDest;

	private void Start()
	{
		targetTransform = target.GetManager().transform;
	}

	private void Update()
	{
		if (target != null && follow)
		{
			var targetPos = targetTransform.position + offset;

			currentAngle += rotateValue * maxSpeed * Time.unscaledDeltaTime;
			currentAngle.x = Mathf.Clamp(currentAngle.x, minVerticalAngle, maxVerticalAngle);

			currentDistance = ((maxDistance - minDistance) * ((currentAngle.x - minVerticalAngle) / (maxVerticalAngle - minVerticalAngle))) + minDistance;

			var rot = Quaternion.Euler(currentAngle);
			var lookDir = rot * Vector3.forward;
			var pos = targetPos - lookDir * currentDistance;

			// Actual motion
			var targetToPos = pos - targetPos;

			if (Physics.Raycast(targetPos, targetToPos, out var hitInfo, targetToPos.magnitude + collisionRadius, collisionLayerMask))
			{
				if (!ReferenceEquals(hitInfo.transform, targetTransform))
					pos = hitInfo.point - targetToPos * collisionRadius;
			}

			posDest = pos;
			rotDest = rot;
		}
	}

	private void FixedUpdate()
	{
		if (target != null && follow)
		{
			transform.position = Vector3.Lerp(transform.position, posDest, Time.unscaledDeltaTime * dampening);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotDest, Time.unscaledDeltaTime * dampening);
		}
	}

	public void SetRotateValue(Vector3 newRotateValue)
	{
		rotateValue = newRotateValue;
	}
}