﻿using UnityEngine;
using UnityEngine.EventSystems;

public class KeepFocus : MonoBehaviour
{
	private GameObject _selectedMemory;
	private GameObject _lastSelected;

	void Start()
	{
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
	}

	void Update()
	{
		var currentSelected = EventSystem.current.currentSelectedGameObject;

		if (currentSelected == null || !currentSelected.activeSelf)
			EventSystem.current.SetSelectedGameObject(_lastSelected);

		if (!ReferenceEquals(currentSelected, _selectedMemory))
		{
			_lastSelected = _selectedMemory;
			_selectedMemory = currentSelected;
		}
	}
}