﻿using System.IO;
using UnityEngine;

public class CameraCapture : MonoBehaviour
{
    private Camera cameraReference;

    private void Start()
    {
        cameraReference = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    public void Capture(string filename)
    {
        RenderTexture activeRenderTexture = RenderTexture.active;
        RenderTexture.active = cameraReference.targetTexture;

        cameraReference.Render();

        Texture2D image = new Texture2D(cameraReference.targetTexture.width, cameraReference.targetTexture.height);
        image.ReadPixels(new Rect(0, 0, cameraReference.targetTexture.width, cameraReference.targetTexture.height), 0, 0);
        image.Apply();
        RenderTexture.active = activeRenderTexture;

        byte[] bytes = image.EncodeToPNG();
        Destroy(image);

        var dir = Application.persistentDataPath + "/screens/";

        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        File.WriteAllBytes(dir + filename + ".png", bytes);
    }
}
