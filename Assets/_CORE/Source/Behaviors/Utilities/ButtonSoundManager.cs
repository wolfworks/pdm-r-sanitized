﻿using FMODUnity;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets._CORE.Source.Behaviors.Utilities
{
	public class ButtonSoundManager : MonoBehaviour, ISelectHandler
	{
		[EventRef]
		public string selectEvent;
		
		public void OnSelect(BaseEventData eventData)
		{
			RuntimeManager.PlayOneShot(selectEvent);
		}
	}
}
