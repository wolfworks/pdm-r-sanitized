﻿using UnityEngine;

[RequireComponent(typeof(Animation))]
public class LegacyAnimationHandler : MonoBehaviour
{
	private new Animation animation;

	void Start()
	{
		animation = GetComponent<Animation>();
	}

	public void Play() {animation.Play();}
	public void Play(string clip) {animation.Play(clip);}
	public void PlayQueued(string clip) {animation.PlayQueued(clip);}
}