﻿/**
 * Akela, 21/07/2019
 * 
 * In deferred rendering, the Depth Only clear flag behaves very unexpectedly, and often does not work at all.
 * While playing around with the cameras I discovered a hacky workaround that I have implemented in this script.
 * 
 * If your depth only camera renders to a texture, then your main camera can render to the screen as it should.
 * The weird thing is, when you delete the render texture afterwards, it fixes the rendering issues and your
 * depth only camera now renders normally.
 * 
 * You have to attach this script to your depth only camera, and you should not set its clear flags to Depth Only,
 * the script will handle it. Set it to Skybox or Solid Color.
 */

using UnityEngine;

public class DepthOnlyHack : MonoBehaviour
{
	private RenderTexture texture;
	private Camera objCamera;

	private void Awake()
	{
		objCamera = GetComponent<Camera>();
		texture = new RenderTexture(Screen.width, Screen.height, 0);

		objCamera.targetTexture = texture;
		objCamera.clearFlags = CameraClearFlags.Depth;
	}

	private void Start()
	{
		objCamera.targetTexture = null;
		texture = null;
	}
}
