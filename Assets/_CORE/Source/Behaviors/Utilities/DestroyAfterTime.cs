﻿using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
	public float time;

	void Start()
	{
		Invoke("Stop", time);
	}
	
	private void Stop()
	{
		Destroy(gameObject);
	}
}
