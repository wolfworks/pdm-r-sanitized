﻿using UnityEngine;
using System.Collections.Generic;
using Gentity;

public class ActiveOnFlag : MonoBehaviour
{
	public List<ExecutionCondition> conditions = new List<ExecutionCondition>();

	void Awake()
	{
		foreach (var c in conditions)
		{
			if ((Game.Gamevars(c.gamevar) == c.val) == c.not)
			{
				gameObject.SetActive(false);
				return;
			}
		}
	}
}