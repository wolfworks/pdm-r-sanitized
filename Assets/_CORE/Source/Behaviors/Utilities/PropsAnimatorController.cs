﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PropsAnimatorController : MonoBehaviour
{
	public string startAnimation;
	public bool randomStartFrame;

	private Animator anim;

	private void Start()
	{
		anim = GetComponent<Animator>();

		if (!string.IsNullOrEmpty(startAnimation))
		{
			if (randomStartFrame)
				anim.Play(startAnimation, 0, Random.value);
			else
				anim.Play(startAnimation);
		}
	}
}