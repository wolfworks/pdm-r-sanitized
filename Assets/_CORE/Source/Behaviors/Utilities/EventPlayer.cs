﻿using FMODUnity;
using UnityEngine;

public class EventPlayer : MonoBehaviour
{
    public void EmitEvent(string @event)
	{
		RuntimeManager.PlayOneShot(@event, transform.position);
	}
}