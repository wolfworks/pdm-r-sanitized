﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Initialization : MonoBehaviour
{
	public AbstractFader loading;
	public AbstractFader difficultySettings;
	public AbstractFader fade;
	
	private static bool created = false;
	private DifficultySettings settings;

	private void Awake()
	{
		if (!created)
		{
			DontDestroyOnLoad(gameObject);
			created = true;

			StartCoroutine(LoadAll());
		}
	}

	private void Start()
	{
		settings = difficultySettings.GetComponent<DifficultySettings>();
	}

	private IEnumerator LoadAll()
	{
		Game.Initialize();
		yield return new WaitForSeconds(1f);

		if (Game.CurrentLevel == -1)
		{
			loading.FadeOut();
			yield return new WaitForSeconds(1f);

			difficultySettings.gameObject.SetActive(true);
			yield return new WaitForEndOfFrame();
			difficultySettings.FadeIn();

			yield return new WaitUntil(() => settings.DifficultySelected);
		}

		fade.FadeIn();
		yield return new WaitForSeconds(1f);

#if UNITY_EDITOR
		SceneManager.LoadScene("DebugHUB");
#else
		SceneManager.LoadScene("Titlescreen");
#endif
		yield return new WaitForSeconds(0.2f);
	}
}