﻿using Pokemon;
using Pokemon.Lexic;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class QuizzHandler : MonoBehaviour
{
	public int numberOfQuestions = 12;
	public GameObject button;
	public InfoPanel infoPanel;
	public InputField nameField;
	public HueShift hueShift;

	private Quizz quizz;
	private PopText pop;
	private List<int> answers;

	void Start()
	{
		pop = GetComponent<PopText>();
		quizz = new Quizz(numberOfQuestions);

		pop.messages = new PopText.MesRef[numberOfQuestions + 1];
		answers = new List<int>();

		int i = 0;
		foreach (int res in quizz.GetSuite())
		{
			pop.messages[i] = new PopText.MesRef
			{
				reference = quizz.GetQuestion(res)
			};

			answers.Add(res);

			i++;
		}

		pop.messages[numberOfQuestions] = new PopText.MesRef
		{
			reference = "c0_prologue/qGender"
		};
	}

	public void Register(int choice, int id)
	{
		if (id < numberOfQuestions)
			quizz.Register(answers[id], choice);
		else
			quizz.protagGender = (Gender)choice;

		hueShift.satDestination = Mathf.Sqrt((float)id / numberOfQuestions) * 100f - 100f;
		hueShift.hueDestination = GetHueAverage(quizz.CurrentScore);
	}

	public void Validate()
	{
		quizz.Validate();
	}

	public void InsertPartnerChoices()
	{
		infoPanel.gameObject.SetActive(true);
		infoPanel.SetStarters(quizz.PotentialPartners);
	}

	public void SendPartnerId(int id)
	{
		quizz.ChoosePartner(id);
	}

	public void SendPartnerGender(int gender, int _)
	{
		quizz.SetPartnerGender((Gender)gender);
	}

	#region Private Methods
	private float GetHueAverage(string score)
	{
		var possibleScores = PokemonStarter.starters
			.Select(x => x.identifier)
			.Where(x => Regex.IsMatch(x, score))
			.ToList();

		return possibleScores.Select(x => GetHueFromScore(x)).Average();
	}

	private float GetHueFromScore(string score)
	{
		var result = 0f;

		if (score.Contains("E"))
			result += 8f;

		if (score.Contains("S"))
			result += 4f;

		if (score.Contains("T"))
			result += 2f;

		if (score.Contains("J"))
			result += 1f;

		return Mathf.Round(result / 15f * 360f - 180f);
	}
	#endregion

#if UNITY_EDITOR
	// If inside editor, draw current quizz stats
	private void OnGUI()
	{
		var current = pop.GetCurrent();

		if (current == 0 || current > numberOfQuestions)
			return;

		var starterName = PokemonStarter.starters.Find(x => x.identifier == quizz.CurrentScore)?.pokemon.Species.name.CorrectTranslation();

		if (starterName == null)
			starterName = "<not defined yet>";

		var qn = answers[current - 1] % 7;
		var questionType = "";

		if (qn == 0) questionType = "EI";
		else if (qn == 1 || qn == 2) questionType = "SN";
		else if (qn == 3 || qn == 4) questionType = "TF";
		else if (qn == 5 || qn == 6) questionType = "JP";

		var r = new Rect(20f, 20f, 500f, 500f);

		GUI.color = Color.black;
		GUI.Label(r,
$@"{current}/{numberOfQuestions}

Current Score: {quizz.CurrentScore} ({starterName})
EI: {quizz.score_EI}
SN: {quizz.score_SN}
TF: {quizz.score_TF}
JP: {quizz.score_JP}

Question Type: {questionType}"
		);
	}
#endif
}
