﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleBackground : MonoBehaviour
{
	public ScreenFade fade;
	public GameObject keyTrigger;
	
	void Start()
	{
		int chapter = 1;

		if (Game.IsInitialized)
			chapter = Game.CurrentChapter;

		StartCoroutine(LoadBackground(chapter));
	}

	private IEnumerator LoadBackground(int chapter)
	{
		var sceneName = "title" + chapter;
		var sceneLoading = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

		yield return sceneLoading;

		SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));

		fade.FadeOut();

		yield return new WaitForSeconds(1f);

		keyTrigger.SetActive(true);
	}

	public void NextLevel()
	{
		if (Game.CurrentLevel == -1)
			SceneManager.LoadScene("c0_prologue");
		else
			SceneManager.LoadScene("MainMenu");
	}
}