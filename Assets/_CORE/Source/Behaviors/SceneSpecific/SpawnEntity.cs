﻿using FMODUnity;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SpawnEntity : MonoBehaviour
{
	public RawImage imageObject;
	public ScreenFade flash;
	[EventRef] public string flashSound;
	public Animation floor;

	private GameObject p;

	private void Awake()
	{
		imageObject.color = Color.black;
	}

	public void Spawn()
	{
		StartCoroutine(Scene());
	}

	public void Despawn()
	{
		Destroy(p);
	}

	public void PlayHappy()
	{
		p.transform.GetChild(0).GetComponent<Animator>().SetTrigger("Happy1");
	}

	private IEnumerator Scene()
	{
		floor.Play();

		var layer = LayerMask.NameToLayer("Noclip");
		p = Instantiate(Game.TeamGetPokemonAt(Game.Protagonist).Model, transform.position, transform.rotation);

		p.transform.SetLayerRecursively(layer);

		yield return new WaitForSeconds(2.5f);

		RuntimeManager.PlayOneShot(flashSound);

		flash.SetColor(Color.white);
		flash.SetSpeed(1f);
		flash.Flash();
		imageObject.color = Color.white;
	}
}