﻿using UnityEngine;

public class RandomBackground : MonoBehaviour
{
	public Sprite[] sprites;

	void Awake()
	{
		int random = Random.Range(0, sprites.Length);
		gameObject.GetComponent<SpriteRenderer>().sprite = sprites[random];
	}
}