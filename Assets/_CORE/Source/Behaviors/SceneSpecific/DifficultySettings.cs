﻿using UnityEngine;
using UnityEngine.UI;

public class DifficultySettings : MonoBehaviour
{
	public Text description;
	public GameObject[] badges;

	private int currentDifficulty = 0;

	public bool DifficultySelected { get; private set; } = false;

	public void SelectDifficulty(int difficultyNumber)
	{
		if (DifficultySelected)
			return;
		
		currentDifficulty = difficultyNumber;

		switch (currentDifficulty)
		{
			case 0:
				description.text = GameText.GetString("_system/diff_greenhorn_desc");
				break;

			case 1:
				description.text = GameText.GetString("_system/diff_normal_desc");
				break;

			case 2:
				description.text = GameText.GetString("_system/diff_veteran_desc");
				break;
		}

		for (var i = 0; i < badges.Length; ++i)
			badges[i].SetActive(i == currentDifficulty);
	}

	public void ApplyDifficulty()
	{
		if (DifficultySelected)
			return;

		Game.Tutorial = currentDifficulty == 0;
		Game.Veteran = currentDifficulty == 2;

		DifficultySelected = true;
	}
}