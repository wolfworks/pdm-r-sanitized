﻿using Character;
using Pokemon.Data;
using System.Collections.Generic;
using UnityEngine;

public class EntityViewer : MonoBehaviour
{
	public Transform circleCenter;

	[HideInInspector] public int currentEntity;

	private readonly Vector3 hiddenCirclePosition = new Vector3(0f, -1.5f, 0f);

	private GameObject circle;
	private float radius;
	private float angleStep;
	private CharacterManager[] entities;
	private bool hideCircle;

	public void Erase()
	{
		Destroy(circle);

		currentEntity = 0;
		radius = 0f;
		angleStep = 0f;
	}

	public void View(List<string> species)
	{
		PokemonDatabase manager = Game.GetDatabase();

		if (species != null)
			Erase();

		circle = new GameObject("Entity Viewer Circle");
		circle.transform.position = circleCenter.position;

		currentEntity = 0;
		angleStep = 2 * Mathf.PI / species.Count;
		radius = Vector2.Distance(transform.position.ToTopviewVector2(), circleCenter.position.ToTopviewVector2());

		entities = new CharacterManager[species.Count];

		for (var i = 0; i < species.Count; ++i)
		{
			var x = radius * Mathf.Cos(angleStep * i + Mathf.PI / 2f);
			var y = radius * Mathf.Sin(angleStep * i + Mathf.PI / 2f);

			var model = manager.Get<Species>(species[i]).maleModel;

			entities[i] = Instantiate(model, new Vector3(x, -0.25f, y), Quaternion.Euler(0f, 200f, 0f), circle.transform).GetComponent<CharacterManager>();
			entities[i].MovementHandler.UsePlatforms = false;
			entities[i].MovementHandler.UseGravity = false;
			entities[i].Collider.isTrigger = true;
			entities[i].Rigidbody.isKinematic = true;
		}

		var layer = LayerMask.NameToLayer("Noclip");

		circle.transform.SetLayerRecursively(layer);
	}

	public void Select()
	{
		entities[currentEntity].transform.SetParent(null);
		hideCircle = true;
	}

	private void Update()
	{
		if (!circle)
			return;

		if (hideCircle)
		{
			circle.transform.position = Vector3.Lerp(circle.transform.position, hiddenCirclePosition, Time.deltaTime * 6f);
			return;
		}
		
		var newRot = Quaternion.Euler(0f, angleStep * currentEntity * Mathf.Rad2Deg, 0f);
		circle.transform.rotation = Quaternion.Lerp(circle.transform.rotation, newRot, Time.deltaTime * 6f);

		foreach (var e in entities)
			e.MovementHandler.LookDirection(Quaternion.Euler(0f, 200f, 0f));
	}
}