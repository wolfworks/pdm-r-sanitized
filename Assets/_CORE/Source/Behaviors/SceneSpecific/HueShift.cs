﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class HueShift : MonoBehaviour
{
	[HideInInspector] public float hueDestination = 0f;
	[HideInInspector] public float satDestination = -100f;
	
	private ColorGrading color;
	private float hueValue = 0f;
	private float satValue = -100f;

	void Start()
	{
		PostProcessVolume volume = GetComponent<PostProcessVolume>();
		volume.profile.TryGetSettings(out color);
	}

	void Update()
	{
		hueDestination = Mathf.Clamp(hueDestination, -180f, 180f);
		hueValue = Mathf.Lerp(hueValue, hueDestination, Time.deltaTime * 4f);

		satDestination = Mathf.Clamp(satDestination, -100f, 0f);
		satValue = Mathf.Lerp(satValue, satDestination, Time.deltaTime * 4f);

		color.hueShift.value = hueValue;
		color.saturation.value = satValue;
	}
}