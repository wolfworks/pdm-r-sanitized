﻿using UnityEngine;

public class ChooseName : MonoBehaviour
{
	public void Choose(string name)
	{
		Game.TeamGetPokemonAt(Game.Protagonist).nickname = name;
	}
}