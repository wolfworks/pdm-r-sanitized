﻿using UnityEngine;
using UnityEngine.UI;
using Pokemon.Data;
using System.Collections.Generic;
using Pokemon;
using System.Linq;

public class InfoPanel : MonoBehaviour
{
	private static readonly Dictionary<string, Color> typeColors = new Dictionary<string, Color>
	{
		["BUG"] = new Color(.43f, .53f, .16f),
		["DARK"] = new Color(.31f, .27f, .27f),
		["DRAGON"] = new Color(.16f, .26f, .53f),
		["ELECTRIC"] = new Color(.53f, .52f, .16f),
		["FAIRY"] = new Color(.51f, .16f, .53f),
		["FIGHTING"] = new Color(.53f, .16f, .16f),
		["FIRE"] = new Color(.53f, .30f, .16f),
		["FLYING"] = new Color(.16f, .36f, .53f),
		["GHOST"] = new Color(.21f, .16f, .53f),
		["GRASS"] = new Color(.24f, .53f, .16f),
		["GROUND"] = new Color(.53f, .47f, .16f),
		["ICE"] = new Color(.16f, .53f, .49f),
		["NORMAL"] = new Color(.48f, .48f, .48f),
		["POISON"] = new Color(.40f, .16f, .53f),
		["PSYCHIC"] = new Color(.53f, .16f, .38f),
		["ROCK"] = new Color(.53f, .40f, .16f),
		["STEEL"] = new Color(.35f, .42f, .51f),
		["WATER"] = new Color(.16f, .42f, .53f)
	};

	public EntityViewer viewer;
	public QuizzHandler handler;
	public Text text;
	public Image icon1;
	public Image icon2;

	private List<PokemonStarter> starters;
	private int current;
	private bool pressed;
	private PokemonDatabase manager;

	void OnEnable()
	{
		pressed = false;
	}

	void Update()
	{
		if (Input.GetAxisRaw("Horizontal") > 0 && !pressed)
		{
			current = current == starters.Count - 1 ? 0 : current + 1;
			pressed = true;
			Refresh();
		}
		else if (Input.GetAxisRaw("Horizontal") < 0 && !pressed)
		{
			current = (current == 0 ? starters.Count - 1 : current - 1);
			pressed = true;
			Refresh();
		}
		else if (Input.GetAxisRaw("Horizontal") == 0)
		{
			pressed = false;
		}

		if (Input.GetButtonDown("Submit"))
		{
			viewer.Select();
			handler.SendPartnerId(starters[current].id);

			GameObject.FindWithTag("Name Edit").GetComponent<NameField>().AskName("_system/nickname", Game.Partner);

			gameObject.SetActive(false);
		}
	}

	private void Refresh()
	{
		if (manager == null)
			manager = Game.GetDatabase();

		viewer.currentEntity = current;

		Species s = starters[current].pokemon.Species;

		icon1.sprite = manager.Get<PokemonType>(s.types[0]).icon;

		if (s.types.Length == 2)
			icon2.sprite = manager.Get<PokemonType>(s.types[1]).icon;
		else
			icon2.sprite = icon1.sprite;

		text.text = s.name.CorrectTranslation(Game.Lang);
		text.GetComponent<Outline>().effectColor = typeColors[s.types[0]];
	}

	public void SetStarters(List<PokemonStarter> list)
	{
		starters = list;
		current = 0;

		viewer.View(list.Select(x => x.pokemon.Species.id).ToList());

		Refresh();
	}
}