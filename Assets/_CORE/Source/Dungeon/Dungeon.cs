﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Delaunay;
using Delaunay.Geo;
using Dungeon.Core;

using Random = System.Random;

namespace Dungeon
{
	public class Dungeon
	{
		const int MAX_ATTEMPTS = 1000;
		const float DEFORM_FACTOR = 5f;
		public const int MARGIN = 7;

		private readonly int width;
		private readonly int height;
		private readonly RoomProfile[] roomProfiles;

		private List<DungeonRoom> rooms;
		private List<DungeonRoom> floodedRooms;
		private List<DungeonCorridor> corridors;
		private List<DungeonCorridor> floodedCorridors;

		public readonly Tile[,] tiles;

		public Random RandomSeed { get; private set; }

		public Dungeon(int w, int h, RoomProfile[] profiles)
		{
			width = w;
			height = h;

			roomProfiles = profiles;

			tiles = new Tile[width + 2 * MARGIN, height + 2 * MARGIN];
		}

		public bool Generate(int maxAttempts, int deadEnds, float imperfection, float floodRatio, Tile fluidType = Tile.Nothing, Random random = null)
		{
			rooms = new List<DungeonRoom>();
			floodedRooms = new List<DungeonRoom>();

			corridors = new List<DungeonCorridor>();
			floodedCorridors = new List<DungeonCorridor>();

			for (int y = 0; y < tiles.GetLength(1); ++y)
			{
				for (int x = 0; x < tiles.GetLength(0); ++x)
				{
					tiles[x, y] = Tile.Wall;
				}
			}

			if (random == null)
				RandomSeed = new Random();
			else
				RandomSeed = random;

			// Rooms
			for (int i = 0; i < maxAttempts; ++i)
			{
				if (!DigRoom(roomProfiles[RandomSeed.Next(roomProfiles.Length)]))
				{
					Debug.Log("Unable to place more rooms. => placed " + i + " rooms.");
					break;
				}
			}

			// Flooded Rooms
			for (int i = 0; i < maxAttempts * floodRatio; ++i)
			{
				if (!DigFloodedRoom(roomProfiles[RandomSeed.Next(roomProfiles.Length)]))
				{
					Debug.Log("Unable to dig flooded rooms");
					break;
				}
			}

			if (!DigCorridors(deadEnds, imperfection))
			{
				Debug.LogWarning("Unable to dig the corridors.");
				return false;
			}

			if (!DigFloodedCorridors(floodRatio))
			{
				Debug.LogWarning("Unable to dig the flooded corridors.");
				return false;
			}

			if (!Carve(fluidType))
			{
				Debug.LogWarning("Unable to carve the dungeon.");
				return false;
			}

			return true;
		}

		public Tile GetTile(int x, int y)
		{
			if (x < 0 || y < 0 || x >= width || y >= height)
				return 0;

			return tiles[x + MARGIN, y + MARGIN];
		}

		public void SetTile(int x, int y, Tile t)
		{
			if (x < 0 || y < 0 || x >= width || y >= height)
				return;

			tiles[x + MARGIN, y + MARGIN] = t;
		}

		public Vector2 RandomRoomPos(int margin)
		{
			var room = rooms[RandomSeed.Next(0, rooms.Count)];
			var min = room.rectBox.min;
			var max = room.rectBox.max;

			Vector2 result;

			do
			{
				var x = RandomSeed.Next((int)min.x + margin, (int)max.x - margin);
				var y = RandomSeed.Next((int)min.y + margin, (int)max.y - margin);

				result = new Vector2(x, y);
			}
			while (!room.Contains(result));

			return result;
		}

		public DungeonRoom RoomByCoords(Vector2 point)
		{
			foreach (var room in rooms)
			{
				if (room.Contains(point))
					return room;
			}

			return null;
		}

		private bool DigRoom(RoomProfile roomProfile)
		{
			for (int i = 0; i < MAX_ATTEMPTS; ++i)
			{
				int w = RandomSeed.Next(roomProfile.minSize, roomProfile.maxSize + 1);
				int h = RandomSeed.Next(roomProfile.minSize, roomProfile.maxSize + 1);
				int x = RandomSeed.Next(0, width - w);
				int y = RandomSeed.Next(0, height - h);

				var testRect = new Rect(x - roomProfile.margin, y - roomProfile.margin, w + 2 * roomProfile.margin, h + 2 * roomProfile.margin);

				var validate = true;

				foreach (var room in rooms)
				{
					if (testRect.Overlaps(room.rectBox))
					{
						validate = false;
						break;
					}
				}

				if (!validate)
					continue;

				var roomRect = new Rect(x, y, w, h);
				rooms.Add(MakeRoom(roomRect, roomProfile));

				return true;
			}

			return false;
		}

		private bool DigFloodedRoom(RoomProfile roomProfile)
		{
			int w = RandomSeed.Next(roomProfile.minSize, roomProfile.maxSize + 1);
			int h = RandomSeed.Next(roomProfile.minSize, roomProfile.maxSize + 1);
			int x = RandomSeed.Next(0, width - w);
			int y = RandomSeed.Next(0, height - h);

			var roomRect = new Rect(x, y, w, h);
			floodedRooms.Add(MakeRoom(roomRect, roomProfile));

			return true;
		}

		private DungeonRoom MakeRoom(Rect rect, RoomProfile roomProfile)
		{
			Vector2 deform(Vector2 from)
			{
				if (roomProfile.deformation == 0f)
					return from;

				var deformationVector = new Vector2(
					(float)RandomSeed.NextDouble() * roomProfile.deformation,
					(float)RandomSeed.NextDouble() * roomProfile.deformation
				);

				return from + deformationVector * DEFORM_FACTOR;
			}

			var newRoom = new DungeonRoom();

			if (roomProfile.edges == 4)
			{
				newRoom.SetVertices(new List<Vector2>
					{
						deform(rect.min),
						deform(new Vector2(rect.xMin, rect.yMax)),
						deform(rect.max),
						deform(new Vector2(rect.xMax, rect.yMin))
					});
			}
			else
			{
				var sides = new[]
				{
						new LineSegment(rect.min, new Vector2(rect.xMin, rect.yMax)),
						new LineSegment(new Vector2(rect.xMin, rect.yMax), rect.max),
						new LineSegment(rect.max, new Vector2(rect.xMax, rect.yMin)),
						new LineSegment(new Vector2(rect.xMax, rect.yMin), rect.min)
					};
				var splitsPerSides = new[] { 0, 0, 0, 0 };
				var currentSide = RandomSeed.Next(sides.Length);

				for (var j = 0; j < roomProfile.edges; ++j)
				{
					splitsPerSides[currentSide]++;
					currentSide = (currentSide + 1) % 4;
				}

				var vertices = new List<Vector2>();

				for (var j = 0; j < sides.Length; ++j)
				{
					var side = sides[j];
					vertices.AddRange(side.Split(splitsPerSides[j]).Select(v => deform(v)));
				}

				newRoom.SetVertices(vertices);
			}

			return newRoom;
		}

		private bool DigCorridors(int deadEnds, float imperfection)
		{
			var intersections = new List<Vector2>();
			var colors = new List<uint>();

			foreach (var room in rooms)
			{
				intersections.Add(room.rectBox.center);
				colors.Add(0);
			}

			for (int i = 0; i < deadEnds; ++i)
			{
				var newDeadEnd = new Vector2
				(
					RandomSeed.Next(0, width),
					RandomSeed.Next(0, height)
				);

				if (intersections.Contains(newDeadEnd) || rooms.Any(x => x.rectBox.Contains(newDeadEnd)))
					continue;

				intersections.Add(newDeadEnd);
				colors.Add(0);
			}

			Voronoi v = new Voronoi(intersections, colors, new Rect(0, 0, width, height));

			List<LineSegment> delaunay = v.DelaunayTriangulation();
			List<LineSegment> spanningTree = v.SpanningTree();

			foreach (var ls in delaunay)
			{
				if (spanningTree.Contains(ls))
					continue;

				if (RandomSeed.NextDouble() < imperfection)
					spanningTree.Add(ls);
			}

			// For each line of the spanning tree, we generate two corridors

			var corridorPoints = new List<Vector2>();

			foreach (var ls in spanningTree)
			{
				var corridor = new DungeonCorridor(Vector2Int.FloorToInt(ls.p0.Value), Vector2Int.FloorToInt(ls.p1.Value), RandomSeed.NextDouble());

				// Check the proximity of all the points of the corridors: close points are merged together
				foreach (var prox in corridorPoints)
				{
					corridor.AlignPoints(prox, DungeonManager.PROXIMITY_THRESHOLD_DIAG);
				}

				corridorPoints.Add(corridor.start);
				corridorPoints.Add(corridor.end);

				if (!corridor.hasNoMiddle)
					corridorPoints.Add(corridor.middle);

				corridors.Add(corridor);
			}

			return true;
		}

		private bool DigFloodedCorridors(float floatRatio)
		{
			var intersections = new List<Vector2>();
			var colors = new List<uint>();

			foreach (var floodedRoom in floodedRooms)
			{
				intersections.Add(floodedRoom.rectBox.center);
				colors.Add(0);
			}

			Voronoi v = new Voronoi(intersections, colors, new Rect(0, 0, width, height));

			List<LineSegment> spanningTree = v.SpanningTree();

			//foreach (var ls in delaunay)
			//{
			//	if (spanningTree.Contains(ls))
			//		continue;

			//	if (RandomSeed.NextDouble() < imperfection)
			//		spanningTree.Add(ls);
			//}

			// For each line of the spanning tree, we generate two corridors

			var corridorPoints = new List<Vector2>();

			foreach (var ls in spanningTree)
			{
				var corridor = new DungeonCorridor(Vector2Int.FloorToInt(ls.p0.Value), Vector2Int.FloorToInt(ls.p1.Value), RandomSeed.NextDouble());

				// Check the proximity of all the points of the corridors: close points are merged together
				foreach (var prox in corridorPoints)
				{
					corridor.AlignPoints(prox, DungeonManager.PROXIMITY_THRESHOLD_DIAG);
				}

				corridorPoints.Add(corridor.start);
				corridorPoints.Add(corridor.end);

				if (!corridor.hasNoMiddle)
					corridorPoints.Add(corridor.middle);

				floodedCorridors.Add(corridor);
			}

			return true;
		}

		private bool Carve(Tile fluidType)
		{
			// Rooms
			foreach (var room in rooms)
			{
				var min = room.rectBox.min;
				var max = room.rectBox.max;

				for (int y = (int)min.y; y < (int)max.y; ++y)
				{
					for (int x = (int)min.x; x < (int)max.x; ++x)
					{
						if (room.Contains(new Vector2(x, y)))
							SetTile(x, y, Tile.Room);
					}
				}
			}

			// Corridors
			foreach (var corridor in corridors)
			{
				var crossingRooms = new List<DungeonRoom>();

				foreach (var point in corridor.WalkThrough())
				{
					var tilepos = Vector2Int.FloorToInt(point);

					if (GetTile(tilepos.x, tilepos.y) != Tile.Room)
					{
						// Creating potential entrance if we reached a room
						DungeonRoom potentialRoom;

						if ((potentialRoom = RoomByCoords(tilepos + Vector2.up)) != null && !crossingRooms.Contains(potentialRoom))
						{
							potentialRoom.AddEntrance(tilepos);
							crossingRooms.Add(potentialRoom);
						}

						if ((potentialRoom = RoomByCoords(tilepos + Vector2.right)) != null && !crossingRooms.Contains(potentialRoom))
						{
							potentialRoom.AddEntrance(tilepos);
							crossingRooms.Add(potentialRoom);
						}

						if ((potentialRoom = RoomByCoords(tilepos + Vector2.down)) != null && !crossingRooms.Contains(potentialRoom))
						{
							potentialRoom.AddEntrance(tilepos);
							crossingRooms.Add(potentialRoom);
						}

						if ((potentialRoom = RoomByCoords(tilepos + Vector2.left)) != null && !crossingRooms.Contains(potentialRoom))
						{
							potentialRoom.AddEntrance(tilepos);
							crossingRooms.Add(potentialRoom);
						}

						SetTile(tilepos.x, tilepos.y, Tile.Corridor);
					}
				}
			}

			// Flooded Rooms
			foreach (var floodedRoom in floodedRooms)
			{
				var min = floodedRoom.rectBox.min;
				var max = floodedRoom.rectBox.max;

				for (int y = (int)min.y; y < (int)max.y; ++y)
				{
					for (int x = (int)min.x; x < (int)max.x; ++x)
					{
						if (floodedRoom.Contains(new Vector2(x, y)) && GetTile(x, y) == Tile.Wall)
							SetTile(x, y, fluidType);
					}
				}
			}

			// Flooded Corridors
			foreach (var floodedCorridor in floodedCorridors)
			{
				foreach (var point in floodedCorridor.WalkThrough())
				{
					var tilepos = Vector2Int.FloorToInt(point);

					if (GetTile(tilepos.x, tilepos.y) == Tile.Wall)
						SetTile(tilepos.x, tilepos.y, fluidType);
				}
			}

			return true;
		}
	}
}