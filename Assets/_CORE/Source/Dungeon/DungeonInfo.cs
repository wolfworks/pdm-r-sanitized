﻿using System;
using Dungeon.Core;

namespace Dungeon
{
	[Serializable]
	public struct DungeonInfo
	{
		public Random random;
		public int floor;

		public DungeonBuilder dBuilder;
		public DungeonPopulation dPopulation;
		public DungeonLogic dLogic;
		public DungeonView dView;
	}
}