﻿using Pokemon;

namespace Dungeon.Core
{
	public enum Tile
	{
		Nothing = -1,
		Wall,
		Room,
		Corridor,
		Water,
		Lava,
		Air
	}

	public static class TileMethods
	{
		public static bool IsCrossableBy(this Tile tile, PokemonState pokemon)
		{
			if (tile == Tile.Room || tile == Tile.Corridor)
				return true;

			// TODO

			return false;
		}
	}
}
