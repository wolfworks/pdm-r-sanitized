﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Dungeon.Core
{
	public class DungeonBuilder
	{
		private readonly DungeonManager manager;

		private delegate int tileTest(int x, int y);

		private GameObject dungeonStructure;
		private GameObject reflectionProbe;
		private int[,] graphicTiles;

		public DungeonBuilder(DungeonManager m)
		{
			manager = m;
		}

		public void Build()
		{
			#region Place Obstacles
			graphicTiles = new int[manager.dungeon.tiles.GetLength(0), manager.dungeon.tiles.GetLength(1)];

			for (int y = 0; y < graphicTiles.GetLength(1); ++y)
			{
				for (int x = 0; x < graphicTiles.GetLength(0); ++x)
				{
					if (graphicTiles[x, y] == 0)
					{
						if (manager.dungeon.tiles[x, y] == Tile.Wall)
						{
							if
							(
								IsFloor(x - 1, y)
								|| IsFloor(x + 1, y)
								|| IsFloor(x, y - 1)
								|| IsFloor(x, y + 1)
								|| IsFloor(x - 1, y - 1)
								|| IsFloor(x + 1, y - 1)
								|| IsFloor(x - 1, y + 1)
								|| IsFloor(x + 1, y + 1)
							)
							{
								int v = 1;
								float r = RandomFromDistribution.RandomFromExponentialDistribution(1f, RandomFromDistribution.Direction_e.Right);
								Vector2Int d = new Vector2Int(1, 1);

								for (int i = 0; i < manager.obstacles.Length; ++i)
								{
									if (r <= manager.obstacles[i].chance)
									{
										bool able = true;

										for (int oy = 0; oy < manager.obstacles[i].dimension.y; ++oy)
										{
											for (int ox = 0; ox < manager.obstacles[i].dimension.x; ++ox)
											{
												able = x + ox < manager.dungeon.tiles.GetLength(0) &&
													y + oy < manager.dungeon.tiles.GetLength(1) &&
													manager.dungeon.tiles[x + ox, y + oy] == Tile.Wall &&
													graphicTiles[x + ox, y + oy] < 10 &&
													graphicTiles[x + ox, y + oy] != 4;

												if (!able) break;
											}

											if (!able) break;
										}

										if (able)
										{
											v = 10 + i;
											d = manager.obstacles[i].dimension;
										}

										break;
									}
								}

								for (int oy = 0; oy < d.y; ++oy)
								{
									for (int ox = 0; ox < d.x; ++ox)
									{
										if (ox == 0 && oy == 0)
											graphicTiles[x + ox, y + oy] = v;
										else
											graphicTiles[x + ox, y + oy] = 4;
									}
								}
							}
							else
							{
								graphicTiles[x, y] = 1;
							}
						}
						else if (manager.dungeon.tiles[x, y] == Tile.Room || manager.dungeon.tiles[x, y] == Tile.Corridor)
						{
							graphicTiles[x, y] = 2;
						}
						else
						{
							graphicTiles[x, y] = 3;
						}
					}
				}
			}
			#endregion

			#region Build Dungeon
			if (dungeonStructure != null)
				Object.Destroy(dungeonStructure);

			dungeonStructure = new GameObject("DungeonStructure");

			for (int y = 0; y < graphicTiles.GetLength(1); ++y)
			{
				for (int x = 0; x < graphicTiles.GetLength(0); ++x)
				{
					string variant = "";

					if (manager.numberOfVariants != 0)
						variant = Random.Range(1, manager.numberOfVariants + 1).ToString();

					bool placeProp = Random.value <= DungeonManager.PROP_CHANCE;

					if (graphicTiles[x, y] == 1 || graphicTiles[x, y] == 3) // Wall or Fluid
					{
						var test = graphicTiles[x, y] == 1 ? (tileTest)IsNotWall: IsNotFluid;

						int u = test(x, y - 1);
						int l = test(x - 1, y) * 8;
						int r = test(x + 1, y) * 2;
						int d = test(x, y + 1) * 4;

						int ul = test(x - 1, y - 1);
						int ur = test(x + 1, y - 1) * 2;
						int dl = test(x - 1, y + 1) * 8;
						int dr = test(x + 1, y + 1) * 4;

						int edges = u + r + d + l;
						int corners = 0;

						if (u != 0 && l != 0) corners += ul;
						if (u != 0 && r != 0) corners += ur;
						if (d != 0 && r != 0) corners += dr;
						if (d != 0 && l != 0) corners += dl;

						var hash = HashTile(edges, corners, out float angle, out float mirror);
						string finalHash;

						if (graphicTiles[x, y] == 3)
							finalHash = "W" + hash;
						else
							finalHash = hash + variant;

						var tr = manager.structure.Find(finalHash);

						if (tr == null)
							Debug.Log(finalHash);

						var obj = tr.gameObject;

						Vector3 pos = new Vector3(x - Dungeon.MARGIN, 0f, y - Dungeon.MARGIN);
						Quaternion rot = Quaternion.Euler(-90f, 0f, angle);
						Vector3 scale = new Vector3(mirror, 1f, 1f);

						var instance = Object.Instantiate(obj, pos, rot, dungeonStructure.transform);
						instance.transform.localScale = scale;

						if (graphicTiles[x, y] == 1 && placeProp && hash == "A" && manager.wallProps.Length > 0)
						{
							var propPos = pos + new Vector3(Random.Range(-0.5f, 0.5f), 0f, Random.Range(-0.5f, 0.5f));
							var propRot = Quaternion.Euler(0f, Random.Range(0, 360), 0f);

							Object.Instantiate(manager.wallProps[Random.Range(0, manager.wallProps.Length)], propPos, propRot, dungeonStructure.transform);
						}
					}
					else // Floor
					{
						Vector3 pos = new Vector3(x - Dungeon.MARGIN, 0f, y - Dungeon.MARGIN);
						Quaternion rot = Quaternion.Euler(-90f, 0f, 0f);
						Object.Instantiate(manager.structure.Find("O" + variant).gameObject, pos, rot, dungeonStructure.transform);

						if (placeProp && graphicTiles[x, y] < 10 && manager.floorProps.Length > 0)
						{
							var propPos = pos + new Vector3(Random.Range(-0.5f, 0.5f), 0f, Random.Range(-0.5f, 0.5f));
							var propRot = Quaternion.Euler(0f, Random.Range(0, 360), 0f);

							Object.Instantiate(manager.floorProps[Random.Range(0, manager.floorProps.Length)], propPos, propRot, dungeonStructure.transform);
						}
					}

					// It's an obstacle
					if (graphicTiles[x, y] >= 10)
					{
						var obstacle = manager.obstacles[graphicTiles[x, y] - 10];

						var pos = new Vector3(x - Dungeon.MARGIN, 0f, y - Dungeon.MARGIN) + new Vector3(Random.Range(-0.25f, 0.25f), 0f, Random.Range(-0.25f, 0.25f));

						Quaternion rot;

						if (obstacle.dimension.x == 1 && obstacle.dimension.y == 1)
							rot = Quaternion.Euler(0f, Random.Range(0, 360), 0f);
						else
							rot = Quaternion.identity;

						Object.Instantiate(obstacle.gameObject, pos, rot, dungeonStructure.transform);
					}
				}
			}
			#endregion

			// Adding a reflection probe in the center
			if (reflectionProbe != null)
				Object.Destroy(reflectionProbe);

			reflectionProbe = new GameObject("Reflection Probe");

			var probe = reflectionProbe.AddComponent<ReflectionProbe>();
			probe.mode = ReflectionProbeMode.Realtime;
			probe.refreshMode = ReflectionProbeRefreshMode.ViaScripting;
			probe.size = new Vector3(graphicTiles.GetLength(0), 10f, graphicTiles.GetLength(1));
			probe.transform.position = new Vector3(graphicTiles.GetLength(0) / 2f - Dungeon.MARGIN, 1f, graphicTiles.GetLength(1) / 2f - Dungeon.MARGIN);
			probe.RenderProbe();
		}

		public void DestroyTile(Vector2Int position)
		{
			if (dungeonStructure == null) return;

			var pos3 = new Vector3(position.x, 0f, position.y);

			foreach (Transform t in dungeonStructure.transform)
			{
				if (Vector3.Distance(t.position, pos3) <= 0.01)
					Object.Destroy(t.gameObject);
			}
		}

		#region Private Methods
		private bool IsFloor(int x, int y)
		{
			if (x < 0 || y < 0 || x >= manager.dungeon.tiles.GetLength(0) || y >= manager.dungeon.tiles.GetLength(1))
				return false;

			return manager.dungeon.tiles[x, y] == Tile.Corridor || manager.dungeon.tiles[x, y] == Tile.Room;
		}

		private int IsNotWall(int x, int y)
		{
			if (x < 0 || y < 0 || x >= graphicTiles.GetLength(0) || y >= graphicTiles.GetLength(1))
				return 1;

			return graphicTiles[x, y] == 1 ? 1 : 0;
		}

		private int IsNotFluid(int x, int y)
		{
			if (x < 0 || y < 0 || x >= graphicTiles.GetLength(0) || y >= graphicTiles.GetLength(1))
				return 1;

			return graphicTiles[x, y] == 3 ? 1 : 0;
		}

		private string HashTile(int edges, int corners, out float rot, out float scale)
		{
			string res = "";
			rot = 0f;
			scale = 1f;

			switch (edges)
			{
				case 0: res = "N"; break;
				case 1: res = "M"; rot = 270f; break;
				case 2: res = "M"; rot = 180f; break;
				case 3:
					switch (corners)
					{
						case 0: res = "L"; rot = 180f; break;
						case 2: res = "K"; rot = 180f; break;
					}
					break;
				case 4: res = "M"; rot = 90f; break;
				case 5: res = "J"; rot = 90f; break;
				case 6:
					switch (corners)
					{
						case 0: res = "L"; rot = 90f; break;
						case 4: res = "K"; rot = 90f; break;
					}
					break;
				case 7:
					switch (corners)
					{
						case 0: res = "I"; rot = 90f; break;
						case 2: res = "H"; rot = 90f; scale = -1f; break;
						case 4: res = "H"; rot = 90f; break;
						case 6: res = "G"; rot = 90f; break;
					}
					break;
				case 8: res = "M"; break;
				case 9:
					switch (corners)
					{
						case 0: res = "L"; rot = 270f; break;
						case 1: res = "K"; rot = 270f; break;
					}
					break;
				case 10: res = "J"; break;
				case 11:
					switch (corners)
					{
						case 0: res = "I"; rot = 180f; break;
						case 1: res = "H"; rot = 180f; scale = -1f; break;
						case 2: res = "H"; rot = 180f; break;
						case 3: res = "G"; rot = 180f; break;
					}
					break;
				case 12:
					switch (corners)
					{
						case 0: res = "L"; break;
						case 8: res = "K"; break;
					}
					break;
				case 13:
					switch (corners)
					{
						case 0: res = "I"; rot = 270f; break;
						case 1: res = "H"; rot = 270f; break;
						case 8: res = "H"; rot = 270f; scale = -1f; break;
						case 9: res = "G"; rot = 270f; break;
					}
					break;
				case 14:
					switch (corners)
					{
						case 0: res = "I"; break;
						case 4: res = "H"; scale = -1f; break;
						case 8: res = "H"; break;
						case 12: res = "G"; break;
					}
					break;
				case 15:
					switch (corners)
					{
						case 0: res = "F"; break;
						case 1: res = "E"; rot = 180f; break;
						case 2: res = "E"; rot = 90f; break;
						case 3: res = "C"; rot = 180f; break;
						case 4: res = "E"; break;
						case 5: res = "D"; break;
						case 6: res = "C"; rot = 90f; break;
						case 7: res = "B"; rot = 180f; break;
						case 8: res = "E"; rot = 270f; break;
						case 9: res = "C"; rot = 270f; break;
						case 10: res = "D"; rot = 90f; break;
						case 11: res = "B"; rot = 270f; break;
						case 12: res = "C"; break;
						case 13: res = "B"; break;
						case 14: res = "B"; rot = 90f; break;
						case 15: res = "A"; break;
					}
					break;
			}

			return res;
		}
		#endregion
	}
}
