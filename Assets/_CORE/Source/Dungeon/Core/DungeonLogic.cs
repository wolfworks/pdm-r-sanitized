﻿using Dungeon.Entity;
using Pokemon;
using Pokemon.Data;
using Pokemon.Lexic;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Core
{
	public class DungeonLogic
	{
		private readonly DungeonManager manager;

		public BattleManager Battle { get; private set; }
		public LuaUtility Lua { get; private set; }

		public DungeonLogic(DungeonManager m)
		{
			manager = m;

			Lua = new LuaUtility(manager);
			Battle = new BattleManager();
		}

		public void Process(DungeonCharacter character, List<DungeonCharacter> movingEntities)
		{
			switch (character.desiredAction[0])
			{
				case "goto":
					var finalPos = character.position + character.desiredAction[1].ToVector();

					if (finalPos != character.LastPosition)
					{
						RegenerateIfPossible(character);
						HungerLoss(character, 1, manager.numberOfTurns % Game.HUNGER_LOSS == 0);

						var canMove = true;
						var occupied = (DungeonCharacter)manager.EntitiesAt(finalPos, floor: false, item: false).Find(x => x != character);

						if (occupied != null)
						{
							if (occupied.enemy == character.enemy && occupied.AIPriority > character.AIPriority)
							{
								movingEntities.Add(occupied);
								occupied.position = character.position;
							}
							else
							{
								canMove = false;
							}
						}

						if (canMove)
						{
							movingEntities.Add(character);
							character.position = finalPos;
						}
					}
					break;

				case "move":
					var moveId = int.Parse(character.desiredAction[1]);
					UseMove(moveId, character);
					HungerLoss(character, 1);
					break;

				case "attack":
					if (character.entityInFront == null || character.entityInFront.enemy != character.enemy)
					{
						UseNormalAttack(character);
						HungerLoss(character, 1);
					}
					else
					{
						manager.TalkTo(character.entityInFront);
					}
					break;

				case "skip":
					RegenerateIfPossible(character);
					break;
			}

			ProcessHunger(character);
		}

		#region Combat Methods
		public void UseNormalAttack(DungeonCharacter user)
		{
			var target = user.entityInFront;
			DealtDamage? damage = null;

			if (target != null && target.enemy != user.enemy)
			{
				damage = Battle.NormalAttack(user, target);
				target.pokemon.HP -= damage.Value.amount;
			}

			manager.AnimateOne(user, target, null, damage);
			manager.TestFainted();
		}

		public void UseMove(int moveId, DungeonCharacter user)
		{
			var move = user.pokemon.Moves(moveId);
			var moveName = TextColor.Colorize(move.name.CorrectTranslation(Game.Lang), TextColor.darkCyan);
			var userName = TextColor.GetColoredName(user);

			if (user.pokemon.PP[moveId] <= 0)
			{
				manager.LogMessage(string.Format(GameText.GetString("_actions/nopp"), moveName));
				return;
			}

			user.pokemon.PP[moveId]--;

			manager.LogMessage(string.Format(GameText.GetString("_actions/use"), userName, moveName));

			using (var script = Lua.LoadScript(move.effect))
			{
				if (move.targeting == MoveTargeting.NoTarget)
				{
					// TODO
				}
				else if (move.targeting == MoveTargeting.CurrentTile)
				{
					// TODO
				}
				else
				{
					Battle.SetScript(script);
				
					var targets = GetVictims(move, user);
					var damage = Battle.Move(user, targets, move);

					if (damage.Count > 0)
					{
						for (int i = 0; i < targets.Count; ++i)
						{
							if (damage[i].isFailed || damage[i].isHealing)
								continue;

							targets[i].pokemon.HP -= damage[i].amount;
						}
					}

					manager.AnimateMultiple(user, targets, move, damage, script);

					foreach (var c in targets)
						c.TestIfFainted();
				}
			}
		}

		public void UseItem(DungeonItem item, DungeonCharacter user)
		{
			// TODO
		}

		public void ActivateFloor(DungeonFloor df, DungeonCharacter target)
		{
			if (Random.value > DungeonManager.TRAP_ACTIVATION_CHANCE || df.floor.alwaysActivate)
			{
				if (df.floor.id != "STAIRS" && df.floor.id != "HSTAIRS")
					manager.PlaySound(manager.trapSound);

				using (var script = Lua.LoadScript(df.floor.effect))
				{
					script.Emit("onEntityWalkOn", target, target is DungeonPlayer);

					manager.AnimateOne(target, null, df.floor, null, script);
				}
			}
			else
			{
				manager.LogMessage(GameText.GetString("_actions/trap_fail"));
			}
		}

		public void PickupItem(DungeonItem di, DungeonCharacter character)
		{
			if (character.enemy)
			{
				string name = TextColor.Colorize(character.pokemon.nickname, TextColor.darkOrange);
				string itemName = TextColor.Colorize(di.item.Name, TextColor.darkCyan);

				if (character.pokemon.heldItem == null)
				{
					manager.PlaySound(manager.itemEnemySound);
					manager.LogMessage(string.Format(GameText.GetString("_actions/pickup"), name, itemName));

					character.pokemon.heldItem = di.item;

					di.Despawn();
				}
				else
				{
					manager.LogMessage(string.Format(GameText.GetString("_actions/walkon"), name, itemName));
				}
			}
			else // is ally
			{
				string name = TextColor.Colorize(character.pokemon.nickname, character is DungeonPlayer ? TextColor.darkBlue : TextColor.darkYellow);
				string itemName = TextColor.Colorize(di.item.Name, TextColor.darkCyan);

				if (manager.GetSprint())
				{
					manager.LogMessage(string.Format(GameText.GetString("_actions/walkon"), name, itemName));
					return;
				}

				if (di.item.ItemType.id == "MONEY")
				{
					manager.PlaySound(manager.itemMoneySound);
					manager.LogMessage(string.Format(GameText.GetString("_actions/pickup"), name, itemName));

					Game.Money += di.item.ammo;

					di.Despawn();
				}
				else
				{
					if (Game.ItemAdd(ref di.item))
					{
						if (di.item.ItemType.stackable && di.item.ammo > 0)
						{
							manager.PlaySound(manager.itemPlayerSound);
							manager.LogMessage(string.Format(GameText.GetString("_actions/pickupsome"), name, itemName));
						}
						else
						{
							manager.PlaySound(manager.itemPlayerSound);
							manager.LogMessage(string.Format(GameText.GetString("_actions/putinbag"), name, itemName));

							di.Despawn();
						}
					}
					else
					{
						manager.LogMessage(string.Format(GameText.GetString("_actions/walkon"), name, itemName));
					}
				}
			}
		}
		#endregion

		#region Utility Methods
		public List<DungeonCharacter> GetVictims(Move move, DungeonCharacter user, Vector2Int? directionOverride = null)
		{
			var result = new List<DungeonCharacter>();
			var dir = directionOverride.HasValue ? directionOverride.Value : user.LookAt;
			var frontTile = user.position + dir;

			switch (move.targeting)
			{
				case MoveTargeting.TileInFront:
					if (manager.TileAt(frontTile) != Tile.Wall && !manager.CornerCut(dir, user.position))
					{
						var c = manager.CharacterAt(frontTile);

						if (c != null && c.enemy != user.enemy)
							result.Add(c);
					}
					break;

				case MoveTargeting.TwoTilesFront:
					var twoTiles = frontTile + dir;

					if (manager.TileAt(frontTile) != Tile.Wall)
					{
						var c = manager.CharacterAt(frontTile);

						if (c != null && c.enemy != user.enemy)
						{
							result.Add(c);
						}
						else if (manager.TileAt(twoTiles) != Tile.Wall && !manager.CornerCut(dir, frontTile))
						{
							c = manager.CharacterAt(twoTiles);

							if (c != null && c.enemy != user.enemy)
								result.Add(c);
						}
					}
					break;

				case MoveTargeting.CutCorners:
					if (manager.TileAt(frontTile) != Tile.Wall)
					{
						var c = manager.CharacterAt(frontTile);

						if (c != null && c.enemy != user.enemy)
							result.Add(c);
					}
					break;

				case MoveTargeting.Line:
					var currentPos = frontTile;
					var iteration = 0;
					while (manager.TileAt(currentPos) != Tile.Wall && iteration < Game.MAX_MOVE_RANGE)
					{
						var c = manager.CharacterAt(currentPos);

						if (c != null && c.enemy != user.enemy)
						{
							result.Add(c);
							break;
						}

						currentPos += dir;
						++iteration;
					}
					break;

				case MoveTargeting.Ring:
					var ring = Rect.MinMaxRect(user.position.x - 1, user.position.y - 1, user.position.x + 1, user.position.y + 1);
					var subject = manager.CharacterIn(ring);

					if (subject != null && subject.enemy != user.enemy)
						result.Add(subject);
					break;

				case MoveTargeting.Arc:
					var leftTile = user.position + Vector2Int.CeilToInt(((Vector2)dir).Rotate(-45f));
					var rightTile = user.position + Vector2Int.CeilToInt(((Vector2)dir).Rotate(45f));
					if (manager.TileAt(frontTile) != Tile.Wall)
					{
						var c = manager.CharacterAt(frontTile);

						if (c != null && c.enemy != user.enemy)
							result.Add(c);
					}

					if (manager.TileAt(leftTile) != Tile.Wall)
					{
						var c = manager.CharacterAt(leftTile);

						if (c != null && c.enemy != user.enemy)
							result.Add(c);
					}

					if (manager.TileAt(rightTile) != Tile.Wall)
					{
						var c = manager.CharacterAt(rightTile);

						if (c != null && c.enemy != user.enemy)
							result.Add(c);
					}
					break;

				case MoveTargeting.Self:
					result.Add(user);
					break;

				case MoveTargeting.Allies:
					result.AddRange(user.Context.visibleAllies);
					break;

				case MoveTargeting.AlliesAndSelf:
					result.Add(user);
					result.AddRange(user.Context.visibleAllies);
					break;

				case MoveTargeting.RandomDirection:
					var rotations = new float[] { 0f, 45f, 90f, 135f, 180f, 225f, 270f, 315f };
					var randRotation = rotations[Random.Range(0, rotations.Length)];
					var randomTile = user.position + Vector2Int.CeilToInt(((Vector2)dir).Rotate(randRotation));
					if (manager.TileAt(randomTile) != Tile.Wall)
					{
						var c = manager.CharacterAt(randomTile);

						if (c != null && c.enemy != user.enemy)
							result.Add(c);
					}
					break;

				case MoveTargeting.EntireRoom:
					result.AddRange(user.Context.visibleEnemies);
					break;

				case MoveTargeting.EntireFloor:
					foreach (DungeonCharacter c in manager.IterateOnEntities(item: false, floor: false))
					{
						if (c.enemy != user.enemy)
							result.Add(c);
					}
					break;

				default:
					return null;
			}

			return result;
		}

		public bool ShouldInterruptSprint()
		{
			Tile currentTile = manager.TileAt(manager.Player.position);
			Tile upTile = manager.TileAt(manager.Player.position + Vector2Int.up);
			Tile rightTile = manager.TileAt(manager.Player.position + Vector2Int.right);
			Tile downTile = manager.TileAt(manager.Player.position + Vector2Int.down);
			Tile leftTile = manager.TileAt(manager.Player.position + Vector2Int.left);

			bool tileCheck;

			if (currentTile == Tile.Room)
			{
				tileCheck = upTile == Tile.Corridor
					|| rightTile == Tile.Corridor
					|| downTile == Tile.Corridor
					|| leftTile == Tile.Corridor;
			}
			else
			{
				tileCheck = (manager.Player.LookAt.x == 0f && (rightTile != Tile.Wall || leftTile != Tile.Wall))
					|| (manager.Player.LookAt.y == 0f && (upTile != Tile.Wall || downTile != Tile.Wall));
			}

			bool entityCheck = false;

			if (manager.Player.entityInFront != null)
			{
				entityCheck = true;
			}
			else
			{
				foreach (var de in manager.IterateOnEntities(item: false))
				{
					if (de is DungeonFloor df)
					{
						if (!df.revealed) continue;

						entityCheck = Vector2Int.Distance(df.position, manager.Player.position) <= DungeonManager.PROXIMITY_THRESHOLD * 1.5f;
					}
					else if (de is DungeonCharacter dc)
					{
						if (!dc.enemy) continue;

						entityCheck = Vector2Int.Distance(de.position, manager.Player.position) <= DungeonManager.PROXIMITY_THRESHOLD * 2.5f;
					}
				}
			}

			return tileCheck || entityCheck;
		}
		#endregion

		#region Private Methods
		private void RegenerateIfPossible(DungeonCharacter character)
		{
			var isPlayer = character is DungeonPlayer;

			if (!isPlayer || character.pokemon.Hunger > 0)
				character.pokemon.HP += character.pokemon.GetStat(Stat.hp) * Game.LIFE_REGEN;
		}

		private void ProcessHunger(DungeonCharacter character)
		{
			if (character is DungeonPlayer && character.pokemon.Hunger <= 0)
			{
				--character.pokemon.HP;
				manager.TestFainted();
			}
		}

		private void HungerLoss(DungeonCharacter character, int quantity, bool predicate = true)
		{
			if (predicate && character is DungeonPlayer && character.pokemon.Hunger > 0)
				character.pokemon.Hunger -= quantity;
		}
		#endregion
	}
}