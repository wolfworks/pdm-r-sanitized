﻿using Dungeon.Entity;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Core
{
	public struct RoomContext
	{
		public DungeonRoom room;
		public DungeonCharacter sender;
		public List<DungeonCharacter> visibleEnemies;
		public List<DungeonCharacter> visibleAllies;
		public List<DungeonItem> visibleItems;
		public List<DungeonFloor> visibleFloors;
	}

	public static class RoomContextExtension
	{
		public static bool Walkable(this RoomContext context, Vector2Int pos, bool entityCheck = true)
		{
			var crossable = context.sender.manager.TileAt(pos).IsCrossableBy(context.sender.pokemon);

			if (!crossable) return false;

			if (!entityCheck) return true;

			return !context.sender.manager.EntitiesAt(pos, floor: false, item: false).Exists(x =>
			{
				var dc = (DungeonCharacter)x;
				if (x != context.sender)
				{
					var enemy = dc.enemy != context.sender.enemy;
					var leader = dc.AIPriority <= context.sender.AIPriority;

					return enemy || leader;
				}

				return false;
			});
		}

		public static bool WalkableDir(this RoomContext context, Vector2Int direction, bool entityCheck = true)
		{
			return context.Walkable(context.sender.position + direction, entityCheck);
		}

		public static bool WalkableDir(this RoomContext context, string stringdir, bool entityCheck = true)
		{
			return context.WalkableDir(stringdir.ToVector(), entityCheck);
		}
	}
}
