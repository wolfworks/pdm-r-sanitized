﻿using System;
using UnityEngine;

namespace Dungeon.Core
{
	[Serializable]
	public struct EnemySpawn
	{
		public string species;
		public Vector2Int levelRange;
		public int aiPriority;
		public bool sleeps;
		public bool standStill;
	}
}
