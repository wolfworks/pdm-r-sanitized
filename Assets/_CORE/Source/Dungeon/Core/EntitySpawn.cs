﻿using System;
using UnityEngine;

namespace Dungeon.Core
{
	[Serializable]
	public struct EntitySpawn
	{
		public string id;
		[Range(0f, 1f)]
		public float chance;
	}
}
