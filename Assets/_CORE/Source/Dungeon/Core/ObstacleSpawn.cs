﻿using System;
using UnityEngine;

namespace Dungeon.Core
{
	[Serializable]
	public struct ObstacleSpawn
	{
		public GameObject gameObject;
		public Vector2Int dimension;
		[Range(0f, 1f)]
		public float chance;
	}
}
