﻿using Dungeon.Entity;
using Dungeon.UI;
using FMODUnity;
using Pokemon.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

namespace Dungeon.Core
{
	public class DungeonManager : MonoBehaviour
	{
		#region Constants
		public const float TURN_DURATION = 0.33f;
		public const float PROP_CHANCE = 0.25f;
		public const float TRAP_ACTIVATION_CHANCE = 0.05f;
		public const int MAP_PIXEL = 6;
		public const float MAP_FADE_DURATION = 1.33f;
		public const float LOG_STAY_DURATION = 4f;
		public const float PROXIMITY_THRESHOLD = 1.3f;
		public const float PROXIMITY_THRESHOLD_DIAG = 1.7f;
		public const float VIEW_DISTANCE = 4f;
		public const int HUNGER_START_EFFECT = 10;
		#endregion

		#region Component Fields
		[Header("Structure")]
		public int width;
		public int height;
		public RoomProfile[] roomProfiles;
		public int maxNumberOfRooms;
		public int maxNumberOfDeadEnds;

		[Range(0f, 1f)]
		public float imperfection;

		[Range(0f, 1f)]
		public float floodRatio;

		public Tile fluidType;

		[Header("Navigation")]
		public string dungeonName;
		public int numberOfFloors;
		public int startFloor = 0;
		public bool goesDown;

		[Header("Aspect")]
		public Transform structure;
		public int numberOfVariants;
		public GameObject stairs;
		public ObstacleSpawn[] obstacles;
		public GameObject[] wallProps;
		public GameObject[] floorProps;

		[Header("Population")]
		public EnemySpawn[] possibleEnemies;
		public int maxNumberOfEnemies;
		public EntitySpawn[] possibleItems;
		public int maxNumberOfItems;
		public int maxAmountOfMoney;
		public int maxAmountOfAmmo;
		public float itemsStickyChance;
		public EntitySpawn[] possibleTraps;
		public int maxNumberOfTraps;

		[Header("Assets")]
		public GameObject diagArrows;
		public GameObject marker;
		public Sprite markerAlly;
		public Sprite markerGuest;
		public Sprite markerEnemy;
		public Sprite markerClient;
		public Sprite markerNPC;
		public GameObject popup;
		public GameObject gridElement;
		public Sprite gridActive;
		[EventRef] public string stairsSound;
		[EventRef] public string itemPlayerSound;
		[EventRef] public string itemEnemySound;
		[EventRef] public string itemMoneySound;
		[EventRef] public string trapSound;
		public AnimationClip fallbackAnimation;

		[Header("Events")]
		public UnityEvent onReachEnd;
		public UnityEvent onKeyCharacterFaints;
		#endregion

		#region Class Fields
		[HideInInspector]
		public Dungeon dungeon;
		[HideInInspector]
		public int currentFloor;
		[HideInInspector]
		public uint numberOfTurns;

		private Tile[,] tiles;

		private DungeonBuilder dBuilder;
		private DungeonPopulation dPopulation;
		private DungeonLogic dLogic;
		private DungeonView dView;

		private CameraController cameraController;
		private PostProcessVolume hungerEffect;

		private bool gridOn = false;
		private bool arrowsOn = false;
		private bool sprint = false;
		private bool interruptedSprint = false;
		private Func<bool> inAnimation = null;
		#endregion

		public DungeonPlayer Player { get; private set; }
		public bool IsInTurn { get; private set; } = false;
		public bool IsMenuOpenend => dView.MenuIsOpened;
		public bool CanPlayerAct => !IsInTurn && !gridOn && !interruptedSprint && dView.LoadingIsDone && !IsMenuOpenend;

		public void ProgressOneFloor(bool firstTime = false)
		{
			numberOfTurns = 0;
			IsInTurn = false;

			if (!firstTime)
				RuntimeManager.PlayOneShot(stairsSound);

			if (currentFloor == numberOfFloors)
			{
				onReachEnd.Invoke();
				return;
			}

			dView.Reset(() =>
			{
				dungeon.Generate(maxNumberOfRooms, maxNumberOfDeadEnds, imperfection, floodRatio, fluidType);
				tiles = new Tile[width, height];

				for (int y = 0; y < height; ++y)
				{
					for (int x = 0; x < width; ++x)
					{
						tiles[x, y] = dungeon.GetTile(x, y);
					}
				}

				dBuilder.Build();
				dPopulation.Populate();
				dPopulation.GenerateContext();
				dView.UpdateMap();

				if (!firstTime)
					++currentFloor;
			});
		}

		public IEnumerator ProgressOneTurn()
		{
			if (IsInTurn || !dView.LoadingIsDone || gridOn)
				yield break;

			IsInTurn = true;
			interruptedSprint = false;

			var movingEntities = new List<DungeonCharacter>();

			dView.HideArrows();
			dPopulation.GenerateContext();
			dLogic.Process(Player, movingEntities);

			if (inAnimation != null)
				yield return new WaitWhile(inAnimation);

			IEnumerable<DungeonCharacter> waitingEntites;
			do
			{
				waitingEntites = dPopulation.Decide();

				foreach (var dc in waitingEntites)
				{
					if (dc.desiredAction[0] == "goto")
					{
						dLogic.Process(dc, movingEntities);
					}
					else
					{
						yield return new WaitUntil(() => DungeonPopulation.MoveCharacters(movingEntities, sprint));

						movingEntities.Clear();
						dLogic.Process(dc, movingEntities);
					}
	 
					if (inAnimation != null)
						yield return new WaitWhile(inAnimation);
				}
			}
			while (waitingEntites != null && waitingEntites.Any());

			yield return new WaitUntil(() => dPopulation.MoveEveryone(sprint));

			dPopulation.Evaluate();
			dView.UpdateMap();

			if (sprint && dLogic.ShouldInterruptSprint())
				interruptedSprint = true;

			++numberOfTurns;

			IsInTurn = false;

			if (arrowsOn)
				dView.ShowArrows(Player.position);
		}

		public void ProcessWalkOnLogic(DungeonEntity entity, DungeonCharacter target)
		{
			if (entity is DungeonFloor df)
			{
				if ((df.floor.id == "STAIRS" || df.floor.id == "HSTAIRS") && target is DungeonPlayer && Player.desiredAction[0] != "stepon")
				{
					var options = new string[] { GameText.GetString("_system/progress"), GameText.GetString("_system/cancel") };

					ShowTileInfo(df.floor.name.CorrectTranslation(Game.Lang), options, false, answer =>
					{
						if (answer == 0)
							Player.enforce = new string[] { "stepon" };
					});
				}
				else
				{
					dLogic.ActivateFloor(df, target);
				}
			}
			else if (entity is DungeonItem di)
			{
				dLogic.PickupItem(di, target);
			}
		}

		public void Register(DungeonEntity entity)
		{
			IconType type;

			if (entity is DungeonPlayer dp)
			{
				Player = dp;
				type = IconType.Player;
			}
			else if (entity is DungeonCharacter dc)
			{
				if (dc.enemy) type = IconType.Enemy;
				else type = IconType.Ally;
			}
			else if (entity is DungeonFloor df)
			{
				if (df.floor.id == "STAIRS" || df.floor.id == "HSTAIRS") type = IconType.Stairs;
				else if (df.floor.id == "WONDER") type = IconType.Wonder;
				else type = IconType.Trap;
			}
			else
			{
				type = IconType.Item;
			}

			entity.manager = this;
			dView.AddIconToMap(entity, type);
		}

		public void Unregister(DungeonEntity entity)
		{
			dPopulation.RemoveEntity(entity);
			dView.RemoveIconFromMap(entity);
		}

		public void PlayerSpawned()
		{
			cameraController.target = Player.Transform;
		}

		#region Utility Methods
		public Vector2Int RandomPos(int margin, bool characters = true, bool items = true, bool floors = true)
		{
			Vector2Int res;
			bool clear;

			do
			{
				clear = true;
				res = Vector2Int.RoundToInt(dungeon.RandomRoomPos(margin));

				foreach (DungeonEntity i in dPopulation.Iterate(characters, items, floors))
				{
					if (i.position == res)
					{
						clear = false;
						break;
					}
				}
			}
			while (!clear);

			return res;
		}

		public IEnumerable<DungeonEntity> IterateOnEntities(bool character = true, bool item = true, bool floor = true)
		{
			return dPopulation.Iterate(character, item, floor);
		}

		public List<DungeonEntity> EntitiesAt(Vector2Int pos, bool character = true, bool item = true, bool floor = true)
		{
			return dPopulation.EntitiesAt(pos, character, item, floor);
		}

		public DungeonCharacter CharacterAt(Vector2Int pos, DungeonCharacter notme = null)
		{
			foreach (var c in dPopulation.Iterate(item: false, floor: false))
			{
				if (c.position == pos && (notme == null || (notme != null && c != notme)))
					return c as DungeonCharacter;
			}

			return null;
		}

		public DungeonCharacter CharacterIn(Rect rect)
		{
			foreach (var c in dPopulation.Iterate(item: false, floor: false))
			{
				if (rect.Contains(c.position))
					return c as DungeonCharacter;
			}

			return null;
		}

		public DungeonRoom RoomAt(Vector2Int pos)
		{
			return dungeon.RoomByCoords(pos);
		}

		public Tile TileAt(Vector2Int pos)
		{
			return TileAt(pos.x, pos.y);
		}

		public Tile TileAt(int x, int y)
		{
			if (x < 0 || x >= width || y < 0 || y >= height)
				return Tile.Nothing;

			return tiles[x, y];
		}

		public void DestroyTile(Vector2Int pos)
		{
			dBuilder.DestroyTile(pos);
		}

		public List<DungeonCharacter> GetVictims(Move move, DungeonCharacter user, Vector2Int? directionOverride = null)
		{
			return dLogic.GetVictims(move, user, directionOverride);
		}

		public bool CornerCut(Vector2Int dir, Vector2Int from)
		{
			if (dir.x == 0 || dir.y == 0)
				return false;

			if (dir.y > 0 && TileAt(from + Vector2Int.up) == Tile.Wall)
				return true;
			if (dir.x > 0 && TileAt(from + Vector2Int.right) == Tile.Wall)
				return true;
			if (dir.y < 0 && TileAt(from + Vector2Int.down) == Tile.Wall)
				return true;
			if (dir.x < 0 && TileAt(from + Vector2Int.left) == Tile.Wall)
				return true;

			return false;
		}

		public void AskContext(DungeonCharacter character)
		{
			dPopulation.GenerateContext(character);
		}

		public void AnimateOne(DungeonCharacter user, DungeonCharacter target, DataElement e = null, DealtDamage? damageInflicted = null, LuaScript scr = null)
		{
			AnimationClip clip = fallbackAnimation;
			Action<DungeonCharacter, DungeonCharacter> mark = null;

			var isAlternative = false;

			if (e is Move move)
			{
				isAlternative = user.pokemon.Species.alternativeAnimation.Contains(move.id);

				if (move.animation != null)
					clip = move.animation;

				var effectMark = dLogic.Lua.GetAnimationMark(scr);

				mark = (DungeonCharacter u, DungeonCharacter t) =>
				{
					effectMark?.Invoke(u, t);

					if (damageInflicted.HasValue)
						dView.PopUp(target, damageInflicted.Value);
				};
			}
			else if (e is Item item)
			{
				clip = item.animation;
				mark = dLogic.Lua.GetAnimationMark(scr);
			}
			else if (e is Floor floor)
			{
				clip = floor.animation;
				mark = dLogic.Lua.GetAnimationMark(scr);
			}
			else if (e == null && damageInflicted.HasValue) // It's a regular attack
			{
				mark = (DungeonCharacter u, DungeonCharacter t) => dView.PopUp(target, damageInflicted.Value);
			}

			// The additional test for "Warp" is because the warping might make the user visible mid-animation
			if (clip == null || (!user.character.Visible && clip.name != "Warp"))
			{
				mark?.Invoke(user, target);
				return;
			}

			var a = BattleAnimation.Animate(user, target, clip, mark, isAlternative);

			inAnimation = () =>
			{
				if (a == null)
				{
					inAnimation = null;
					return false;
				}

				return true;
			};
		}

		public void AnimateMultiple(DungeonCharacter user, List<DungeonCharacter> targets, DataElement e = null, List<DealtDamage> damageInflicted = null, LuaScript scr = null)
		{
			if (targets.Count == 0)
			{
				AnimateOne(user, null, e, null, scr);
				return;
			}

			if (targets.Count == 1)
			{
				AnimateOne(user, targets[0], e, damageInflicted.Count == 1 ? damageInflicted[0] : (DealtDamage?)null, scr);
				return;
			}
		}

		public void TestFainted()
		{
			dPopulation.TestFainted();
		}

		public void CheckPlayerGround()
		{
			dPopulation.CheckPlayerGround();
		}
		#endregion

		#region Handles
		public void LogMessage(string message)
		{
			dView.LogMessage(message);
		}

		public void UpdateMap()
		{
			dView.UpdateMap();
		}

		public void ToggleMenu()
		{
			if (!IsInTurn)
			{
				dView.ToggleMenu();
				Player.ResetAction();
			}
		}

		public void ShowTileInfo(string message, string[] choices, bool fromMenu=false, Action<int> callback = null)
		{
			dView.ShowTileInfo(message, choices, fromMenu, callback);
		}

		public void ShowGrid(bool show)
		{
			if (show)
			{
				if (!gridOn)
				{
					gridOn = true;
					UpdateGrid();
				}
			}
			else
			{
				gridOn = false;
				dView.HideGrid();
			}
		}

		public void UpdateGrid()
		{
			if (!gridOn)
				return;
			
			var r = RoomAt(Player.position);
			Rect zone;

			if (r == null)
			{
				zone = Rect.MinMaxRect
				(
					Player.position.x - 3,
					Player.position.y - 3,
					Player.position.x + 3,
					Player.position.y + 3
				);
			}
			else
			{
				zone = r.rectBox;
			}

			dView.ShowGrid(zone, Player.position, Player.LookAt);
		}

		public void ShowArrows(bool show)
		{
			arrowsOn = show;
			
			if (show && !IsInTurn)
				dView.ShowArrows(Player.position);
			else
				dView.HideArrows();
		}

		public bool GetSprint()
		{
			return sprint;
		}

		public void SetSprint(bool setSprint)
		{
			sprint = setSprint;

			if (interruptedSprint && !setSprint)
				interruptedSprint = false;
		}

		public void ShowMoves(bool show)
		{
			if (show)
				dView.ShowMoves(); // "Show me your moves" - Captain Falcon
			else
				dView.HideMoves();
		}

		public void PlaySound(string @event)
		{
			RuntimeManager.PlayOneShot(@event);
		}

		public void TalkTo(DungeonCharacter character)
		{
			// TODO Implement characters talking in dungeons
		}
		#endregion

		#region Component Methods
		void Start()
		{
			// Initialize
			dungeon = new Dungeon(width, height, roomProfiles);
			currentFloor = startFloor;

			// Instantiate
			dBuilder = new DungeonBuilder(this);
			dPopulation = new DungeonPopulation(this);
			dLogic = new DungeonLogic(this);

			var hudManager = GameObject.FindWithTag("DungeonHUD").GetComponent<DungeonHUDManager>();
			dView = new DungeonView(this, hudManager);
			hudManager.view = dView;

			// Get components
			cameraController = GameObject.FindWithTag("MainCamera").GetComponent<CameraController>();
			hungerEffect = cameraController.GetComponentInChildren<PostProcessVolume>();

			// Format parameters
			Array.Sort(obstacles, (a, b) => a.chance.CompareTo(b.chance));

			if (Game.Veteran)
				numberOfFloors = (int)Mathf.Floor(numberOfFloors * Game.VETERAN_FLOOR_MULTIPLIER);

			// Start
			ProgressOneFloor(true);
		}

		void Update()
		{
			if (Player != null && !IsInTurn && dView.LoadingIsDone && !IsMenuOpenend)
				Player.Update();

			if (Player != null && Player.pokemon.Hunger <= HUNGER_START_EFFECT)
				hungerEffect.weight = 1f - (float)Player.pokemon.Hunger / HUNGER_START_EFFECT;
		}

		void LateUpdate()
		{
			if (Player == null)
				return;
			
			if (CanPlayerAct)
				Player.WaitFor(() => StartCoroutine(ProgressOneTurn()));
			else
				Player.ResetAction();
		}
		#endregion
	}
}
