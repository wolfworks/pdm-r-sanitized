﻿using Character;
using Dungeon.Entity;
using Dungeon.UI;
using Pokemon.Lexic;
using System;
using System.Collections.Generic;
using UnityEngine;

using Object = UnityEngine.Object;

namespace Dungeon.Core
{
	public class DungeonView
	{
		private readonly DungeonHUDManager hudManager;

		private GameObject currentArrows;
		private List<GameObject> currentGrid;

		public bool LoadingIsDone { get; set; }
		public DungeonManager Manager { get; private set; }
		public bool MenuIsOpened => hudManager.Menu.Opened;

		public DungeonView(DungeonManager m, DungeonHUDManager hm)
		{
			Manager = m;
			hudManager = hm;

			currentGrid = new List<GameObject>();
		}

		public void Reset(Action loadEverythingElse)
		{
			LoadingIsDone = false;

			Manager.StartCoroutine(hudManager.UI.FloorSequence(() =>
			{
				hudManager.ResetState();
				loadEverythingElse();
			}));
		}

		#region Handles
		public void UpdateMap()
		{
			hudManager.Map.Unveil(Manager.Player.position.x, Manager.Player.position.y);
		}

		public void AddIconToMap(DungeonEntity entity, IconType type)
		{
			hudManager.Map.AddIcon(entity, type);
		}

		public void RemoveIconFromMap(DungeonEntity entity)
		{
			hudManager.Map.RemoveIcon(entity);
		}

		public void ShowMoves()
		{
			hudManager.UI.MoveIndicatorVisible(true);
		}

		public void HideMoves()
		{
			hudManager.UI.MoveIndicatorVisible(false);
		}

		public void ToggleMenu()
		{
			if (hudManager.Menu.Opened)
			{
				hudManager.Menu.Close();
			}
			else
			{
				hudManager.Log.HideLog();
				hudManager.Menu.Open();
			}
		}

		public void ShowTileInfo(string message, string[] choices, bool fromMenu, Action<int> callback)
		{
			hudManager.Log.HideLog();
			hudManager.Menu.ShowTileInfo(message, choices, fromMenu, callback);
		}

		public void LogMessage(string message)
		{
			hudManager.Log.AddMessage(message);
		}
		#endregion

		#region Utility Methods
		public void PopUp(DungeonCharacter character, DealtDamage damage)
		{
			var pos = character.character.GetComponent<CharacterManager>().head.position;

			var pp = Object.Instantiate(Manager.popup, pos + Vector3.up * 0.25f, Quaternion.identity);
			var pptxt = pp.GetComponentInChildren<TextMesh>();

			if (damage.isFailed)
			{
				pptxt.text = GameText.GetString("_system/missed");
				pptxt.color = new Color(0.43f, 0.76f, 0.23f);
			}
			else
			{
				pptxt.text = (damage.isHealing ? "+" : "") + damage.amount.ToString();
				pptxt.color = damage.isHealing ? Color.green : Color.yellow;
			}

			string message;
			
			if (damage.isFailed)
			{
				message = GameText.GetString("_actions/miss");
			}
			else if (damage.isHealing)
			{
				if (character.pokemon.HP == character.pokemon.GetStat(Stat.hp))
					message = GameText.GetString("_actions/fullregen");
				else
					message = GameText.GetString("_actions/regen");
			}
			else
			{
				message = GameText.GetString("_actions/hit");

				if (damage.isCritical)
					LogMessage(GameText.GetString("_actions/critical"));

				if (damage.effectiveness < 0)
					LogMessage(GameText.GetString("_actions/ineffective"));
				else if (damage.effectiveness > 0)
					LogMessage(GameText.GetString("_actions/effective"));
			}

			var finalMessage = string.Format(message, TextColor.GetColoredName(character), damage.amount);
			LogMessage(finalMessage);
		}

		public void ShowGrid(Rect zone, Vector2Int pos, Vector2Int rot)
		{
			if (currentGrid.Count != 0) HideGrid();

			for (int y = (int)Mathf.Floor(zone.y); y < (int)Mathf.Floor(zone.yMax); ++y)
			{
				for (int x = (int)Mathf.Floor(zone.x); x < (int)Mathf.Floor(zone.xMax); ++x)
				{
					if (Manager.TileAt(x, y) != Tile.Wall)
					{
						var o = Object.Instantiate(Manager.gridElement, new Vector3(x, 0f, y), Quaternion.identity);

						int a = rot.y;
						int b = rot.x * -1;
						int c = a * pos.x * -1 - b * pos.y;

						if ((a * x + b * y + c == 0 && (x - pos.x) < 0 == rot.x < 0 && (y - pos.y) < 0 == rot.y < 0) || (x == pos.x && y == pos.y))
							o.GetComponentInChildren<SpriteRenderer>().sprite = Manager.gridActive;

						currentGrid.Add(o);
					}
				}
			}
		}

		public void HideGrid()
		{
			foreach (GameObject o in currentGrid)
				Object.Destroy(o);

			currentGrid = new List<GameObject>();
		}

		public void ShowArrows(Vector2Int pos)
		{
			if (currentArrows == null)
				currentArrows = Object.Instantiate(Manager.diagArrows, new Vector3(pos.x, 0.15f, pos.y), Quaternion.identity);
		}

		public void HideArrows()
		{
			if (currentArrows != null)
			{
				Object.Destroy(currentArrows);
				currentArrows = null;
			}
		}
		#endregion
	}
}
