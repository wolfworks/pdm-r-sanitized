﻿#pragma warning disable IDE0051 // Remove unused private members
using UnityEngine;
using UnityEngine.InputSystem;

namespace Dungeon.Core
{
	[RequireComponent(typeof(DungeonManager))]
	public class DungeonController : MonoBehaviour
	{
		private const float MAGNITUDE_THRESHOLD = 0.9f;

		private DungeonManager _manager;
		private Vector2 _currentMovement = Vector2.zero;

		#region Component Messages
		private void Awake()
		{
			_manager = GetComponent<DungeonManager>();
		}

		private void LateUpdate()
		{
			if (_currentMovement != Vector2.zero && !_manager.IsInTurn && !_manager.IsMenuOpenend)
				_manager.Player.SendVector(_currentMovement);
		}

		private void OnMove(InputValue value)
		{
			var vector = value.Get<Vector2>();

			if (vector.magnitude < MAGNITUDE_THRESHOLD)
			{
				_currentMovement = Vector2.zero;
				return;
			}

			if (_manager.IsMenuOpenend)
				return;

			_currentMovement = vector.Round();
		}

		private void OnUseMove1(InputValue _)
		{
			if (_manager.IsMenuOpenend)
				return;

			_manager.Player.SendMove(0);
		}

		private void OnUseMove2(InputValue _)
		{
			if (_manager.IsMenuOpenend)
				return;

			_manager.Player.SendMove(1);
		}

		private void OnUseMove3(InputValue _)
		{
			if (_manager.IsMenuOpenend)
				return;

			_manager.Player.SendMove(2);
		}

		private void OnUseMove4(InputValue _)
		{
			if (_manager.IsMenuOpenend)
				return;

			_manager.Player.SendMove(3);
		}

		private void OnAction(InputValue _)
		{
			if (_manager.IsMenuOpenend)
				return;

			_manager.Player.SendRegularAttack();
		}

		private void OnSkipTurn(InputValue _)
		{
			if (_manager.IsMenuOpenend)
				return;

			_manager.Player.SendSkipTurn();
		}

		private void OnRun(InputValue value)
		{
			_manager.SetSprint(_currentMovement == Vector2.zero && value.isPressed);
		}

		private void OnGrid(InputValue value)
		{
			_manager.ShowGrid(value.isPressed);
		}

		private void OnShowMoves(InputValue value)
		{
			_manager.ShowMoves(value.isPressed);
		}

		private void OnMenu(InputValue _)
		{
			_manager.ToggleMenu();
		}

		private void OnDiagonal(InputValue value)
		{
			_manager.ShowArrows(value.isPressed);
			_manager.Player.diagonalMovement = value.isPressed;
		}
		#endregion
	}
}
#pragma warning restore IDE0051 // Remove unused private members