﻿using Dungeon.AI;
using Dungeon.Entity;
using Pokemon;
using Pokemon.Data;
using Pokemon.Lexic;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dungeon.Core
{
	public class DungeonPopulation
	{
		private DungeonManager manager;

		private List<DungeonCharacter> characters;
		private List<DungeonFloor> floors;
		private List<DungeonItem> items;
		private GameObject population;

		public DungeonPopulation(DungeonManager m)
		{
			manager = m;
		}

		public void Populate()
		{
			if (population != null)
				Object.Destroy(population);

			characters = new List<DungeonCharacter>();
			items = new List<DungeonItem>();
			floors = new List<DungeonFloor>();

			population = new GameObject("DungeonPopulation");

			#region Characters
			// Player
			var playerPos = manager.RandomPos(2);
			var playerEntity = new DungeonPlayer(Game.TeamGetPokemonAt(0));

			characters.Add(playerEntity);
			manager.Register(playerEntity);

			playerEntity.Spawn(playerPos);
			playerEntity.ParentTo(population.transform);
			playerEntity.Attach(Object.Instantiate(manager.marker).transform);

			// Allies
			for (int i = 1; i < 4; ++i)
			{
				if (Game.TeamGetPokemonAt(i) == null) continue;

				var allyPos = playerPos;

				switch (i)
				{
					case 1: allyPos += Vector2Int.down; break;
					case 2: allyPos += Vector2Int.right; break;
					case 3: allyPos += Vector2Int.left; break;
				}

				// TODO : Save the player preferences and use them
				var allyAI = new DungeonAI();
				allyAI.SetFlags(AIFlag.FollowLeader | AIFlag.AvoidTraps | AIFlag.PrioritizeHighPPMoves | AIFlag.CanMove);

				var ally = new DungeonCharacter(Game.TeamGetPokemonAt(i), allyAI, false, i);

				characters.Add(ally);
				manager.Register(ally);

				var marker = Object.Instantiate(manager.marker);
				marker.GetComponentInChildren<SpriteRenderer>().sprite = manager.markerAlly;

				ally.Spawn(allyPos);
				ally.ParentTo(population.transform);
				ally.Attach(marker.transform);
			}

			// Enemies
			for (int i = 0; i < manager.maxNumberOfEnemies; ++i)
				SpawnEnemy(true);
			#endregion

			#region Floors
			// Stairs
			var stairsPos = manager.RandomPos(1, characters: false);
			var stairsEntity = new DungeonFloor("STAIRS".As<Floor>(), manager.stairs, true);

			floors.Add(stairsEntity);
			manager.Register(stairsEntity);

			stairsEntity.Spawn(stairsPos);
			stairsEntity.ParentTo(population.transform);
			manager.DestroyTile(stairsPos);

			// Other floors
			for (int i = 0; i < manager.maxNumberOfTraps; ++i)
			{
				var trap = manager.possibleTraps[Random.Range(0, manager.possibleTraps.Length)];

				if (Random.value > trap.chance) continue;

				var trapPos = manager.RandomPos(1, characters: false);
				var trapEntity = new DungeonFloor(trap.id.As<Floor>(), null, trap.id == "WONDER");

				floors.Add(trapEntity);
				manager.Register(trapEntity);

				trapEntity.Spawn(trapPos);

				if (trap.id == "WONDER")
				{
					manager.DestroyTile(trapPos);
					trapEntity.ParentTo(population.transform);
				}
				else Debug.DrawRay(new Vector3(trapPos.x, 0f, trapPos.y), Vector3.up, Color.white, 50f);
			}
			#endregion

			#region Items
			for (int i = 0; i < manager.maxNumberOfItems; ++i)
			{
				var item = manager.possibleItems[Random.Range(0, manager.possibleItems.Length)];

				if (Random.value > item.chance) continue;

				var itemPos = manager.RandomPos(1);
				var source = item.id.As<Item>();
				int ammo = 0;

				if (item.id == "MONEY")
					ammo = Random.Range(0, manager.maxAmountOfMoney);
				else if (source.stackable)
					ammo = Random.Range(0, manager.maxAmountOfAmmo);

				var itemState = new ItemState(source, ammo, Random.value > manager.itemsStickyChance);
				var itemEntity = new DungeonItem(itemState);

				items.Add(itemEntity);
				manager.Register(itemEntity);

				itemEntity.Spawn(itemPos);
				itemEntity.ParentTo(population.transform);
			}
			#endregion
		}

		public void RemoveEntity(DungeonEntity entity)
		{
			if (entity is DungeonCharacter dc)
				characters.Remove(dc);
			else if (entity is DungeonFloor df)
				floors.Remove(df);
			else if (entity is DungeonItem di)
				items.Remove(di);

			GenerateContext();
		}

		public void SpawnEnemy(bool allowPlayerRoom = false)
		{
			Vector2Int enemyPos;

			do
			{
				enemyPos = manager.RandomPos(1, floors: false);
			}
			while (!allowPlayerRoom && manager.RoomAt(manager.Player.position) != manager.RoomAt(enemyPos));

			var enemySpecs = manager.possibleEnemies[Random.Range(0, manager.possibleEnemies.Length)];
			int l = Random.Range(enemySpecs.levelRange.x, enemySpecs.levelRange.y + 1) + (Game.Veteran ? Game.VETERAN_LEVEL_ADD : 0);

			var enemyState = new PokemonState(enemySpecs.species.As<Species>(), (byte)l);

			var enemyAI = new DungeonAI();
			enemyAI.SetFlags(AIFlag.EngageEnemies | AIFlag.PrioritizeHighPPMoves | AIFlag.TryRegen);

			if (!enemySpecs.standStill)
				enemyAI.AddFlag(AIFlag.CanMove);

			if (Game.Veteran)
				enemyAI.AddFlag(AIFlag.FollowLeader | AIFlag.FetchItems | AIFlag.PrioritizeWeakEnemies | AIFlag.AvoidTraps);

			var enemyEntity = new DungeonCharacter(enemyState, enemyAI, true, enemySpecs.aiPriority);

			characters.Add(enemyEntity);
			manager.Register(enemyEntity);

			var marker = Object.Instantiate(manager.marker);
			marker.GetComponentInChildren<SpriteRenderer>().sprite = manager.markerEnemy;

			enemyEntity.Spawn(enemyPos);
			enemyEntity.ParentTo(population.transform);
			enemyEntity.Attach(marker.transform);
		}

		public void GenerateContext()
		{
			for (int i = 0; i < characters.Count; ++i)
				GenerateContext(characters[i]);
		}

		public void GenerateContext(DungeonCharacter dc)
		{
			var context = new RoomContext
			{
				sender = dc
			};

			context.room = manager.RoomAt(dc.position);

			var visibleAllies = new List<DungeonCharacter>();
			var visibleEnemies = new List<DungeonCharacter>();
			var visibleItems = new List<DungeonItem>();
			var visibleFloors = new List<DungeonFloor>();

			foreach (DungeonEntity de in Iterate())
			{
				var available = false;

				if (context.room != null)
					available = context.room.rectBox.Contains(de.position);

				if (!available)
					available = Vector2.Distance(dc.position, de.position) <= DungeonManager.VIEW_DISTANCE;
					
				if (!available)
					continue;

				if (de is DungeonCharacter dc2)
				{
					if (dc == dc2)
						continue;

					if (dc.enemy != dc2.enemy)
						visibleEnemies.Add(dc2);
					else
						visibleAllies.Add(dc2);
				}
				else if (de is DungeonItem di)
				{
					visibleItems.Add(di);
				}
				else if (de is DungeonFloor df)
				{
					if (df.revealed) visibleFloors.Add(df);
				}
			}

			context.visibleEnemies = visibleEnemies;
			context.visibleAllies = visibleAllies;
			context.visibleItems = visibleItems;
			context.visibleFloors = visibleFloors;

			dc.SetContext(context);
		}

		public IEnumerable<DungeonCharacter> Decide()
		{
			for (var i = 0; i < characters.Count; ++i)
			{
				var dc = characters[i];

				if (dc is DungeonPlayer) continue;
				if (dc.hasPlayed) continue;
				if (dc.moveCount >= dc.pokemon.statBoosts[(int)Stat.spe]) continue;
			
				dc.Decide();

				if (dc.desiredAction != null)
				{
					if (dc.desiredAction[0] == "goto")
						++dc.moveCount;
					else
						dc.hasPlayed = true;

					yield return dc;
				}
			}
		}

		public static bool MoveCharacters(DungeonCharacter character, bool sprint)
		{
			return character.Move(sprint); ;
		}

		public static bool MoveCharacters(IEnumerable<DungeonCharacter> movingCharacters, bool sprint)
		{
			var everyoneIsDone = true;

			foreach (DungeonCharacter dc in movingCharacters)
				everyoneIsDone &= dc.Move(sprint);

			return everyoneIsDone;
		}

		public bool MoveEveryone(bool sprint)
		{
			return MoveCharacters(characters, sprint);
		}

		public void Evaluate()
		{
			foreach (var dc in characters)
			{
				dc.moveCount = 0;
				dc.hasPlayed = false;

				if (!manager.TileAt(dc.position).IsCrossableBy(dc.pokemon))
				{
					// TODO Warp the character somewhere else
				}

				if (dc.desiredAction != null && (dc.desiredAction[0] == "goto" || dc.desiredAction[0] == "stepon"))
				{
					foreach (var entity in EntitiesAt(dc.position, character: false))
					{
						if (entity is DungeonFloor df)
						{
							if (!df.revealed)
							{
								df.Reveal();
								df.ParentTo(population.transform);
								manager.DestroyTile(df.position);
							}
						}

						manager.ProcessWalkOnLogic(entity, dc);
					}
				}

				dc.lastAction = dc.desiredAction;
				dc.SetEntityInFront();
			}
		}

		public void CheckPlayerGround()
		{
			var collidingWith = EntitiesAt(manager.Player.position, character: false).FirstOrDefault();
   
			if (collidingWith == null)
			{
				manager.ShowTileInfo(
					string.Format(GameText.GetString("_actions/trap_nothing"), TextColor.GetColoredName(manager.Player)),
					new string[] { GameText.GetString("_system/cancel") },true
				);
			}
			else
			{
				if (collidingWith is DungeonFloor df)
				{
					string choiceText;

					if (df.floor.id == "STAIRS" || df.floor.id == "HSTAIRS")
						choiceText = GameText.GetString("_system/progress");
					else
						choiceText = GameText.GetString("_system/stepon");

					var options = new string[] { choiceText, GameText.GetString("_system/cancel") };

					manager.ShowTileInfo(df.floor.name.CorrectTranslation(Game.Lang), options,true, answer =>
					{
						if (answer == 0)
							manager.Player.enforce = new string[] { "stepon" };
					});
				}
				else if (collidingWith is DungeonItem di)
				{
					// TODO
				}
			}
		}

		#region Utility Methods
		public void TestFainted()
		{
			for (int i = 0; i < characters.Count; ++i)
				characters[i].TestIfFainted();
		}

		public IEnumerable<DungeonEntity> Iterate(bool character = true, bool item = true, bool floor = true)
		{
			if (character)
			{
				foreach (DungeonEntity de in characters)
					yield return de;
			}

			if (item)
			{
				foreach (DungeonEntity de in items)
					yield return de;
			}

			if (floor)
			{
				foreach (DungeonEntity de in floors)
					yield return de;
			}
		}

		public List<DungeonEntity> EntitiesAt(Vector2Int position, bool character = true, bool item = true, bool floor = true)
		{
			var res = new List<DungeonEntity>();

			foreach (DungeonEntity de in Iterate(character, item, floor))
			{
				if (de.position == position)
					res.Add(de);
			}

			return res;
		}
		#endregion
	}
}
