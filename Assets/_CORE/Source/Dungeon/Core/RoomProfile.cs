﻿using System;
using UnityEngine;

namespace Dungeon.Core
{
	[Serializable]
	public class RoomProfile
	{
		public int minSize;
		public int maxSize;
		[Range(4, 8)]
		public int edges = 4;
		public int margin;
		[Range(0f, 1f)]
		public float deformation;
	}
}
