﻿using Delaunay.Geo;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Core
{
	public struct Zone
	{
		private List<Vector2Int> positions;
		private LineSegment line;
		private Rect? rect;

		public static Zone Empty => new Zone();

		public Zone(List<Vector2Int> _positions)
		{
			positions = _positions;
			line = null;
			rect = null;
		}

		public Zone(LineSegment _line)
		{
			positions = null;
			line = _line;
			rect = null;
		}

		public Zone(Rect _rect)
		{
			positions = null;
			line = null;
			rect = _rect;
		}

		public bool IsIn(Vector2Int pos)
		{
			if (positions != null)
				return positions.Contains(pos);

			if (line != null)
			{
				for (int i = 0; i <= Game.MAX_MOVE_RANGE; ++i)
				{
					if (pos == line?.p0 + (line?.p1 * i))
						return true;
				}

				return false;
			}

			if (rect != null)
				return ((Rect)rect).Contains(pos);

			return false;
		}
		
		public IEnumerator<Vector2Int> Sweep()
		{
			if (positions != null)
			{
				foreach (var p in positions)
					yield return p;
			}

			if (line != null)
			{
				for (int i = 0; i <= Game.MAX_MOVE_RANGE; ++i)
					yield return Vector2Int.RoundToInt((Vector2)(line?.p0 + (line?.p1 * i)));
			}

			if (rect != null)
			{
				var r = (Rect)rect;

				for (int y = (int)r.y; y < r.yMax; ++y)
				{
					for (int x = (int)r.x; x < r.xMax; ++x)
					{
						yield return new Vector2Int(x, y);
					}
				}
			}
		}

		public override bool Equals(object obj)
		{
			if (obj is Zone other)
			{
				if (GetType() != other.GetType())
					return false;
			
				if (ReferenceEquals(this, other))
					return true;

				return this == other;
			}

			return false;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public static bool operator ==(Zone a, Zone b)
		{
			return a.positions == b.positions
				&& a.line == b.line
				&& a.rect == b.rect;
		}

		public static bool operator !=(Zone a, Zone b)
		{
			return a.positions != b.positions
				|| a.line != b.line
				|| a.rect != b.rect;
		}
	}
}
