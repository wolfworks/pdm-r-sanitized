﻿using Dungeon.UI.Menus;
using Pokemon.Data;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dungeon.UI
{
	public class DungeonMenu : MonoBehaviour
	{
		#region Component Fields
		[Header("Menus")]
		public GameObject[] menus;

		[Header("TileInfo")]
		public GameObject tileMenu;
		public Text tileMessage;
		public Transform tilePanel;
		public GameObject choiceButton;

		[Header("Info")]
		public Text dungeonInfo;
		public Text weather;
		public Text money;
		#endregion

		public DungeonHUDManager HUD { get; set; }
		public bool Opened { get; private set; }

		private int currentMenu;
		private bool tileMenuActive;

		public void ResetState()
		{
			currentMenu = 0;
			Opened = false;

			foreach (var menu in menus)
				menu.SetActive(false);

			tileMenu.SetActive(false);
		}

		public void Open(int menuId = 0)
		{
			if (Opened) return;

			Opened = true;
			menus[menuId].SetActive(true);
			currentMenu = menuId;
		}

		public void Close()
		{
			if (!Opened) return;

			if (tileMenuActive)
			{
				tileMenu.SetActive(false);

				foreach (Transform child in tilePanel)
					Destroy(child.gameObject, 0.04f);

				tileMenuActive = false;
			}

			Opened = false;
			menus[currentMenu].SetActive(false);
		}

		public void SwitchMenu(string menuName)
		{
			var id = GetMenuByName(menuName);

			if (!Opened)
			{
				Open(id);
			}
			else
			{
				menus[currentMenu].SetActive(false);

				currentMenu = id;

				menus[currentMenu].SetActive(true);
			}
		}

		public void ShowTileInfo(string message, string[] choices, bool fromMenu, Action<int> callback)
		{
			if (tileMenuActive)
				return;

			Close();

			tileMenuActive = true;
			Opened = true;

			// Message
			tileMessage.text = message;

			// Choices
			for (int i = 0; i < choices.Length; ++i)
			{
				var obj = Instantiate(choiceButton, tilePanel);

				if (i == 0)
					obj.AddComponent<DefaultSelected>();

				obj.GetComponent<Text>().text = choices[i];

				int index = i;
				obj.GetComponent<Button>().onClick.AddListener(() =>
				{
					Close();

					if(fromMenu)
						SwitchMenu("Menu Top");

					callback?.Invoke(index);
				});

				obj.GetComponent<EventTrigger>().triggers.First(x => x.eventID == EventTriggerType.Cancel).callback.AddListener(_ =>
				{
					Close();

					if (fromMenu)
						SwitchMenu("Menu Top");
				});
			}

			tileMenu.SetActive(true);
		}

		public void CheckFeet()
		{
			HUD.Manager.CheckPlayerGround();
		}

		#region Component Messages
		private void Update()
		{
			var name = GameText.GetString(HUD.Manager.dungeonName);
			var floor = HUD.GetFloorText(TextColor.darkYellow);

			dungeonInfo.text = $"{name} {floor}";
			weather.text = ""; /// TODO
			money.text = TextColor.Colorize(Game.Money.ToString(), TextColor.darkYellow) + " " + "MONEY".As<Item>().name.CorrectTranslation();
		}
		#endregion

		#region Private Methods
		private int GetMenuByName(string menuName)
		{
			for (var i = 0; i < menus.Length; ++i)
			{
				if (menus[i].name == menuName)
					return i;
			}

			return -1;
		}
		#endregion
	}
}