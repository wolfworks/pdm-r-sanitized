﻿namespace Dungeon.UI
{
	public enum IconType
	{
		Player,
		Ally,
		Enemy,
		Item,
		Trap,
		Wonder,
		Stairs
	}
}
