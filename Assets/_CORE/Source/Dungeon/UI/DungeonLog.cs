﻿using Dungeon.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI
{
	public class DungeonLog : MonoBehaviour
	{
		#region Component Fields
		public GameObject log;
		public ScrollRect fullLog;
		#endregion

		private Coroutine logCoroutine;
		private List<string> messageLog;

		public DungeonHUDManager HUD { get; set; }

		public void ResetState()
		{
			messageLog = new List<string>();
			log.GetComponentInChildren<Text>().text = "";
		}

		public void AddMessage(string message)
		{
			if (logCoroutine != null)
				StopCoroutine(logCoroutine);

			messageLog.Add(message);

			log.GetComponentInChildren<Text>().text += "\n" + message;
			fullLog.GetComponentInChildren<Text>().text += "\n" + message;
			log.gameObject.SetActive(true);

			logCoroutine = StartCoroutine(DisableLog());
		}

		public void HideLog()
		{
			if (logCoroutine != null)
				StopCoroutine(logCoroutine);

			log.gameObject.SetActive(false);
		}

		#region Private Methods
		private IEnumerator DisableLog()
		{
			yield return new WaitForSeconds(DungeonManager.LOG_STAY_DURATION);
			log.gameObject.SetActive(false);
		}
		#endregion

		#region Component Methods
		void Update()
		{
			if (HUD.Menu.Opened && fullLog.gameObject.activeSelf)
			{
				float multiplicator = 16f / fullLog.GetComponentInChildren<Text>().preferredHeight;
				float newValue = Mathf.Clamp01(fullLog.verticalNormalizedPosition + Input.GetAxisRaw("Vertical") * multiplicator);

				fullLog.verticalNormalizedPosition = newValue;
			}
		}
		#endregion
	}
}