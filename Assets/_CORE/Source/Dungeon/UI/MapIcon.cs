﻿using Dungeon.Entity;
using UnityEngine.UI;

namespace Dungeon.UI
{
	public struct MapIcon
	{
		public DungeonEntity entity;
		public Image icon;
		public IconType type;

		public MapIcon(DungeonEntity _entity, Image _icon, IconType _type)
		{
			entity = _entity;
			icon = _icon;
			type = _type;
		}
	}
}
