﻿using Pokemon.Data;
using Pokemon.Lexic;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI
{
	public class DungeonInterface : MonoBehaviour
	{
		public const string HUD_YELLOW = "#ffe866";
		public const string HUD_RED = "#ff6666";
		public const string HUD_CYAN = "#66e0ff";

		#region Component Fields
		public Text floorIndicator;
		public Text dungeonNameIndicator;
		public Text weatherIndicator;
		public Text leaderIndicator;
		public Image faceIndicator;
		public Text levelIndicator;
		public RectTransform moveIndicator;
		public Text HPIndicator;
		public Image HPGauge;
		public Text HungerIndicator;
		public Image HungerGauge;
		public Text entryDungeonNameIndicator;
		public Image entryFade;
		#endregion

		private float fadeAlpha;
		private float nameAlpha;
		private float movePos;
		private int fadeDir;
		private int nameDir;
		private int moveDir;

		public DungeonHUDManager HUD { get; set; }

		private bool MenuMemory { get; set; }

		public void ResetState()
		{
			
		}

		public IEnumerator FloorSequence(Action loading)
		{
			fadeDir = 1;
			yield return new WaitForSeconds(0.8f);

			loading();

			entryDungeonNameIndicator.text = $"{GameText.GetString(HUD.Manager.dungeonName)}\n{HUD.GetFloorText()}";

			nameDir = 1;
			yield return new WaitForSeconds(1.2f);

			nameDir = -1;
			yield return new WaitForSeconds(0.8f);

			fadeDir = -1;
			yield return new WaitForSeconds(0.8f);

			HUD.view.LoadingIsDone = true;
		}

		public void MoveIndicatorVisible(bool show)
		{
			moveDir = show ? 1 : -1;
		}

		#region Component Methods
		void Start()
		{
			fadeAlpha = 1f;
			nameAlpha = 0f;
			movePos = 60f;
			fadeDir = 1;
			nameDir = 0;
			moveDir = -1;
		}

		void Update()
		{
			var focusOn = Game.TeamGetPokemonAt(HUD.focusOn);

			floorIndicator.text = HUD.GetFloorText(HUD_YELLOW);
			dungeonNameIndicator.text = GameText.GetString(HUD.Manager.dungeonName);
			weatherIndicator.text = ""; /// TODO
			leaderIndicator.text = focusOn.nickname;

			levelIndicator.text = "<size=12>" + GameText.GetString("_system/ab_level") + ".</size>" + TextColor.Colorize(focusOn.Level.ToString(), HUD_YELLOW);

			var maxHP = focusOn.GetStat(Stat.hp);

			HPIndicator.text = TextColor.Colorize(Mathf.Floor(focusOn.HP).ToString(), HUD_CYAN) + "/" + maxHP;
			HPGauge.fillAmount = focusOn.HP / maxHP;

			HungerIndicator.text = TextColor.Colorize(focusOn.Hunger.ToString(), HUD_CYAN) + "/" + 100;
			HungerGauge.fillAmount = (float)focusOn.Hunger / 100;

			var emotion = focusOn.Hunger == 0 || focusOn.HP / maxHP < 0.25f ? PortraitEmotion.Enduring : PortraitEmotion.Neutral;
			faceIndicator.sprite = focusOn.Species.portraits[(int)emotion];

			for (int i = 0; i < 4; ++i)
			{
				var indicator = moveIndicator.GetChild(i).gameObject;
				var move = focusOn.Moves(i);

				if (move == null)
				{
					indicator.SetActive(false);
				}
				else
				{
					indicator.SetActive(true);
					indicator.GetComponent<Image>().sprite = move.type.As<PokemonType>().moveIndicator;

					indicator.transform.GetChild(1).GetComponent<Text>().text = move.name.CorrectTranslation(Game.Lang);
					indicator.transform.GetChild(2).GetComponent<Text>().text = TextColor.Colorize(focusOn.PP[i].ToString(), focusOn.PP[i] == 0 ? HUD_RED : HUD_CYAN) + "/" + move.numberOfPP.ToString();
				}
			}

			if (HUD.Menu.Opened != MenuMemory)
			{
				MenuMemory = HUD.Menu.Opened;

				foreach (Transform child in transform)
					child.gameObject.SetActive(!HUD.Menu.Opened);
			}

			if (!HUD.Menu.Opened)
			{
				fadeAlpha = Mathf.Clamp01(fadeAlpha + Time.deltaTime * fadeDir * 2f);
				nameAlpha = Mathf.Clamp01(nameAlpha + Time.deltaTime * nameDir * 2f);
				movePos = Mathf.Clamp(movePos + Time.deltaTime * -moveDir * 1000f, -10f, 220f);

				entryFade.color = new Color(0f, 0f, 0f, fadeAlpha);
				entryDungeonNameIndicator.color = new Color(1f, 1f, 1f, nameAlpha);
				moveIndicator.anchoredPosition = new Vector2(movePos, moveIndicator.anchoredPosition.y);
			}
		}
		#endregion
	}
}