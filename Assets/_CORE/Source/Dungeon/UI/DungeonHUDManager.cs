﻿using Dungeon.Core;
using UnityEngine;

namespace Dungeon.UI
{
	public class DungeonHUDManager : MonoBehaviour
	{
		#region Component Fields
		public int focusOn = 0;
		#endregion

		public DungeonView view;
	
		public DungeonMap Map { get; private set; }
		public DungeonInterface UI { get; private set; }
		public DungeonMenu Menu { get; private set; }
		public DungeonLog Log { get; private set; }

		public DungeonManager Manager
		{
			get
			{
				return view.Manager;
			}
		}

		public void ResetState()
		{
			Map.ResetState();
			UI.ResetState();
			Menu.ResetState();
			Log.ResetState();
		}

		public string GetFloorText(string color = null)
		{
			if (Manager.currentFloor == 0)
			{
				if (color == null)
					return GameText.GetString("_system/ab_groundFloor");
				else
					return TextColor.Colorize(GameText.GetString("_system/ab_groundFloor"), color);
			}

			var floor = (Manager.goesDown ? "-" : "") + Manager.currentFloor;

			if (color != null)
				floor = TextColor.Colorize(floor, color);

			return string.Format(GameText.GetString("_system/ab_floor"), floor);
		}

		#region Component Methods
		void Start()
		{
			Map = GetComponentInChildren<DungeonMap>();
			Map.HUD = this;

			UI = GetComponentInChildren<DungeonInterface>();
			UI.HUD = this;

			Menu = GetComponentInChildren<DungeonMenu>();
			Menu.HUD = this;

			Log = GetComponentInChildren<DungeonLog>();
			Log.HUD = this;
		}
		#endregion
	}
}