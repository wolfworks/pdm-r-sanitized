﻿using Dungeon.Core;
using Dungeon.Entity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI
{
	public class DungeonMap : MonoBehaviour
	{
		private readonly Color roomColor = new Color(0.2f, 0.25f, 0.5f);
		private readonly Color corridorColor = new Color(0.2f, 0.5f, 0.5f);
		private readonly Color fluidColor = new Color(0.4f, 0.2f, 0.5f);

		#region Component Fields
		public Image mapImage;
		public Sprite playerIcon;
		public Sprite allyIcon;
		public Sprite enemyIcon;
		public Sprite itemIcon;
		public Sprite trapIcon;
		public Sprite wonderIcon;
		public Sprite stairsIcon;
		#endregion

		private Texture2D map;
		private int[,] explored;
		private List<MapIcon> icons;

		public DungeonHUDManager HUD { get; set; }

		private bool MenuMemory { get; set; }

		public void ResetState()
		{
			for (int y = 0; y < explored.GetLength(0); ++y)
			{
				for (int x = 0; x < explored.GetLength(1); ++x)
				{
					explored[x, y] = -1;
				}
			}

			if (icons.Count > 0)
			{
				foreach (var i in icons)
					Destroy(i.icon.gameObject);

				icons = new List<MapIcon>();
			}
		}

		public void Unveil(int x, int y, bool secondTime = false)
		{
			if (x < 0 || x >= HUD.Manager.width || y < 0 || y >= HUD.Manager.height)
				return;

			int t = -1;
			if (explored[x, y] == -1)
			{
				t = (int)HUD.Manager.TileAt(x, y);
				explored[x, y] = t;
			}

			if (t == 1 || !secondTime)
			{
				if (x > 0 && explored[x - 1, y] == -1)
					Unveil(x - 1, y, true);

				if (y > 0 && explored[x, y - 1] == -1)
					Unveil(x, y - 1, true);

				if (x + 1 < HUD.Manager.width && explored[x + 1, y] == -1)
					Unveil(x + 1, y, true);

				if (y + 1 < HUD.Manager.height && explored[x, y + 1] == -1)
					Unveil(x, y + 1, true);
			}

			if (!secondTime)
				RefreshMap();
		}

		#region Utility Methods
		public void AddIcon(DungeonEntity entity, IconType type)
		{
			var o = new GameObject("MapIcon");
			o.transform.parent = mapImage.transform;
			o.transform.localScale = Vector3.one;
			o.transform.SetAsFirstSibling();

			Sprite s = null;
			switch (type)
			{
				case IconType.Player: s = playerIcon; break;
				case IconType.Ally: s = allyIcon; break;
				case IconType.Enemy: s = enemyIcon; break;
				case IconType.Item: s = itemIcon; break;
				case IconType.Stairs: s = stairsIcon; break;
				case IconType.Trap: s = trapIcon; break;
				case IconType.Wonder: s = wonderIcon; break;
			}

			var img = o.AddComponent<Image>();
			img.sprite = s;

			img.rectTransform.sizeDelta = new Vector2(DungeonManager.MAP_PIXEL, DungeonManager.MAP_PIXEL);
			img.rectTransform.pivot = Vector2.zero;
			img.rectTransform.anchorMin = Vector2.zero;
			img.rectTransform.anchorMax = Vector2.zero;
			img.rectTransform.anchoredPosition = entity.position * DungeonManager.MAP_PIXEL;

			if (explored[entity.position.x, entity.position.y] == -1 || explored[entity.position.x, entity.position.y] == 0)
				o.SetActive(false);

			icons.Add(new MapIcon(entity, img, type));
		}

		public void RemoveIcon(DungeonEntity entity)
		{
			for (int i = 0; i < icons.Count; ++i)
			{
				if (icons[i].entity == entity)
				{
					Destroy(icons[i].icon.gameObject);
					icons.RemoveAt(i);
					break;
				}
			}
		}
		#endregion

		#region Private Methods
		private void RefreshMap()
		{
			for (int y = 0; y < HUD.Manager.height; ++y)
			{
				for (int x = 0; x < HUD.Manager.width; ++x)
				{
					if (explored[x, y] == 1)
						map.SetPixel(x, y, roomColor);
					else if (explored[x, y] == 2)
						map.SetPixel(x, y, corridorColor);
					else if (explored[x, y] == 3)
						map.SetPixel(x, y, fluidColor);
					else
						map.SetPixel(x, y, Color.clear);
				}
			}

			foreach (var i in icons)
			{
				i.icon.rectTransform.anchoredPosition = i.entity.position * DungeonManager.MAP_PIXEL;

				if (i.type == IconType.Item || i.type == IconType.Stairs || i.type == IconType.Wonder)
					i.icon.gameObject.SetActive((explored[i.entity.position.x, i.entity.position.y] != -1 && explored[i.entity.position.x, i.entity.position.y] != 0));
				else if (i.type == IconType.Trap)
					i.icon.gameObject.SetActive(((DungeonFloor)i.entity).revealed && explored[i.entity.position.x, i.entity.position.y] != -1 && explored[i.entity.position.x, i.entity.position.y] != 0);
				else if (i.type == IconType.Enemy)
					i.icon.gameObject.SetActive(HUD.Manager.Player.Context.visibleEnemies.Contains((DungeonCharacter)i.entity));
				else
					i.icon.gameObject.SetActive(true);
			}

			map.Apply();
			mapImage.sprite = Sprite.Create(map, new Rect(0, 0, HUD.Manager.width, HUD.Manager.height), new Vector2(0.5f, 0.5f));
			mapImage.rectTransform.sizeDelta = new Vector2(HUD.Manager.width * DungeonManager.MAP_PIXEL, HUD.Manager.height * DungeonManager.MAP_PIXEL);
		}
		#endregion

		#region Component Methods
		void Start()
		{
			map = new Texture2D(HUD.Manager.width, HUD.Manager.height, TextureFormat.RGBA32, false)
			{
				filterMode = FilterMode.Point
			};

			explored = new int[HUD.Manager.width, HUD.Manager.height];

			icons = new List<MapIcon>();
		}

		void Update()
		{
			if (HUD.Menu.Opened != MenuMemory)
			{
				MenuMemory = HUD.Menu.Opened;
				mapImage.gameObject.SetActive(!HUD.Menu.Opened);
			}

			if (!HUD.Menu.Opened && icons.Count > 0)
			{
				icons[0].icon.color = new Color(1f, 1f, 1f, 1f-Mathf.Round(Mathf.Cos(Time.time * 20f)));
			}
		}
		#endregion
	}
}