﻿using Pokemon;
using Pokemon.Data;
using Pokemon.Lexic;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dungeon.UI.Menus
{
	public class DungeonMenuTeam : MonoBehaviour
	{
		#region Component Fields
		public DungeonMenu menuManager;

		[Header("Menu")]
		public GameObject menuPanel;
		public GameObject buttonObject;

		[Header("Status")]
		public Text nameText;
		public Text levelText;
		public Text statsText;
		public Image typeImage1;
		public Image typeImage2;
		public Text abilityText;
		public Text nextLevelText;

		[Header("Moves")]
		public List<Button> moveInteraction;
		public List<GameObject> moveActiveState;
		public List<Text> moveName;
		public List<Text> movePP;
		public List<Image> moveType;
		public List<GameObject> moveLevelling;
		#endregion

		public int PartyMember { get; set; }

		private TeamPreview _teamPreview;
		private int _partyMemberMemory = 0;
		private readonly List<Button> teamButtons = new List<Button>();

		public void SelectMove(int index)
		{
			var pokemon = Game.TeamGetPokemonAt(PartyMember);
			var newState = !pokemon.allowedMoves[index];
			pokemon.allowedMoves[index] = newState;

			moveActiveState[index].SetActive(newState);
		}

		public void CancelMove()
		{
			moveInteraction.ForEach(x => x.interactable = false);

			for (var i = 0; i < teamButtons.Count; ++i)
			{
				if (!teamButtons[i].gameObject.activeSelf)
					continue;

				teamButtons[i].interactable = true;

				if (i == PartyMember)
					EventSystem.current.SetSelectedGameObject(teamButtons[i].gameObject);
			}
		}

		#region Component Messages
		private void Start()
		{
			_teamPreview = GameObject.FindWithTag("Team Preview").GetComponent<TeamPreview>();
			var closeButton = menuPanel.transform.GetChild(0).GetComponent<Button>();

			for (var i = 0; i < Game.Team.Count; ++i)
			{
				var buttonObj = Instantiate(buttonObject, menuPanel.transform);
				buttonObj.transform.SetSiblingIndex(i);

				var text = buttonObj.GetComponent<Text>();

				Color textColor;
				if (i == 0)
				{
					ColorUtility.TryParseHtmlString(TextColor.darkBlue, out textColor);
					buttonObj.AddComponent<DefaultSelected>();
				}
				else
				{
					ColorUtility.TryParseHtmlString(TextColor.darkYellow, out textColor);
				}

				text.color = textColor;
				text.text = Game.TeamGetPokemonAt(i).nickname;

				var index = i;

				var button = buttonObj.GetComponent<Button>();
				button.onClick.AddListener(() =>
				{
					SelectTeamMate();
				});

				var trigger = buttonObj.GetComponent<EventTrigger>();
				trigger.triggers.First(x => x.eventID == EventTriggerType.Select).callback.AddListener(_ =>
				{
					PartyMember = index;
				});

				trigger.triggers.First(x => x.eventID == EventTriggerType.Cancel).callback.AddListener(_ =>
				{
					menuManager.SwitchMenu("Menu Top");
				});

				teamButtons.Add(button);
			}

			teamButtons.Add(closeButton);

			Refresh();
		}

		private void Update()
		{
			if (PartyMember != _partyMemberMemory)
			{
				_partyMemberMemory = PartyMember;
				Refresh();
			}
		}
		#endregion

		#region Private Methods
		private void Refresh()
		{
			var pokemon = Game.TeamGetPokemonAt(PartyMember);
			var f = new Formula(pokemon);

			_teamPreview.SpawnEntity(pokemon.Species.id, pokemon.gender == Gender.Female);

			var name = TextColor.Colorize(pokemon.nickname, PartyMember == 0 ? TextColor.darkBlue : DungeonInterface.HUD_YELLOW);
			var species = TextColor.Colorize(pokemon.Species.name.CorrectTranslation(Game.Lang), TextColor.darkGreen);

			string gender;
			switch (pokemon.gender)
			{
				case Gender.Male:
					gender = "♂";
					break;

				case Gender.Female:
					gender = "♀";
					break;

				default:
					gender = "";
					break;
			}

			nameText.text = $"{name} <size=12>{species} {gender}</size>";
			levelText.text = "<size=12>" + GameText.GetString("_system/ab_level") + ".</size>" + TextColor.Colorize(pokemon.Level.ToString(), DungeonInterface.HUD_YELLOW);
			statsText.text = string.Join("\n",
				TextColor.Colorize(Mathf.Floor(pokemon.HP).ToString(), DungeonInterface.HUD_CYAN) + "/" + pokemon.GetStat(Stat.hp).ToString(),
				pokemon.GetStat(Stat.atk).ToString(),
				pokemon.GetStat(Stat.def).ToString(),
				pokemon.GetStat(Stat.spa).ToString(),
				pokemon.GetStat(Stat.spd).ToString(),
				pokemon.GetStat(Stat.spe).ToString()
			);

			typeImage1.sprite = pokemon.Species.types[0].As<PokemonType>().icon;

			if (pokemon.Species.types[1] != pokemon.Species.types[0])
			{
				typeImage2.gameObject.SetActive(true);
				typeImage2.sprite = pokemon.Species.types[1].As<PokemonType>().icon;
			}
			else
			{
				typeImage2.gameObject.SetActive(false);
			}

			for (int i = 0; i < 4; ++i)
			{
				var move = pokemon.Moves(i);

				if (move == null)
				{
					moveInteraction[i].gameObject.SetActive(false);
				}
				else
				{
					moveInteraction[i].gameObject.SetActive(true);
					moveActiveState[i].SetActive(pokemon.allowedMoves[i]);
					moveType[i].sprite = move.type.As<PokemonType>().icon;

					moveName[i].text = move.name.CorrectTranslation(Game.Lang);
					movePP[i].text = TextColor.Colorize(pokemon.PP[i].ToString(), pokemon.PP[i] == 0 ? DungeonInterface.HUD_RED : DungeonInterface.HUD_CYAN) + "/" + move.numberOfPP.ToString();
				}
			}

			abilityText.text = GameText.GetString("_system/ability") + ": " + TextColor.Colorize(pokemon.Ability.name.CorrectTranslation(Game.Lang), DungeonInterface.HUD_YELLOW);

			if (pokemon.Level == 100)
				nextLevelText.text = "";
			else
				nextLevelText.text = GameText.GetString("_system/next_level") + ": " + TextColor.Colorize((f.ExpAtLevel((byte)(pokemon.Level + 1)) - pokemon.Exp).ToString(), DungeonInterface.HUD_CYAN);
		}

		private void SelectTeamMate()
		{
			teamButtons.ForEach(x => x.interactable = false);
			
			for (var i = 0; i < moveInteraction.Count; ++i)
			{
				if (!moveInteraction[i].gameObject.activeSelf)
					continue;

				moveInteraction[i].interactable = true;

				if (i == 0)
					EventSystem.current.SetSelectedGameObject(moveInteraction[i].gameObject);
			}
		}
		#endregion
	}
}
