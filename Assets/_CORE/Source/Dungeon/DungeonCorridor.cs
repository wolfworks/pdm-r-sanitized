﻿using Delaunay.Geo;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dungeon
{
	public class DungeonCorridor
	{
		public Vector2 start;
		public Vector2 middle;
		public Vector2 end;
		public bool hasNoMiddle;

		private readonly bool middleXIsStartX; // this indicates wether middle.x is equal to start.x or to end.x

		public DungeonCorridor(Vector2 from, Vector2 to, double randomValue)
		{
			start = from;
			end = to;

			if (from.x == to.x || from.y == to.y)
			{
				hasNoMiddle = true;
				return;
			}

			middleXIsStartX = randomValue >= 0.5;

			if (middleXIsStartX)
				middle = new Vector2(start.x, end.y);
			else
				middle = new Vector2(end.x, start.y);
		}

		public void AlignPoints(Vector2 point, float threshold)
		{
			if (Mathf.Abs(point.x - start.x) <= threshold)
				start.x = point.x;

			if (Mathf.Abs(point.x - end.x) <= threshold)
				end.x = point.x;

			if (!hasNoMiddle && Mathf.Abs(point.x - middle.x) <= threshold)
				middle.x = point.x;


			if (Mathf.Abs(point.y - start.y) <= threshold)
				start.y = point.y;

			if (Mathf.Abs(point.y - end.y) <= threshold)
				end.y = point.y;

			if (!hasNoMiddle && Mathf.Abs(point.y - middle.y) <= threshold)
				middle.y = point.y;
		}

		public IEnumerable<LineSegment> GetSegments()
		{
			if (hasNoMiddle)
			{
				return new[] { new LineSegment(start, end) };
			}
			else
			{
				return new[]
				{
					new LineSegment(start, middle),
					new LineSegment(middle, end)
				};
			}
		}

		public IEnumerable<Vector2> WalkThrough()
		{
			if (hasNoMiddle)
				return FromTo(start, end, start.x == end.x);

			if (middleXIsStartX)
				return FromTo(start, middle, true).Concat(FromTo(middle, end, false));
			
			return FromTo(start, middle, false).Concat(FromTo(middle, end, true));
		}

		private IEnumerable<Vector2> FromTo(Vector2 from, Vector2 to, bool onY)
		{
			float startCoord, endCoord;

			if (onY)
			{
				startCoord = Mathf.Min(from.y, to.y);
				endCoord = Mathf.Max(from.y, to.y);
			}
			else
			{
				startCoord = Mathf.Min(from.x, to.x);
				endCoord = Mathf.Max(from.x, to.x);
			}

			for (var i = startCoord; i <= endCoord; ++i)
			{
				if (onY)
					yield return new Vector2(from.x, i);
				else
					yield return new Vector2(i, from.y);
			}
		}
	}
}