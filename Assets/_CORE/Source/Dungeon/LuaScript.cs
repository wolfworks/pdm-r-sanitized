﻿using MoonSharp.Interpreter;
using System;
using UnityEngine;

namespace Dungeon
{
	public class LuaScript : IDisposable
	{
		public Script script;

		private bool handleDispose;

		public LuaScript()
		{
			script = new Script();
			handleDispose = true;
		}

		public void Emit(string eventName, params object[] args)
		{
			var func = script.Globals.Get(eventName);

			if (func != null && func.Type == DataType.Function)
				script.Call(func, args);
		}

		public T? Emit<T>(string eventName, params object[] args) where T : unmanaged
		{
			var func = script.Globals.Get(eventName);

			if (func != null && func.Type == DataType.Function)
			{
				var resp = script.Call(func, args);

				if (resp == null || resp.IsNil())
					return null;

				return resp.ToObject<T>();
			}

			return null;
		}

		public LuaScript With(TextAsset s)
		{
			if (s == null)
				return null;

			return With(s.text);
		}

		public LuaScript With(string s)
		{
			if (s == null)
				return null;

			var tempScript = new LuaScript();
			tempScript.HandleDispose(false);

			tempScript.script.Globals["std"] = script.Globals["std"];
			tempScript.script.DoString(s);

			return tempScript;
		}

		public void HandleDispose(bool handle)
		{
			handleDispose = handle;
		}

		public void Dispose()
		{
			if (handleDispose)
				script = null;

			GC.SuppressFinalize(this);
		}
	}
}
