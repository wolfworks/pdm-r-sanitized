﻿using Dungeon.Core;
using Dungeon.Entity;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;
using Pokemon;
using System;
using UnityEngine;

namespace Dungeon
{
	public class LuaUtility
	{
		public DungeonManager manager;

		static LuaUtility()
		{
			UserData.RegistrationPolicy = InteropRegistrationPolicy.Automatic;
			UserData.RegisterAssembly();
		}

		public LuaUtility(DungeonManager _manager)
		{
			manager = _manager;
		}

		public LuaScript LoadScript(TextAsset s)
		{
			if (s == null || s.text == null)
				return null;

			return LoadScript(s.text);
		}

		public LuaScript LoadScript(string s)
		{
			if (s == null)
				return null;

			LuaScript ls = new LuaScript();

			ls.script.Globals["std"] = new LuaStandard(this);
			ls.script.DoString(s);

			return ls;
		}

		public Action<DungeonCharacter, DungeonCharacter> GetAnimationMark(LuaScript ls)
		{
			if (ls == null)
				return null;

			ls.HandleDispose(false);
				
			var func = ls.script.Globals.Get("onAnimationMark");

			if (func == null || func.Type != DataType.Function)
				return null;

			return (DungeonCharacter user, DungeonCharacter target) =>
			{
				ls.script.Call(func, user, target);
				ls.Dispose();
			};
		}
	}
}
