﻿using UnityEngine;

namespace Dungeon
{
	public static class DirUtility
	{
		public static string ToStringdir(this Vector2Int vector)
		{
			string finalDir = "";

			if (vector.x < 0)
				finalDir += "w";
			if (vector.x > 0)
				finalDir += "e";
			if (vector.y < 0)
				finalDir += "s";
			if (vector.y > 0)
				finalDir += "n";

			return finalDir;
		}

		public static Vector2Int ToVector(this string stringdir)
		{
			if (stringdir == null) return Vector2Int.zero;
		
			var finalDir = Vector2Int.zero;

			if (stringdir.Contains("n"))
				finalDir += Vector2Int.up;
			if (stringdir.Contains("e"))
				finalDir += Vector2Int.right;
			if (stringdir.Contains("s"))
				finalDir += Vector2Int.down;
			if (stringdir.Contains("w"))
				finalDir += Vector2Int.left;

			return finalDir;
		}

		public static string ReverseStringdir(this string stringdir)
		{
			var finalDir = "";

			foreach (char c in stringdir)
			{
				switch (c)
				{
					case 'n': finalDir += "s"; break;
					case 'e': finalDir += "w"; break;
					case 's': finalDir += "n"; break;
					case 'w': finalDir += "e"; break;
				}
			}

			return finalDir;
		}

		public static bool IsStringdir(this string test)
		{
			if (test.Length > 2)
				return false;

			string seen = "";
			foreach (char c in test)
			{
				if (c != 'n' && c != 'e' && c != 's' && c != 'w')
					return false;

				if (seen.Contains(c.ToString()))
					return false;

				seen += c;
			}

			return true;
		}

		public static bool Equals(string a, string b)
		{
			foreach (char c in a)
			{
				if (!b.Contains(c.ToString()))
					return false;
			}

			return true;
		}
	}
}
