﻿#pragma warning disable IDE1006 // Naming Styles, Justification : these are the names that are gonna be used in the lua scripts
using Dungeon.Entity;
using FMODUnity;
using MoonSharp.Interpreter;

namespace Dungeon
{
	[MoonSharpUserData]
	public class LuaStandard
	{
		private readonly LuaUtility handler;

		public LuaStandard(LuaUtility _handler)
		{
			handler = _handler;
		}

		public void message(string entry, params string[] subs)
		{
			var message = string.Format(GameText.GetString(entry), subs);
			handler.manager.LogMessage(message);
		}

		public string writeName(DungeonCharacter entity)
		{
			return TextColor.GetColoredName(entity);
		}

		public void progressOneFloor()
		{
			handler.manager.ProgressOneFloor();
		}

		public void moveRandom(DungeonCharacter entity)
		{
			entity.InstantMove(handler.manager.RandomPos(0));
		}

		public void playSound(string sound)
		{
			RuntimeManager.PlayOneShot($"event:/{sound}");
		}
	}
}