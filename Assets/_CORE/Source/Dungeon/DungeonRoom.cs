﻿using Delaunay.Geo;
using Dungeon.Exceptions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dungeon
{
	public class DungeonRoom
	{
		const float LINE_PRECISION = 0.2f;
		private static Vector2 OUT_POINT = new Vector2(-7f, -11f);

		public readonly List<Vector2> vertices = new List<Vector2>();
		public readonly List<Vector2> entrances = new List<Vector2>();
		public Rect rectBox;

		public void SetVertices(IList<Vector2> _vertices)
		{
			if (_vertices.Count < 4)
				throw new InvalidRoomShapeException();

			vertices.Clear();
			vertices.AddRange(_vertices);

			SetRect();
		}

		public void AddEntrance(Vector2 entrance)
		{
			if (!entrances.Contains(entrance))
				entrances.Add(entrance);
		}

		public bool Contains(Vector2 point)
		{
			if (!rectBox.Contains(point))
				return false;

			var ray = new LineSegment(OUT_POINT, point);
			var encounteredPoints = new List<Vector2>();

			foreach (var segment in GetSegments())
			{
				if (segment.ContainsPoint(point, LINE_PRECISION))
					return true;

				if (ray.Intersects(segment, out var intersection))
				{
					if (!encounteredPoints.Contains(intersection))
						encounteredPoints.Add(intersection);
				}
			}

			return encounteredPoints.Count % 2 != 0;
		}

		public bool Overlaps(DungeonRoom other)
		{
			return rectBox.Overlaps(other.rectBox);
		}

		public Vector2 ClosestPoint(Vector2 origin)
		{
			var result = Vector2.zero;
			var minDistance = 0f;

			foreach (var vertex in vertices)
			{
				var distance = Vector2.Distance(origin, vertex);

				if (distance < minDistance)
				{
					result = vertex;
					minDistance = distance;
				}
			}

			return result;
		}

		public IEnumerable<LineSegment> GetSegments()
		{
			var count = vertices.Count;

			for (var i = 0; i < count; ++i)
				yield return new LineSegment(vertices[i], vertices[(i + 1) % count]);
		}

		private void SetRect()
		{
			var minVector = vertices.First();
			var maxVector = vertices.First();

			foreach (var vertex in vertices)
			{
				minVector = Vector2.Min(minVector, vertex);
				maxVector = Vector2.Max(maxVector, vertex);
			}

			rectBox = Rect.MinMaxRect(minVector.x, minVector.y, maxVector.x + 1, maxVector.y + 1);
		}
	}
}
