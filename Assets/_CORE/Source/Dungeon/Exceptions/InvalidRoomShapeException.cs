﻿using System;
using System.Runtime.Serialization;

namespace Dungeon.Exceptions
{
	[Serializable]
	public class InvalidRoomShapeException : Exception
	{
		public InvalidRoomShapeException()
		{
		}

		public InvalidRoomShapeException(string message) : base(message)
		{
		}

		public InvalidRoomShapeException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected InvalidRoomShapeException(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
		}
	}
}
