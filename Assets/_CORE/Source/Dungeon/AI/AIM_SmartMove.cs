﻿/*using Dungeon.Core;
using Dungeon.Entity;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.AI
{
	public class AIM_SmartMove
	{
		private Vector2Int desiredLocation = Vector2Int.zero;
		private bool wandering = false;

		public string[] Apply(string[] origin, RoomContext context, Dictionary<string, bool> parameters)
		{
			if (origin != null && origin[0] != "" && origin[0] != "goto")
				return origin;

			if (!parameters["moves"])
				return new[] { "skip", "" };

			bool success = false;
			bool reverse = false;

			if (parameters["fetchItems"])
			{
				var item = FindItem(context);

				if (item != null)
				{
					desiredLocation = item.position;
					success = true;
				}
			}

			if (parameters["confrontEnemies"] || parameters["avoidEnemies"])
			{
				var enemy = FindEnemy(context);

				if (enemy != null)
				{
					desiredLocation = enemy.position;
					success = true;
					reverse = parameters["avoidEnemies"];
				}
			}

			if (parameters["followLeader"])
			{
				var leader = FindLeader(context);

				if (leader != null && leader.AIPriority < context.sender.AIPriority)
				{
					desiredLocation = leader.LastPosition;
					success = true;
				}
			}

			string dir;

			if (success)
			{
				wandering = false;
				dir = MoveTowardsDesired(context);

				if (reverse)
					dir = dir.ReverseStringdir();
			}
			else
			{
				dir = Wander(context);
			}

			string finalDir = EvaluateDestination(context, dir, parameters);

			return new string[] { "goto", finalDir };
		}

		public void Reset()
		{
			desiredLocation = Vector2Int.zero;
		}

		#region Private Methods
		private bool Walkable(RoomContext context, Vector2Int pos)
		{
			var crossable = context.sender.manager.TileAt(pos).IsCrossableBy(context.sender.pokemon);

			if (!crossable) return false;

			return !context.sender.manager.EntitiesAt(pos, floor: false, item: false).Exists(x =>
			{
				var dc = (DungeonCharacter)x;
				if (x != context.sender)
				{
					var enemy = dc.enemy != context.sender.enemy;
					var leader = dc.AIPriority <= context.sender.AIPriority;

					return enemy || leader;
				}

				return false;
			});
		}

		private DungeonCharacter FindLeader(RoomContext context)
		{
			if (context.visibleAllies.Count == 0)
				return null;

			int minPriority = context.sender.AIPriority;
			DungeonCharacter favoriteLeader = null;

			foreach (DungeonCharacter dc in context.visibleAllies)
			{
				if (dc.AIPriority < minPriority)
				{
					minPriority = dc.AIPriority;
					favoriteLeader = dc;
				}
			}

			return favoriteLeader;
		}

		private DungeonCharacter FindEnemy(RoomContext context)
		{
			if (context.visibleEnemies.Count == 0)
				return null;

			float minDistance = float.MaxValue;
			DungeonCharacter favoriteEnemy = null;

			foreach (DungeonCharacter dc in context.visibleEnemies)
			{
				float dist = Vector2.Distance(dc.position, context.sender.position);

				if (dist < minDistance)
				{
					minDistance = dist;
					favoriteEnemy = dc;
				}
			}

			return favoriteEnemy;
		}

		private DungeonItem FindItem(RoomContext context)
		{
			if (context.visibleItems.Count == 0)
				return null;

			if (context.sender.enemy && context.sender.pokemon.heldItem != null)
				return null;

			if (!context.sender.enemy && Game.IsBagFull() && context.sender.pokemon.heldItem != null)
				return null;

			float minDistance = float.MaxValue;
			DungeonItem favoriteItem = null;

			foreach (DungeonItem di in context.visibleItems)
			{
				float dist = Vector2.Distance(di.position, context.sender.position);

				if (dist < minDistance)
				{
					minDistance = dist;
					favoriteItem = di;
				}
			}

			return favoriteItem;
		}

		private string Wander(RoomContext context)
		{
			string act;

			if (context.room == null)
			{
				wandering = true;
				desiredLocation = Vector2Int.zero;

				var dir = context.sender.LookAt * -1;
				var paths = new List<Vector2Int>();

				var n = context.sender.position + Vector2Int.up;
				var s = context.sender.position + Vector2Int.down;
				var e = context.sender.position + Vector2Int.right;
				var w = context.sender.position + Vector2Int.left;

				if (Walkable(context, n) && dir != Vector2Int.up)
					paths.Add(Vector2Int.up);
				if (Walkable(context, s) && dir != Vector2Int.down)
					paths.Add(Vector2Int.down);
				if (Walkable(context, e) && dir != Vector2Int.right)
					paths.Add(Vector2Int.right);
				if (Walkable(context, w) && dir != Vector2Int.left)
					paths.Add(Vector2Int.left);

				if (paths.Count != 0)
					dir = paths[Random.Range(0, paths.Count)];

				act = dir.ToStringdir();
			}
			else
			{
				if (desiredLocation != Vector2Int.zero && wandering)
				{
					act = MoveTowardsDesired(context);
				}
				else
				{
					wandering = true;
					var entrances = new List<Vector2Int>();
					var evaluation = (Rect)context.room;
					var backward = context.sender.position + context.sender.LookAt * -1;

					for (int y = (int)evaluation.y; y < (int)evaluation.yMax; y++)
					{
						if (y == (int)evaluation.y || y == (int)evaluation.yMax - 1)
						{
							for (int x = (int)evaluation.x; x < (int)evaluation.xMax; ++x)
							{
								var pos = new Vector2Int(x, y);

								if (context.sender.manager.TileAt(pos) == Tile.Corridor && pos != backward)
									entrances.Add(pos);
							}
						}
						else
						{
							var pos = new Vector2Int((int)evaluation.x, y);
							if (context.sender.manager.TileAt(pos) == Tile.Corridor && pos != backward)
								entrances.Add(pos);

							pos = new Vector2Int((int)evaluation.xMax - 1, y);
							if (context.sender.manager.TileAt(pos) == Tile.Corridor && pos != backward)
								entrances.Add(pos);
						}
					}

					if (entrances.Count == 0)
						desiredLocation = backward;
					else
						desiredLocation = entrances[Random.Range(0, entrances.Count)];

					act = MoveTowardsDesired(context);
				}
			}

			return act;
		}

		private string MoveTowardsDesired(RoomContext context)
		{
			var currentDist = Vector2.Distance(context.sender.position, desiredLocation);

			// We cannot go to exact location, but we're right next to it
			if (!Walkable(context, desiredLocation) && currentDist < DungeonManager.PROXIMITY_THRESHOLD)
				return "";
		
			var finalDir = Vector2Int.zero;
			float minDist = float.MaxValue;

			for (int i = 0; i < 8; ++i)
			{
				bool crossable = true;
				var dir = Vector2Int.zero;
				switch (i)
				{
					case 0:
						dir = Vector2Int.down;
						break;
					case 1:
						dir = Vector2Int.down + Vector2Int.right;
						crossable = context.sender.manager.TileAt(context.sender.position + Vector2Int.down).IsCrossableBy(context.sender.pokemon);
						crossable &= context.sender.manager.TileAt(context.sender.position + Vector2Int.right).IsCrossableBy(context.sender.pokemon);
						break;
					case 2:
						dir = Vector2Int.right;
						break;
					case 3:
						dir = Vector2Int.right + Vector2Int.up;
						crossable = context.sender.manager.TileAt(context.sender.position + Vector2Int.right).IsCrossableBy(context.sender.pokemon);
						crossable &= context.sender.manager.TileAt(context.sender.position + Vector2Int.up).IsCrossableBy(context.sender.pokemon);
						break;
					case 4:
						dir = Vector2Int.up;
						break;
					case 5:
						dir = Vector2Int.up + Vector2Int.left;
						crossable = context.sender.manager.TileAt(context.sender.position + Vector2Int.up).IsCrossableBy(context.sender.pokemon);
						crossable &= context.sender.manager.TileAt(context.sender.position + Vector2Int.left).IsCrossableBy(context.sender.pokemon);
						break;
					case 6:
						dir = Vector2Int.left;
						break;
					case 7:
						dir = Vector2Int.left + Vector2Int.down;
						crossable = context.sender.manager.TileAt(context.sender.position + Vector2Int.left).IsCrossableBy(context.sender.pokemon);
						crossable &= context.sender.manager.TileAt(context.sender.position + Vector2Int.down).IsCrossableBy(context.sender.pokemon);
						break;
				}

				var next = context.sender.position + dir;
				//var cornerCut = context.sender.manager.CornerCut(dir, context.sender.position);

				//if (Walkable(context, next) && crossable && !cornerCut)
				if (Walkable(context, next) && crossable)
				{
					float d = Vector2.Distance(next, desiredLocation);
					if (d < minDist && d < currentDist)
					{
						minDist = d;
						finalDir = dir;
					}
				}
			}

			return finalDir.ToStringdir();
		}

		private string EvaluateDestination(RoomContext context, string dest, Dictionary<string, bool> parameters)
		{
			var futurePos = dest.ToVector();

			if (parameters["avoidTraps"] && context.room != null)
				futurePos = MoveAvoidingTrap(context, futurePos);

			return futurePos.ToStringdir();
		}

		private Vector2Int MoveAvoidingTrap(RoomContext context, Vector2Int dest, int iteration = 0)
		{
			if (iteration == 7)
				return dest;

			foreach (DungeonFloor df in context.visibleFloors)
			{
				if (df.floor.id == "STAIRS" || df.floor.id == "HSTAIRS" || df.floor.id == "WONDER")
					continue;

				if (df.position != dest + context.sender.position)
					continue;

				var newPos = Vector2Int.CeilToInt(((Vector2)dest).Rotate(45f).normalized);

				if (!Walkable(context, context.sender.position + newPos))
				{
					newPos = Vector2Int.CeilToInt(((Vector2)dest).Rotate(-45f).normalized);

					if (!Walkable(context, context.sender.position + newPos))
						return dest;
				}

				return MoveAvoidingTrap(context, Vector2Int.CeilToInt(newPos), iteration + 1);
			}

			return dest;
		}
		#endregion
	}
}
*/