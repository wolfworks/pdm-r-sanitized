﻿using Dungeon.AI.Actions;
using Dungeon.Core;
using Dungeon.Entity;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.AI
{
	public class AIEngine
	{
		public static readonly (Func<AIAction>, int, AIFlag)[] RegisteredActions = new (Func<AIAction>, int, AIFlag)[]
		{
			(() => new ActionSkipTurn(), 0, AIFlag.None),
			(() => new ActionWander(), 1, AIFlag.CanMove),
			(() => new ActionFollowLeader(), 2, AIFlag.CanMove | AIFlag.FollowLeader),
			(() => new ActionEngageEnemies(), 3, AIFlag.CanMove | AIFlag.EngageEnemies),
			(() => new ActionFightEnemy(), 4, AIFlag.None)
		};

		private AIFlag flags;

		private readonly AIBrain brain = new AIBrain();

		public void Reset()
		{
			brain.Reset();
		}

		public void SetFlags(AIFlag _flags)
		{
			flags = _flags;

			var possibleActions = new List<AIAction>();

			foreach (var candidate in RegisteredActions)
			{
				if (candidate.Item3 == AIFlag.None || flags.HasFlag(candidate.Item3))
				{
					var action = candidate.Item1();
					action.SetFlags(flags);
					action.SetBaseScore(candidate.Item2);

					possibleActions.Add(action);
				}
			}

			brain.SetAvailableActions(possibleActions);
		}

		public string[] GetMovement(RoomContext context)
		{
			/* 
			 * The algorithm generates the action as follows:
			 * - If we're not at our desired location, we move towards it
			 * - If we are, we check that our direction is correct, and if not we adjust it
			 * - We pick the action with the highest priority, action priority being
			 *	 firstly regular attack, then move, then item, then check the floor
			 */

			var actionResult = brain.GetDesiredAction(context).Result();

			if (actionResult.position.HasValue && actionResult.position.Value != context.sender.position)
			{
				var movement = GetMovementTowardsLocation(context, actionResult.position.Value);
				movement = EvaluateDestination(context, movement);

				context.sender.LookAt = movement.ToVector();

				return new[] { "goto", movement };
			}

			if (actionResult.direction.HasValue)
				context.sender.LookAt = actionResult.direction.Value;

			if (actionResult.attack)
				return new[] { "attack" };

			if (actionResult.useMove != null)
				return new[] { "move", actionResult.useMove.Value.ToString() };

			if (actionResult.useItem != null)
				return new[] { "item", actionResult.useItem.Value.ToString() };

			if (actionResult.checkFloor)
				return new[] { "floor" };

			return new[] { "skip" };
		}

		#region Private Methods
		private string GetMovementTowardsLocation(RoomContext context, Vector2Int location)
		{
			var currentDist = Vector2.Distance(context.sender.position, location);

			// We cannot go to exact location, but we're right next to it
			if (!context.Walkable(location) && context.sender.IsNextTo(location, false))
				return "";

			var finalDir = Vector2Int.zero;
			float minDist = float.MaxValue;

			for (int i = 0; i < 8; ++i)
			{
				bool crossable = true;
				var dir = Vector2Int.zero;
				switch (i)
				{
					case 0:
						dir = Vector2Int.down;
						break;
					case 1:
						dir = Vector2Int.down + Vector2Int.right;
						crossable = context.Walkable(context.sender.position + Vector2Int.down);
						crossable &= context.Walkable(context.sender.position + Vector2Int.right);
						break;
					case 2:
						dir = Vector2Int.right;
						break;
					case 3:
						dir = Vector2Int.right + Vector2Int.up;
						crossable = context.Walkable(context.sender.position + Vector2Int.right);
						crossable &= context.Walkable(context.sender.position + Vector2Int.up);
						break;
					case 4:
						dir = Vector2Int.up;
						break;
					case 5:
						dir = Vector2Int.up + Vector2Int.left;
						crossable = context.Walkable(context.sender.position + Vector2Int.up);
						crossable &= context.Walkable(context.sender.position + Vector2Int.left);
						break;
					case 6:
						dir = Vector2Int.left;
						break;
					case 7:
						dir = Vector2Int.left + Vector2Int.down;
						crossable = context.Walkable(context.sender.position + Vector2Int.left);
						crossable &= context.Walkable(context.sender.position + Vector2Int.down);
						break;
				}

				var next = context.sender.position + dir;
				//var cornerCut = context.sender.manager.CornerCut(dir, context.sender.position);

				if (context.Walkable(next) && crossable/* && !cornerCut*/)
				{
					float d = Vector2.Distance(next, location);
					if (d < minDist && d < currentDist)
					{
						minDist = d;
						finalDir = dir;
					}
				}
			}

			return finalDir.ToStringdir();
		}

		private string EvaluateDestination(RoomContext context, string dest)
		{
			var futurePos = dest.ToVector();

			if (flags.HasFlag(AIFlag.AvoidTraps) && context.room != null)
				futurePos = MoveAvoidingTrap(context, futurePos);

			return futurePos.ToStringdir();
		}

		private Vector2Int MoveAvoidingTrap(RoomContext context, Vector2Int dest, int iteration = 0)
		{
			if (iteration == 7)
				return dest;

			foreach (DungeonFloor df in context.visibleFloors)
			{
				if (df.floor.id == "STAIRS" || df.floor.id == "HSTAIRS" || df.floor.id == "WONDER")
					continue;

				if (df.position != dest + context.sender.position)
					continue;

				var newPos = dest.Rotate(45f);

				if (!context.Walkable(context.sender.position + newPos))
				{
					newPos = dest.Rotate(-45f);

					if (!context.Walkable(context.sender.position + newPos))
						return dest;
				}

				return MoveAvoidingTrap(context, Vector2Int.CeilToInt(newPos), iteration + 1);
			}

			return dest;
		}
		#endregion
	}
}
