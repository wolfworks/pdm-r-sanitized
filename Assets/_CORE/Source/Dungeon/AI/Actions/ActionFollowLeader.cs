﻿using Dungeon.Entity;

namespace Dungeon.AI.Actions
{
	/*
	 * The character will locate the leader with the highest priority
	 * and will try to follow it
	 */
	public class ActionFollowLeader : AIAction
	{
		private DungeonCharacter favoriteLeader;
  
		public override bool IsDoable()
		{
			if (context.visibleAllies.Count == 0)
				return false;

			int minPriority = context.sender.AIPriority;
			favoriteLeader = null;

			foreach (DungeonCharacter dc in context.visibleAllies)
			{
				if (dc.AIPriority < minPriority)
				{
					minPriority = dc.AIPriority;
					favoriteLeader = dc;
				}
			}

			return favoriteLeader != null;
		}

		public override AIActionResult Result()
		{
			return new AIActionResult
			{
				position = favoriteLeader?.position
			};
		}

		public override void Reset()
		{
			favoriteLeader = null;
		}
	}
}