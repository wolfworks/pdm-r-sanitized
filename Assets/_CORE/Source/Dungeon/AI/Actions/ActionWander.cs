﻿using Dungeon.Core;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dungeon.AI.Actions
{
	/*
	 * If ActionSkipTurn is the default action, this one comes right after
	 * The character will wander around in the dungeon
	 * Its only constraint is that the character needs to be able to move
	 */
	public class ActionWander : AIAction
	{
		private bool hasDestination;
		private Vector2Int desiredLocation;

		public override bool IsDoable() => true;

		public override AIActionResult Result()
		{
			if (context.room == null)
			{
				hasDestination = false;

				var paths = new List<Vector2Int>();

				var n = context.sender.position + Vector2Int.up;
				var s = context.sender.position + Vector2Int.down;
				var e = context.sender.position + Vector2Int.right;
				var w = context.sender.position + Vector2Int.left;

				if (context.sender.LookAt != Vector2Int.down && context.Walkable(n) && DoubleCorridorCheck(Vector2Int.up))
					paths.Add(n);
				if (context.sender.LookAt != Vector2Int.up && context.Walkable(s) && DoubleCorridorCheck(Vector2Int.down))
					paths.Add(s);
				if (context.sender.LookAt != Vector2Int.left && context.Walkable(e) && DoubleCorridorCheck(Vector2Int.right))
					paths.Add(e);
				if (context.sender.LookAt != Vector2Int.right && context.Walkable(w) && DoubleCorridorCheck(Vector2Int.left))
					paths.Add(w);

				if (paths.Count != 0)
					desiredLocation = paths[Random.Range(0, paths.Count)];
				else
					desiredLocation = context.sender.LastPosition;
			}
			else if (!hasDestination)
			{
				hasDestination = true;

				var backward = context.sender.position + context.sender.LookAt * -1;
				var entrances = context.room.entrances.Where(x => x != backward).ToList();

				if (entrances.Count == 0)
					desiredLocation = backward;
				else
					desiredLocation = Vector2Int.FloorToInt(entrances[Random.Range(0, entrances.Count)]);
			}

			return new AIActionResult
			{
				position = desiredLocation
			};
		}

		public override void Reset()
		{
			hasDestination = false;
		}

		#region Private Methods
		// This methods checks if the entity is in a double corridor
		// And if so, it will check if going in a specified direction will actually make it go further	
		private bool DoubleCorridorCheck(Vector2Int desiredDirection)
		{
			var firstDiagonal = context.WalkableDir(desiredDirection.Rotate(45f), false);
			var firstNormal = context.WalkableDir(desiredDirection.Rotate(90f), false);

			// If both the diagonal and the normal directions are walkable, then we move only if two tiles ahead is walkable too
			if (firstDiagonal && firstNormal)
				return context.Walkable(context.sender.position + desiredDirection * 2);

			var secondDiagonal = context.WalkableDir(desiredDirection.Rotate(-45f), false);
			var secondNormal = context.WalkableDir(desiredDirection.Rotate(-90f), false);

			if (secondDiagonal && secondNormal)
				return context.Walkable(context.sender.position + desiredDirection * 2);

			return true;
		}
		#endregion
	}
}
