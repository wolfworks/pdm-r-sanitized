﻿using Dungeon.Entity;
using Pokemon;
using Pokemon.Data;
using Pokemon.Lexic;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dungeon.AI.Actions
{
	/*
	 * The character will first check if any of its moves can hit one or more targets at a distance
	 * If it is the case, the character will use the move that hits the most enemies
	 * If not, the character will check if it holds an item that could harm an enemy at a distance, and use it if possible (TODO)
	 * If not, the character will check if there is at least one enemy next to itself
	 * If PrioritizeWeakEnemies is set, the character will always pick the weakest enemy (the one with the lowest HP ratio)
	 * If the flag is not set, the character will pick a random enemy
	 * Either way, the character will try to fight the same enemy each turn, until it faints or runs away
	 */
	public class ActionFightEnemy : AIAction
	{
		struct MoveInformation
		{
			public int moveIndex;
			public Vector2Int direction;
			public List<DungeonCharacter> victims;
		}

		private readonly List<MoveInformation> usableDistanceMoves = new List<MoveInformation>();
		private DungeonCharacter targetEnemy;

		public override bool IsDoable()
		{
			if (context.visibleEnemies.Count == 0)
			{
				Reset();
				return false;
			}

			FindTarget();

			if (targetEnemy != null)
				return true;

			GetUsableDistanceMoves();

			if (usableDistanceMoves.Count > 0)
				return true;

			return false;
		}

		public override void Reset()
		{
			usableDistanceMoves.Clear();
			targetEnemy = null;
		}

		public override AIActionResult Result()
		{
			var move = default(MoveInformation);

			if (targetEnemy == null)
			{
				if (flags.HasFlag(AIFlag.PrioritizeHighPPMoves))
				{
					var maxRatio = 0f;

					foreach (var m in usableDistanceMoves)
					{
						var PPRatio = context.sender.pokemon.PP[m.moveIndex] / context.sender.pokemon.Moves(m.moveIndex).numberOfPP;

						if (PPRatio > maxRatio)
						{
							maxRatio = PPRatio;
							move = m;
						}
					}
				}
				else
				{
					move = usableDistanceMoves[Random.Range(0, usableDistanceMoves.Count)];
				}
			}
			else
			{
				move = FightTarget();
			}

			if (move.moveIndex == -1)
			{
				return new AIActionResult
				{
					direction = move.direction,
					attack = true
				};
			}
			else
			{
				return new AIActionResult
				{
					direction = move.direction,
					useMove = move.moveIndex
				};
			}
		}

		#region Private Methods
		private void FindTarget()
		{
			if (targetEnemy != null && !targetEnemy.HasFainted && context.sender.IsNextTo(targetEnemy, true))
				return;

			targetEnemy = null;

			var candidates = context.visibleEnemies.FindAll(x => context.sender.IsNextTo(x, true));

			if (candidates.Count == 0)
				return;

			if (flags.HasFlag(AIFlag.PrioritizeWeakEnemies))
			{
				var minRatio = float.MaxValue;

				foreach (var c in candidates)
				{
					var HPRatio = c.pokemon.HP / c.pokemon.GetStat(Stat.hp);

					if (HPRatio < minRatio)
					{
						minRatio = HPRatio;
						targetEnemy = c;
					}
				}
			}
			else
			{
				targetEnemy = candidates[Random.Range(0, candidates.Count)];
			}
		}

		private MoveInformation FightTarget()
		{
			var direction = targetEnemy.position - context.sender.position;
			var firstPass = new List<(int index, int score, Move move)>();
			var secondPass = new List<(int index, Move move)>();

			// First pass (usable candidates)
			MoveType favoriteMoveType;

			if (context.sender.pokemon.GetStat(Stat.atk) > context.sender.pokemon.GetStat(Stat.spa))
				favoriteMoveType = MoveType.Physical;
			else
				favoriteMoveType = MoveType.Special;

			for (var i = 0; i < 4; ++i)
			{
				if (context.sender.pokemon.PP[i] == 0)
					continue;

				var move = context.sender.pokemon.Moves(i);

				if (move == null
					|| targetEnemy.pokemon.Species.types[0].As<PokemonType>().immunedTo.Contains(move.type)
					|| targetEnemy.pokemon.Species.types[1].As<PokemonType>().immunedTo.Contains(move.type)
					|| move.targeting == MoveTargeting.Allies
					|| move.targeting == MoveTargeting.AlliesAndSelf
					|| move.targeting == MoveTargeting.CurrentTile
					|| move.targeting == MoveTargeting.NoTarget
					|| move.targeting == MoveTargeting.Self
				)
				{
					continue;
				}

				var score = 0;

				if (move.moveType == MoveType.Status)
				{
					if (targetEnemy.pokemon.statuses.Any(x => x.StatusType.associatedMoves.Contains(move.id)))
						continue;
				}
				else
				{
					if (move.moveType == favoriteMoveType) score++;
					if (context.sender.pokemon.Species.IsOfType(move.type)) score++;
					if (move.basePower >= AIConstants.MOVE_POWERFUL_THRESHOLD) score++;

					if (move.targeting == MoveTargeting.RandomDirection) score--;

					if (Game.Veteran)
					{
						var f = new Formula(context.sender.pokemon);
						var effectiveness = f.TypeEffectiveness(targetEnemy.pokemon, move);

						if (effectiveness > 1f)
							score++;
						else if (effectiveness < 1f)
							score--;
					}
				}

				firstPass.Add((index: i, score, move));
			}

			// Second pass (best scores)
			var maxScore = 0;

			foreach (var (index, score, move) in firstPass)
			{
				if (score > maxScore)
				{
					maxScore = score;

					secondPass.Clear();
					secondPass.Add((index, move));
				}
				else if (score >= maxScore - AIConstants.MOVE_SCORE_WIGGLE)
				{
					secondPass.Add((index, move));
				}
			}

			if (secondPass.Count == 0)
			{
				return new MoveInformation
				{
					direction = direction,
					moveIndex = -1
				};
			}
			
			var moveInfo = new MoveInformation { direction = direction };

			if (flags.HasFlag(AIFlag.PrioritizeHighPPMoves))
			{
				var maxRatio = 0f;

				foreach (var (index, move) in secondPass)
				{
					var PPRatio = context.sender.pokemon.PP[index] / move.numberOfPP;

					if (PPRatio > maxRatio)
					{
						maxRatio = PPRatio;
						moveInfo.moveIndex = index;
					}
				}
			}
			else
			{
				moveInfo.moveIndex = secondPass[Random.Range(0, secondPass.Count)].index;
			}

			return moveInfo;
		}

		private void GetUsableDistanceMoves()
		{
			usableDistanceMoves.Clear();

			var minNumberOfVictims = 0;

			for (var i = 0; i < 4; ++i)
			{
				if (context.sender.pokemon.PP[i] == 0)
					continue;

				var move = context.sender.pokemon.Moves(i);

				if (move == null || move.moveType == MoveType.Status)
					continue;

				var direction = context.sender.LookAt;
				var directionDependant = move.targeting == MoveTargeting.Arc
					|| move.targeting == MoveTargeting.CutCorners
					|| move.targeting == MoveTargeting.Line
					|| move.targeting == MoveTargeting.TileInFront
					|| move.targeting == MoveTargeting.TwoTilesFront;

				for (var d = 0; d < (directionDependant ? 8 : 1); ++d)
				{
					var victims = context.sender.manager.GetVictims(move, context.sender, direction);

					if (victims == null)
					{
						if (directionDependant)
						{
							direction = direction.Rotate(45f);
							continue;
						}
						else
						{
							break;
						}
					}

					var numberOfVictims = victims.Count;

					if (numberOfVictims > minNumberOfVictims)
					{
						usableDistanceMoves.Clear();
						usableDistanceMoves.Add(new MoveInformation
						{
							moveIndex = i,
							direction = direction,
							victims = victims
						});

						minNumberOfVictims = numberOfVictims;
					}
					else if (numberOfVictims != 0 && numberOfVictims == minNumberOfVictims)
					{
						usableDistanceMoves.Add(new MoveInformation
						{
							moveIndex = i,
							direction = direction,
							victims = victims
						});
					}
				}
			}
		}
		#endregion
	}
}
