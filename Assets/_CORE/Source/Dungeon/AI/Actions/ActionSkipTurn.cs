﻿namespace Dungeon.AI.Actions
{
	/*
	 * This action is always doable and always present
	 * It does not have any constraint
	 * It's the default action
	 */
	public class ActionSkipTurn : AIAction
	{
		public override bool IsDoable() => true;

		public override AIActionResult Result() => new AIActionResult();

		public override void Reset() { }
	}
}