﻿using Dungeon.Core;

namespace Dungeon.AI.Actions
{
	public abstract class AIAction
	{

		protected AIFlag flags;
		protected RoomContext context;
		protected int baseScore;

		public void SetFlags(AIFlag _flags)
		{
			flags = _flags;
		}
  
		public void SetContext(RoomContext _context)
		{
			context = _context;
		}

		public void SetBaseScore(int _baseScore)
		{
			baseScore = _baseScore;
		}

		public virtual int Score()
		{
			return baseScore;
		}

		/* 
		 * The call stack is as follows:
		 * - IsDoable is called first, and if it returns false, the execution stops
		 * - Score is called after that
		 * - Result is called last
		 */
		public abstract bool IsDoable();
		public abstract AIActionResult Result();
		public abstract void Reset();
	}
}
