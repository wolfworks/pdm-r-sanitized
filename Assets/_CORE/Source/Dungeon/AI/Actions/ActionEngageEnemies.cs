﻿using Dungeon.Entity;
using Pokemon.Lexic;
using UnityEngine;

namespace Dungeon.AI.Actions
{
	/*
	 * The character will select the closest enemy it sees (or the weakest if the corresponding flag is set) and move towards it
	 */
	public class ActionEngageEnemies : AIAction
	{
		public override bool IsDoable() => context.visibleEnemies.Count > 0;

		public override void Reset() {}

		public override AIActionResult Result()
		{
			var enemy = FindEnemy();

			return new AIActionResult
			{
				position = enemy?.position
			};
		}

		#region Private Methods
		private DungeonCharacter FindEnemy()
		{
			var minComparison = float.MaxValue;
			DungeonCharacter favoriteEnemy = null;

			foreach (DungeonCharacter dc in context.visibleEnemies)
			{
				if (flags.HasFlag(AIFlag.PrioritizeWeakEnemies))
				{
					var HPRatio = dc.pokemon.HP / dc.pokemon.GetStat(Stat.hp);

					if (HPRatio < minComparison)
					{
						minComparison = HPRatio;
						favoriteEnemy = dc;
					}
				}
				else
				{
					var dist = Vector2.Distance(dc.position, context.sender.position);

					if (dist < minComparison)
					{
						minComparison = dist;
						favoriteEnemy = dc;
					}
				}
			}

			return favoriteEnemy;
		}
		#endregion
	}
}
