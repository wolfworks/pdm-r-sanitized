﻿using UnityEngine;

namespace Dungeon.AI.Actions
{
	public struct AIActionResult
	{
		public Vector2Int? position;
		public Vector2Int? direction;
		public bool attack;
		public int? useMove;
		public int? useItem;
		public bool checkFloor;
	}
}
