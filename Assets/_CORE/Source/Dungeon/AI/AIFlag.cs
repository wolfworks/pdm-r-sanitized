﻿using System;

namespace Dungeon.AI
{
	[Flags]
	public enum AIFlag
	{
		None = 0,
		FollowLeader = 1,
		FetchItems = 2,
		EngageEnemies = 4,
		PrioritizeWeakEnemies = 8,
		RunAwayIfLowHP = 16,
		TryRegen = 32,
		AvoidTraps = 64,
		DefendStairs = 128,
		PrioritizeHighPPMoves = 256,
		CanMove = 512
	}
}
