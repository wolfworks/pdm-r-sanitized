﻿using Dungeon.Core;

namespace Dungeon.AI
{
	public class DungeonAI
	{
		private AIFlag flags;
		private AIEngine engine = new AIEngine();

		public string[] Decide(RoomContext context)
		{
			return engine?.GetMovement(context);
		}

		public void Reset()
		{
			engine.Reset();
		}

		#region Flag Methods
		public void SetFlags(AIFlag _flags)
		{
			flags = _flags;
			engine.SetFlags(flags);
		}

		public void AddFlag(AIFlag flag)
		{
			flags |= flag;
			engine.SetFlags(flags);
		}

		public void RemoveFlag(AIFlag flag)
		{
			flags &= ~flag;
			engine.SetFlags(flags);
		}

		public void ToggleFlag(AIFlag flag)
		{
			flags ^= flag;
			engine.SetFlags(flags);
		}
		#endregion
	}
}