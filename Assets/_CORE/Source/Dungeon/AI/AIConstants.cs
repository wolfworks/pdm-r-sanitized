﻿namespace Dungeon.AI
{
	public static class AIConstants
	{
		// The score in the move selection algorithm will wiggle this amount
		public const int MOVE_SCORE_WIGGLE = 2;

		// The move BP threshold after which the AI considers it to ba a powerful move
		public const int MOVE_POWERFUL_THRESHOLD = 100;
	}
}
