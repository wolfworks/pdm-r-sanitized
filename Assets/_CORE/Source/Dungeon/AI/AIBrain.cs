﻿using Dungeon.AI.Actions;
using Dungeon.Core;
using UnityEngine;
using System.Collections.Generic;

namespace Dungeon.AI
{
	public class AIBrain
	{
		private List<AIAction> possibleActions;
		private int favoriteAction = -1;

		public AIAction GetDesiredAction(RoomContext context)
		{
			possibleActions.ForEach(x => x.SetContext(context));
			ChooseAction();

			return possibleActions[favoriteAction];
		}

		public void SetAvailableActions(List<AIAction> actions)
		{
			possibleActions = actions;
		}

		public void Reset()
		{
			possibleActions.ForEach(x => x.Reset());
			favoriteAction = -1;
		}

		#region Private Methods
		private void ChooseAction()
		{
			var favorites = new List<(AIAction, int)>();
			var minScore = 0;

			foreach (var action in possibleActions)
			{
				if (!action.IsDoable()) continue;
	
				var score = action.Score();

				if (score < minScore) continue;

				if (score == minScore) favorites.Add((action, score));

				if (score > minScore)
				{
					minScore = score;
					favorites.Clear();
					favorites.Add((action, score));
				}
			}

			var chosenAction = favorites[Random.Range(0, favorites.Count)];

			favoriteAction = possibleActions.IndexOf(chosenAction.Item1);
		}
		#endregion
	}
}
