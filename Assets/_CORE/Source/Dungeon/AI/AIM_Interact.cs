﻿/*using System.Collections.Generic;
using System.Linq;
using Dungeon.Core;
using Pokemon.Lexic;
using UnityEngine;

using Random = System.Random;

namespace Dungeon.AI
{
	public class AIM_Interact
	{
		public const int MOVE_BASE = 25;
		public const int Z_MOVE_BASE = 10;
		public const int ITEM_BASE = 20;
		public const int REGULAR_ATTACK_BASE = 10;

		private List<IInteractable> potentialActions;

		public string[] Apply(string[] origin, RoomContext context, Dictionary<string, bool> parameters)
		{
			potentialActions = new List<IInteractable>();

			DetermineDamagingMoves(context);
			//DetermineStatusMoves(context);
			//DetermineSupportMoves(context);
			//DetermineItemUse(context);

			var minScore = 0f;
			var favoriteActions = new List<IInteractable>();

			foreach (var action in potentialActions)
			{
				var actionScore = action.ComputeScore();

				if (actionScore == minScore)
				{
					favoriteActions.Add(action);
				}
				else if (actionScore > minScore)
				{
					minScore = actionScore;

					favoriteActions.Clear();
					favoriteActions.Add(action);
				}
			}

			if (!favoriteActions.Any())
				return origin;

			return favoriteActions[new Random().Next(favoriteActions.Count)].AsAction();
		}

		public void Reset()
		{
			potentialActions.Clear();
		}

		#region Private Methods
		private void DetermineDamagingMoves(RoomContext context)
		{
			var potentialMoves = new List<DamagingMoveInteractable>();

			for (var i = 0; i < context.sender.pokemon.moves.Length; ++i)
			{
				var move = context.sender.pokemon.moves[i];

				if (move == null || move.moveType == MoveType.Status)
					continue;

				var direction = context.sender.LookAt;
				var directionDependant = move.targeting == MoveTargeting.Arc
					|| move.targeting == MoveTargeting.CutCorners
					|| move.targeting == MoveTargeting.Line
					|| move.targeting == MoveTargeting.TileInFront
					|| move.targeting == MoveTargeting.TwoTilesFront;

				for (var d = 0; d < 8; ++d)
				{
					var potentialTargets = context.sender.manager.GetVictims(move, context.sender, direction);

					if (potentialTargets == null)
					{
						if (directionDependant)
						{
							direction = Vector2Int.CeilToInt(((Vector2)direction).Rotate(45f).normalized);
							continue;
						}
						else
						{
							break;
						}
					}

					potentialMoves.Add(new DamagingMoveInteractable
					{
						User = context.sender.pokemon,
						MoveIndex = i,
						Targets = potentialTargets
					});
				}
			}

			potentialActions.AddRange(potentialMoves);
		}

		private void DetermineStatusMoves(RoomContext context)
		{
			var potentialMoves = new List<StatusMoveInteractable>();

			for (var i = 0; i < context.sender.pokemon.moves.Length; ++i)
			{
				var move = context.sender.pokemon.moves[i];

				if
				(
					move == null
					|| move.moveType != MoveType.Status
					|| move.targeting == MoveTargeting.Allies
					|| move.targeting == MoveTargeting.AlliesAndSelf
					|| move.targeting == MoveTargeting.NoTarget
					|| move.targeting == MoveTargeting.Self
				)
					continue;

				var potentialTargets = context.sender.manager.GetVictims(move, context.sender);

				if (potentialTargets == null)
					continue;

				var actualTargets = potentialTargets
					.Where(x => !x.pokemon.statuses.Any(y => y.status.associatedMoves.Contains(move.id)))
					.ToList();

				potentialMoves.Add(new StatusMoveInteractable
				{
					MoveIndex = i,
					Targets = actualTargets
				});
			}
		}

		private void DetermineSupportMoves(RoomContext context)
		{
			var potentialMoves = new List<StatusMoveInteractable>();

			for (var i = 0; i < context.sender.pokemon.moves.Length; ++i)
			{
				var move = context.sender.pokemon.moves[i];

				if
				(
					move == null
					|| move.moveType != MoveType.Status
					|| move.targeting != MoveTargeting.Allies
					|| move.targeting != MoveTargeting.AlliesAndSelf
					|| move.targeting != MoveTargeting.NoTarget
					|| move.targeting != MoveTargeting.Self
				)
					continue;

				var potentialTargets = context.sender.manager.GetVictims(move, context.sender);

				if (potentialTargets == null)
					continue;

				potentialMoves.Add(new StatusMoveInteractable
				{
					MoveIndex = i,
					Targets = potentialTargets
				});
			}
		}

		private void DetermineItemUse(RoomContext context)
		{
			if (context.sender.pokemon.heldItem == null)
				return;

			potentialActions.Add(new ItemInteractable
			{

			});
		}
		#endregion
	}
}
*/