﻿using Pokemon;
using UnityEngine;

namespace Dungeon.Entity
{
	public class DungeonItem : DungeonEntity
	{
		public ItemState item;

		public DungeonItem(ItemState _item)
		{
			item = _item;
		}
	
		public override void Spawn(Vector2Int pos)
		{
			position = pos;
			model = Object.Instantiate(item.ItemType.model, new Vector3(position.x, 0f, position.y), Quaternion.identity);
		}
	}
}
