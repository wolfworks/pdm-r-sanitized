﻿using Pokemon.Data;
using UnityEngine;

namespace Dungeon.Entity
{
	public class DungeonFloor : DungeonEntity
	{
		private bool revealWhenSpawn;
	
		public Floor floor;
		public bool revealed;

		public DungeonFloor(Floor _floor, GameObject _model = null, bool _reveal = false)
		{
			floor = _floor;
			revealWhenSpawn = _reveal;

			if (_model == null)
				model = floor.model;
			else
				model = _model;
		}

		public override void Spawn(Vector2Int pos)
		{
			position = pos;

			if (revealWhenSpawn) Reveal();
		}

		public void Reveal()
		{
			if (revealed) return;

			revealed = true;
			model = Object.Instantiate(model, new Vector3(position.x, 0f, position.y), Quaternion.identity);
		}
	}
}
