﻿using Dungeon.Core;
using UnityEngine;

namespace Dungeon.Entity
{
	public abstract class DungeonEntity
	{
		public DungeonManager manager;
		public Vector2Int position;

		protected GameObject model;

		public abstract void Spawn(Vector2Int pos);

		public void Despawn()
		{
			manager.Unregister(this);
			
			if (model != null)
				Object.Destroy(model);
		}

		public Transform Transform
		{
			get
			{
				return model.transform;
			}
		}

		public void ParentTo(Transform parent)
		{
			model.transform.parent = parent;
		}

		public void Attach(Transform child)
		{
			child.SetParent(model.transform);
			child.position = new Vector3(model.transform.position.x, child.position.y, model.transform.position.z);
		}

		public bool IsNextTo(Vector2Int pos, bool checkDiagonals)
		{
			var dist = Vector2.Distance(position, pos);

			if (!checkDiagonals)
				return dist < DungeonManager.PROXIMITY_THRESHOLD;

			return !manager.CornerCut(pos - position, position) && dist < DungeonManager.PROXIMITY_THRESHOLD_DIAG;
		}

		public bool IsNextTo(DungeonEntity entity, bool checkDiagonals)
		{
			return IsNextTo(entity.position, checkDiagonals);
		}
	}
}