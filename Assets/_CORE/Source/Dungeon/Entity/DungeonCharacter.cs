﻿using Character;
using Character.Lexic;
using Dungeon.AI;
using Dungeon.Core;
using Pokemon;
using System.Collections;
using UnityEngine;

namespace Dungeon.Entity
{
	public class DungeonCharacter : DungeonEntity
	{
		private readonly DungeonAI ai;
		private Vector2Int lookAt;
		private Vector2Int positionBeforeAnimation;
		private bool moving;

		public CharacterManager character;
		public DungeonCharacter entityInFront;
		public string[] desiredAction;
		public string[] lastAction;
		public int moveCount;
		public bool hasPlayed;
		public bool enemy;
		public PokemonState pokemon;

		public bool HasFainted { get; private set; }
		public Vector2Int LastPosition { get; private set; }
		public RoomContext Context { get; private set; }
		public int AIPriority { get; private set; }

		public Vector2Int LookAt
		{
			get
			{
				return lookAt;
			}
			set
			{
				lookAt = value;
				character.MovementHandler.LookDirection(lookAt.ToRightHandVector3());
				SetEntityInFront();
			}
		}

		public new Transform Transform
		{
			get
			{
				return character.transform;
			}
		}

		public DungeonCharacter(PokemonState _pokemon, DungeonAI _ai, bool _enemy = true, int _aiPriority = 0)
		{
			pokemon = _pokemon;
			ai = _ai;
			AIPriority = _aiPriority;
			enemy = _enemy;

			moving = false;
			hasPlayed = false;
			moveCount = 0;
		}

		public override void Spawn(Vector2Int pos)
		{
			position = pos;
			positionBeforeAnimation = pos;
			LastPosition = pos;

			HasFainted = false;

			var o = Object.Instantiate(pokemon.Model, new Vector3(position.x, 0f, position.y), Quaternion.Euler(0f, 180f, 0f));
			character = o.GetComponent<CharacterManager>();
			character.Rigidbody.isKinematic = true;
			character.handleAnimation = false;
			character.MovementHandler.UseGravity = false;
			character.AnimationHandler.InADungeon(true);

			LookAt = Vector2Int.down;
		}

		public void Decide()
		{
			desiredAction = ai.Decide(Context);
		}

		public void Reset()
		{
			ai.Reset();
		}

		public bool Move(bool instant = false)
		{
			if (!moving)
				LastPosition = position;
   
			moving = true;
			var pos3 = new Vector3(position.x, 0f, position.y);

			if (instant || !character.Visible)
			{
				character.Teleport(pos3, TeleportMode.Hard);

				if (position != positionBeforeAnimation && character.Visible)
				{
					character.AnimationHandler.SetBool("Walking", true);
					character.AnimationHandler.SetBool("Running", true);
				}
			}
			else
			{
				var prevPos3 = new Vector3(positionBeforeAnimation.x, 0f, positionBeforeAnimation.y);
				var realPos3 = new Vector3(character.Rigidbody.position.x, 0f, character.Rigidbody.position.z);
				var dir = pos3 - prevPos3;

				float speed = dir.magnitude / DungeonManager.TURN_DURATION;

				if (Vector3.Dot(dir, pos3 - realPos3) > 0)
				{
					character.MovementHandler.Move(dir.normalized * speed, true);
					character.AnimationHandler.SetBool("Walking", true);
					character.AnimationHandler.SetBool("Running", false);

					return false;
				}
				else
				{
					character.Teleport(pos3, TeleportMode.Hard);
				}
			}

			positionBeforeAnimation = position;
			moving = false;

			character.StartCoroutine(StopAnimation());

			return true;
		}

		public new void ParentTo(Transform parent)
		{
			character.transform.parent = parent;
		}

		public new void Attach(Transform child)
		{
			child.SetParent(character.transform);
			child.position = new Vector3(character.Rigidbody.position.x, child.position.y, character.Rigidbody.position.z);
		}

		public void SetContext(RoomContext ctx)
		{
			Context = ctx;
		}

		public void TestIfFainted()
		{
			if (pokemon.HP > 0) return;

			HasFainted = true;

			character.AnimationHandler.SetBool("Fainted", true);

			var animationTime = character.AnimationHandler.FaintTime;
			character.StartCoroutine(Faint(animationTime));

			Despawn();
		}

		#region Utility Methods
		public void SetEntityInFront()
		{
			entityInFront = null;

			foreach (var entity in manager.EntitiesAt(position + LookAt, floor: false, item: false))
			{
				entityInFront = (DungeonCharacter)entity;
				break;
			}
		}

		public void InstantMove(Vector2Int _position)
		{
			position = _position;
			positionBeforeAnimation = _position;
			character.Teleport(_position.ToRightHandVector3());

			SetEntityInFront();
			ai?.Reset();
			manager.AskContext(this);
			manager.UpdateMap();
		}
		#endregion

		#region Private Methods
		private IEnumerator StopAnimation()
		{
			yield return new WaitForEndOfFrame();

			if (!moving)
				character.AnimationHandler.SetBool("Walking", false);

			yield return new WaitForEndOfFrame();

			if (!moving)
				character.AnimationHandler.SetBool("Running", false);
		}

		private IEnumerator Faint(float time)
		{
			yield return new WaitForSeconds(time);
		
			var message = string.Format(GameText.GetString("_actions/faint"), TextColor.GetColoredName(this));
			manager.LogMessage(message);

			Object.Destroy(character.gameObject);
		}
		#endregion
	}
}
