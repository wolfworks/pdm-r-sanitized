﻿using Dungeon.Core;
using Pokemon;
using System;
using UnityEngine;

namespace Dungeon.Entity
{
	public class DungeonPlayer : DungeonCharacter
	{
		public string[] sendAction;
		public string[] enforce;
		public bool diagonalMovement = false;

		public DungeonPlayer(PokemonState _pokemon) : base(_pokemon, null, false, 0) { }

		public override void Spawn(Vector2Int pos)
		{
			base.Spawn(pos);
			manager.PlayerSpawned();
		}

		public void SendVector(Vector2 vector)
		{
			if (vector == Vector2.zero)
			{
				sendAction = null;
				return;
			}

			var intVector = Vector2Int.CeilToInt(vector);
			var act = intVector.ToStringdir();

			if (LookAt != vector)
			{
				LookAt = intVector;
				manager.UpdateGrid();
			}

			if (diagonalMovement && act.Length < 2)
				return;

			act = CheckDirection(act);

			if (!string.IsNullOrEmpty(act))
				sendAction = new[] { "goto", act };
		}

		public void SendMove(int move)
		{
			if (manager.IsInTurn || pokemon.Moves(move) == null)
				return;

			sendAction = new[] { "move", move.ToString() };
		}

		public void SendRegularAttack()
		{
			if (manager.IsInTurn)
				return;

			sendAction = new[] { "attack", "" };
		}

		public void SendSkipTurn()
		{
			if (manager.IsInTurn)
				return;

			sendAction = new[] { "skip", "" };
		}

		public void Update()
		{
			character.MovementHandler.LookDirection(LookAt.ToRightHandVector3());
		}

		public void WaitFor(Action callback)
		{
			if (enforce != null && enforce.Length > 0)
			{
				sendAction = enforce;
				enforce = null;
			}

			if (sendAction != null && sendAction.Length > 0)
			{
				desiredAction = sendAction;

				callback();

				sendAction = null;
			}
		}

		public void ResetAction()
		{
			sendAction = null;
		}

		#region Private Methods
		public string CheckDirection(string input)
		{
			var finalDir = "";
			bool diag = input.Length > 1;

			if (input.Contains("n") && (!diag || (diag && manager.TileAt(position + Vector2Int.up).IsCrossableBy(pokemon))))
				finalDir += "n";
			if (input.Contains("e") && (!diag || (diag && manager.TileAt(position + Vector2Int.right).IsCrossableBy(pokemon))))
				finalDir += "e";
			if (input.Contains("s") && (!diag || (diag && manager.TileAt(position + Vector2Int.down).IsCrossableBy(pokemon))))
				finalDir += "s";
			if (input.Contains("w") && (!diag || (diag && manager.TileAt(position + Vector2Int.left).IsCrossableBy(pokemon))))
				finalDir += "w";

			var finalPos = position + finalDir.ToVector();

			if (!manager.TileAt(finalPos).IsCrossableBy(pokemon))
				return "";

			if (manager.EntitiesAt(finalPos, floor: false, item: false).Exists(x => ((DungeonCharacter)x).enemy))
				return "";

			return finalDir;
		}
		#endregion
	}
}
