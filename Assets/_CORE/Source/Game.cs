﻿using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using Pokemon;
using Dungeon;
using System.Linq;
using System.Runtime.Serialization;
using System;

public static class Game
{
	public const byte START_LEVEL = 5;

	public const int MAX_MONEY = 999999;
	public const int MAX_BAG_CAPACITY = 30;
	public const int MAX_STORAGE_CAPACITY = 999;
	public const int MAX_AMMO = 99;

	public const float LIFE_REGEN = 1f / 32f; // hom much do you regen life each turn
	public const int HUNGER_LOSS = 10; // how many turns does it take to lose 1 hunger point
	public const int MAX_MOVE_RANGE = 10; // maximum distance for long range moves
	public const int VETERAN_LEVEL_ADD = 2; // hom many levels are added to enemies when playing veteran
	public const float VETERAN_FLOOR_MULTIPLIER = 1.33f; // multiply the number of floors when playing veteran

	const string filename = "gamestate.dat";

	[Serializable]
	private class Gamestate
	{
		public char[] lang;
		public bool veteran;

		public int currentLevel;
		public int currentChapter;

		public string teamName;
		public List<PokemonState> party;
		public List<int> team;
		public int protagonist;
		public int partner;
		public PokemonState postgamePartner;

		public int money;

		public List<ItemState> bag;
		public List<ItemState> storage;

		public bool tutorial;

		public DungeonInfo? dungeon;
		public bool nightTime;

		public Dictionary<string, int> gamevars;

		public Gamestate()
		{
			lang = GetSupportedLanguage();

			currentLevel = -1;
			currentChapter = 1;

			teamName = "";
			party = new List<PokemonState>();
			team = new List<int>();
			protagonist = -1;
			partner = -1;

			money = 0;
			bag = new List<ItemState>();
			storage = new List<ItemState>();

			dungeon = null;
			nightTime = false;

			gamevars = new Dictionary<string, int>();
		}
	}

	private static Gamestate instance;

	public static bool IsInitialized => instance != null;

	public static void Initialize()
	{
		if (Load() != 0)
			instance = new Gamestate();

		if (!GameText.Initialize())
			Application.Quit();

		PokemonStarter.InitializeStarters();
	}

	public static void Reset()
	{
		instance = new Gamestate();
	}

	public static int CurrentLevel { get => instance.currentLevel; }
	public static int CurrentChapter { get => instance.currentChapter; }
	public static string Lang { get => new string(instance.lang); }
	public static bool Veteran { get => instance.veteran; set => instance.veteran = value; }
	public static string TeamName { get => instance.teamName; set => instance.teamName = value; }
	public static int Protagonist { get => instance.protagonist; set => instance.protagonist = value; }
	public static int Partner { get => instance.partner; set => instance.partner = value; }
	public static PokemonState PostgamePartner { get => instance.postgamePartner; set => instance.postgamePartner = value; }
	public static int Money { get => instance.money; set => instance.money = Mathf.Clamp(value, 0, MAX_MONEY); }
	public static List<ItemState> Bag { get => instance.bag; }
	public static List<ItemState> Storage { get => instance.storage; }
	public static bool Tutorial { get => instance.tutorial; set => instance.tutorial = value; }
	public static DungeonInfo? Dungeon { get => instance.dungeon; set => instance.dungeon = value; }
	public static bool NightTime { get => instance.nightTime; set => instance.nightTime = value; }

	public static IEnumerable<string> Gamevars()
	{
		return instance.gamevars.Keys.AsEnumerable();
	}

	public static int Gamevars(string key)
	{
		if (!instance.gamevars.ContainsKey(key))
			return 0;

		return instance.gamevars[key];
	}

	public static void Gamevars(string key, int val)
	{
		if (!instance.gamevars.ContainsKey(key))
			instance.gamevars.Add(key, val);
		else
			instance.gamevars[key] = val;
	}

	public static IList<int> Team => instance.team;

	public static PokemonState TeamGetPokemonAt(int id)
	{
		if (id < 0 || id >= instance.team.Count)
			return null;

		if (instance.team[id] < 0 || instance.team[id] >= instance.party.Count)
			return null;

		return instance.party[instance.team[id]];
	}

	public static int TeamAddIndex(int id)
	{
		if (id < 0 || id >= instance.party.Count)
			return -1;

		if (instance.team.Contains(id))
			return -1;

		instance.team.Add(id);

		return instance.team.IndexOf(id);
	}

	public static void TeamRemoveIndex(int id)
	{
		if (id < 0 || id >= instance.party.Count)
			return;

		if (!instance.team.Contains(id))
			return;

		instance.team.Remove(id);
	}

	public static PokemonState Party(int id)
	{
		if (id < 0 || id >= instance.party.Count)
			return null;

		return instance.party[id];
	}

	public static int PartyCount()
	{
		return instance.party.Count(x => x != null);
	}

	public static void PartyAdd(PokemonState p)
	{
		var freeSpot = instance.party.FindIndex(x => x == null);

		if (freeSpot == -1)
			instance.party.Add(p);
		else
			instance.party[freeSpot] = p;
	}

	public static void PartyRemove(int id)
	{
		if (id < 0 || id >= instance.party.Count)
			return;

		instance.party[id] = null;
	}

	public static int PartyIndex(PokemonState p)
	{
		return instance.party.IndexOf(p);
	}

	public static bool ItemAdd(ref ItemState item)
	{
		if (item.ItemType.stackable)
		{
			foreach (ItemState i in instance.bag)
			{
				if (i.ItemType == item.ItemType)
				{
					if (i.ammo == MAX_AMMO) return false;

					int remaining = item.ammo;
					while (remaining > 0 && i.ammo < MAX_AMMO)
					{
						++i.ammo;
						--remaining;
					}

					item.ammo = remaining;
					return true;
				}
			}
		}

		if (instance.bag.Count < MAX_BAG_CAPACITY)
		{
			instance.bag.Add(item);
			return true;
		}

		return false;
	}

	public static bool IsBagFull()
	{
		return instance.bag.Count == MAX_BAG_CAPACITY;
	}

	public static int Save(int sceneIndex = -1)
	{
		if (sceneIndex == -1)
			instance.currentLevel = SceneManager.GetActiveScene().buildIndex;
		else
			instance.currentLevel = sceneIndex;

		var bf = new BinaryFormatter();

		using (var file = File.Create(Application.persistentDataPath + "/" + filename))
		{
			try
			{
				bf.Serialize(file, instance);
			}
			catch (SerializationException)
			{
				return -1;
			}
			finally
			{
				file.Close();
			}
		}

		return 0;
	}

	public static int Load(bool moveImmediately = false)
	{
		if (!File.Exists(Application.persistentDataPath + "/" + filename))
			return 1;

		var bf = new BinaryFormatter();

		using (var file = File.Open(Application.persistentDataPath + "/" + filename, FileMode.Open))
		{
			try
			{
				instance = (Gamestate)bf.Deserialize(file);

				if (moveImmediately)
					SceneManager.LoadScene(instance.currentLevel);
			}
			catch (SerializationException)
			{
				// The save file is corrupted			
				return -1;
			}
			finally
			{
				file.Close();
			}
		}

		GameText.Initialize();

		return 0;
	}

	public static bool Erase()
	{
		if (!File.Exists(Application.persistentDataPath + "/" + filename)) return false;

		File.Delete(Application.persistentDataPath + "/" + filename);

		Reset();

		return true;
	}

	public static char[] GetSupportedLanguage()
	{
		switch (Application.systemLanguage)
		{
			case SystemLanguage.French:
				return "FR".ToCharArray();
			case SystemLanguage.English:
				return "EN".ToCharArray();
			default:
				return "FR".ToCharArray();
		}
	}

	public static PokemonDatabase GetDatabase()
	{
		return GameObject.FindWithTag("DataManager").GetComponent<PokemonDataManager>().database;
	}
}