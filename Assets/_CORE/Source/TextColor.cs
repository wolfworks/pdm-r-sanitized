﻿using Dungeon.Entity;

public static class TextColor
{
	public static string red = "#a03939";
	public static string green = "#298729";
	public static string blue = "#1e50c4";
	public static string cyan = "#298a8e";
	public static string magenta = "#83309b";
	public static string yellow = "#877b14";
	public static string orange = "#ba671b";
	public static string pink = "#873a77";

	public static string darkGreen = "#42c942";
	public static string darkBlue = "#4873d6";
	public static string darkCyan = "#1ebdc4";
	public static string darkYellow = "#d9c72d";
	public static string darkOrange = "#c46e1f";

	public static string Colorize(string input, string color)
	{
		return "<color="+color+">"+input+"</color>";
	}

	#region Utility Methods
	public static string GetColoredName(DungeonCharacter entity)
	{
		string color;

		if (entity.enemy)
			color = darkOrange;
		else if (entity is DungeonPlayer)
			color = darkBlue;
		else
			color = darkYellow;

		return Colorize(entity.pokemon.nickname, color);
	}
	#endregion
}