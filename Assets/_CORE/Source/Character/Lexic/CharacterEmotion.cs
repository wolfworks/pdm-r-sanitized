﻿namespace Character.Lexic
{
	[System.Serializable]
	public enum CharacterEmotion
	{
		Neutral,
		Smiling,
		Happy,
		Serious,
		Angry,
		Shocked,
		Shouting,
		Enduring,
		Sleeping,
		Sad
	}
}