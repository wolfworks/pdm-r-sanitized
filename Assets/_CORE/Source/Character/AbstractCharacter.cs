﻿using UnityEngine;

namespace Character
{
	// This abstract class is only really useful when using spawners in the context of Gentity
	public abstract class AbstractCharacter : MonoBehaviour
	{
		public abstract CharacterManager GetManager();
	}
}