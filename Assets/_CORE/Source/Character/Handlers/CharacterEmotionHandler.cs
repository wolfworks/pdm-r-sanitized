﻿using Character.Lexic;
using UnityEngine;

namespace Character.Handler
{
	public class CharacterEmotionHandler : MonoBehaviour
	{
		private CharacterManager _manager;

		private Vector2[] eyesCoords;
		private Vector2[] mouthCoords;
		private Vector2 eyesTexStart;
		private Vector2 mouthTexStart;
		private bool eyesMirrored;
		private bool mouthMirrored;
		private GameObject currentEmotionEffect;

		public CharacterEmotion CurrentEmotion { get; set; }

		public void PopEffect(EmotionEffectType effect)
		{
			StopEffect();

			currentEmotionEffect = Instantiate(_manager.effects[effect].effect, _manager.InteractPoint, Quaternion.identity);
		}

		public void StopEffect()
		{
			if (currentEmotionEffect == null)
				return;

			Destroy(currentEmotionEffect);
			currentEmotionEffect = null;
		}

		#region Component Messages
		private void Awake()
		{
			_manager = GetComponent<CharacterManager>();

			if (_manager.eyes != null)
			{
				eyesTexStart = _manager.eyes.material.GetTextureOffset("_MainTex");
				eyesMirrored = _manager.eyes.material.mainTexture.wrapMode == TextureWrapMode.Mirror;
			}

			if (_manager.mouth != null)
			{
				mouthTexStart = _manager.mouth.material.GetTextureOffset("_MainTex");
				mouthMirrored = _manager.mouth.material.mainTexture.wrapMode == TextureWrapMode.Mirror;
			}

			eyesCoords = new Vector2[]
			{
				new Vector2(0f, 0f),							// Neutral
				new Vector2(!eyesMirrored ? .5f : 1f, -.25f),	// Smiling
				new Vector2(!eyesMirrored ? .5f : 1f, -.25f),	// Happy
				new Vector2(0f, 0f),							// Serious
				new Vector2(!eyesMirrored ? .5f : 1f, 0f),		// Angry
				new Vector2(0f, 0f),							// Shocked
				new Vector2(0f, -.75f),							// Shouting
				new Vector2(0f, -.75f),							// Enduring
				new Vector2(0f, -.5f),							// Sleeping
				new Vector2(!eyesMirrored ? .5f : 1f, -.5f)		// Sad
			};

			mouthCoords = new Vector2[]
			{
				new Vector2(0f, 0f),							// Neutral
				new Vector2(0f, -.25f),							// Smiling
				new Vector2(0f, -.5f),							// Happy
				new Vector2(0f, -.75f),							// Serious
				new Vector2(0f, -.75f),							// Angry
				new Vector2(!mouthMirrored ? .5f : 1f, 0f),		// Shocked
				new Vector2(!mouthMirrored ? .5f : 1f, 0f),		// Shouting
				new Vector2(!mouthMirrored ? .5f : 1f, -.25f),	// Enduring
				new Vector2(0f, -.75f),							// Sleeping
				new Vector2(!mouthMirrored ? .5f : 1f, -.25f)	// Sad
			};
		}

		private void Update()
		{
			if (_manager.eyes != null)
			{
				var offset = eyesCoords[(int)CurrentEmotion];

				_manager.eyes.material.SetTextureOffset("_MainTex", eyesTexStart + offset);
			}

			if (_manager.mouth != null)
			{
				var offset = mouthCoords[(int)CurrentEmotion];

				_manager.mouth.material.SetTextureOffset("_MainTex", mouthTexStart + offset);
			}
		}
		#endregion
	}
}