﻿using System.Collections;
using UnityEngine;

namespace Character.Handler
{
	public class CharacterMovementHandler : MonoBehaviour
	{
		private const float CHECK_GROUND_DISTANCE = 0.4f;
		//private const float CHECK_STEP_DISTANCE = 0.2f;
		//private const float UPWARD_CLIMB_FORCE = 0.1f;
		private const float LANDING_COOLDOWN = 0.75f;
		private const float ORIENTATION_THRESHOLD = 50f;
		private const float PROXIMITY_THRESHOLD = 0.1f;
		private const float DIRECTION_THRESHOLD = 5f;

		private CharacterManager _manager;
		private Rigidbody _rigidbody;
		private bool _onPlatform;
		private bool _onGround = true;
		private bool _running;
		private bool _inCooldown;

		// Movement control from outside
		private Vector3? _goTo;
		private float _goToSpeed;
		private Quaternion? _lookTowards;
		private float _lookTowardsSpeed;
		private bool _ignoreSpeedCalculations;

		public bool UseCameraOrientation { get; set; }
		public bool UsePlatforms { get; set; } = true;
		public bool UseGravity { get; set; } = true;

		public Vector3 CurrentMovement { get; private set; }
		public Quaternion CurrentRotation { get; private set; }

		public bool MotionEnded => !_goTo.HasValue && !_lookTowards.HasValue;

		public void Move(Vector3 movement, bool ignoreSpeed = false)
		{
			if (_goTo.HasValue || _lookTowards.HasValue)
				return;

			CurrentMovement = movement;
			_ignoreSpeedCalculations = ignoreSpeed;
		}

		public void LookDirection(Vector3 direction)
		{
			if (_lookTowards.HasValue)
				return;

			direction.y = 0f;

			if (direction == Vector3.zero)
				return;

			LookDirection(Quaternion.LookRotation(direction, Vector3.up));
		}

		public void LookDirection(Quaternion rotation)
		{
			CurrentRotation = rotation;
		}

		public void SetRunning(bool running)
		{
			_running = running;
		}

		public void GoTo(Vector3 to, float speed)
		{
			_goTo = to;
			_goToSpeed = speed;
		}

		public void LookTowards(Vector3 direction, float speed)
		{
			direction.y = 0f;
			_lookTowards = Quaternion.LookRotation(direction, Vector3.up);
			_lookTowardsSpeed = speed;
			_manager.Turning = true;
		}

		public void Stop()
		{
			_goTo = null;
			_lookTowards = null;
			_manager.Turning = false;
		}

		public void ResetPositionAndRotation()
		{
			CurrentMovement = Vector3.zero;
			CurrentRotation = Quaternion.Euler(0f, _rigidbody.rotation.eulerAngles.y, 0f);
		}

		#region Component Messages
		private void Awake()
		{
			_manager = GetComponent<CharacterManager>();
			_rigidbody = GetComponent<Rigidbody>();
		}

		private void Start()
		{
			CurrentRotation = transform.rotation;

			if (!UseGravity)
				_rigidbody.useGravity = false;
		}

		private void Update()
		{
			// Go To
			if (_goTo.HasValue)
			{
				if (Vector3.Distance(_goTo.Value, _rigidbody.position) < PROXIMITY_THRESHOLD)
				{
					Stop();
				}
				else
				{
					var dir = _goTo.Value - _rigidbody.position;
					dir.y = 0f;

					CurrentMovement = dir.normalized * (_goToSpeed / 3f);
				}
			}

			// LookTowards
			if (_lookTowards.HasValue)
			{
				if (Quaternion.Angle(_rigidbody.rotation, _lookTowards.Value) < DIRECTION_THRESHOLD)
				{
					Stop();
				}
				else
				{
					CurrentRotation = Quaternion.Lerp(_rigidbody.rotation, _lookTowards.Value, _lookTowardsSpeed * 5f * Time.deltaTime);
				}
			}
		}

		private void FixedUpdate()
		{
			// Ground control
			var groundRayStart = _rigidbody.position + Vector3.up * (CHECK_GROUND_DISTANCE / 2f);
			var groundCheck = Physics.Raycast(groundRayStart, Vector3.down, out var hitInfo, CHECK_GROUND_DISTANCE);

			Debug.DrawRay(groundRayStart, Vector3.down * CHECK_GROUND_DISTANCE);

			var normalOrientation = Vector3.up;

			if (groundCheck)
			{
				if (!_onGround)
				{
					_inCooldown = true;
					StartCoroutine(LandingCooldown());
				}

				if (UseGravity)
					_rigidbody.useGravity = false;

				_onGround = true;
				_rigidbody.position = hitInfo.point;

				normalOrientation = hitInfo.normal;

				// Going up stairs and small steps
				//var stepRayStart = transform.position + Vector3.up * CHECK_GROUND_DISTANCE / 2.5f + transform.forward * CHECK_STEP_DISTANCE;
				//var stepCheck = Physics.Raycast(stepRayStart, Vector3.down, CHECK_GROUND_DISTANCE / 3f);

				//Debug.DrawRay(stepRayStart, Vector3.down * (CHECK_GROUND_DISTANCE / 3f), Color.yellow);

				//if (stepCheck && CurrentMovement.magnitude > 0)
				//	_rigidbody.AddForce(Vector3.up * UPWARD_CLIMB_FORCE, ForceMode.VelocityChange);

				// Attaching to platforms
				if (UsePlatforms)
				{
					var isAPlatform = hitInfo.transform.CompareTag("Platform");

					if (isAPlatform && !_onPlatform)
					{
						_onPlatform = true;
						transform.SetParent(hitInfo.transform, true);
					}
					else if (!isAPlatform)
					{
						_onPlatform = false;
						transform.SetParent(null, true);
					}
				}
			}
			else if (_onGround)
			{
				_onGround = false;

				if (UseGravity)
					_rigidbody.useGravity = true;
			}

			// Movement
			var movement = Vector3.zero;

			if (!_inCooldown)
			{
				Vector3 dir;

				if (UseCameraOrientation && !_goTo.HasValue)
				{
					var cameraForward = Camera.main.transform.forward;
					var cameraRight = Camera.main.transform.right;

					cameraForward.y = 0f;
					cameraRight.y = 0f;

					dir = cameraForward.normalized * CurrentMovement.z + cameraRight.normalized * CurrentMovement.x;
					dir = Vector3.ClampMagnitude(dir, 1f);
				}
				else
				{
					dir = CurrentMovement;
				}

				movement = dir;

				if (!_ignoreSpeedCalculations)
				{
					movement *= _manager.maxWalkSpeed;

					if (!_onGround)
						movement *= _manager.airControl;

					if (_running)
						movement *= _manager.runSpeedMultiplier;
				}

				movement = Quaternion.FromToRotation(Vector3.up, normalOrientation) * movement;

				if (movement.magnitude > 0f)
				{
					_rigidbody.constraints &= ~RigidbodyConstraints.FreezePositionX;
					_rigidbody.constraints &= ~RigidbodyConstraints.FreezePositionZ;

					LookDirection(movement);

					var targetPosition = _rigidbody.position + movement * Time.fixedDeltaTime;
					_rigidbody.MovePosition(targetPosition);
				}
				else
				{
					_rigidbody.constraints |= RigidbodyConstraints.FreezePositionX;
					_rigidbody.constraints |= RigidbodyConstraints.FreezePositionZ;
				}
			}

			// Rotation
			Quaternion finalRotation, newRotation;

			if (Vector3.Angle(normalOrientation, Vector3.up) <= ORIENTATION_THRESHOLD)
			{
				var orientation = Quaternion.LookRotation(Vector3.Cross(normalOrientation, -Vector3.right), normalOrientation);
				finalRotation = orientation * CurrentRotation;
			}
			else
			{
				finalRotation = CurrentRotation;
			}

			if (_lookTowards.HasValue)
				newRotation = finalRotation;
			else
				newRotation = Quaternion.Lerp(_rigidbody.rotation, finalRotation, _manager.turnSmoothing * Time.fixedDeltaTime);

			_rigidbody.MoveRotation(newRotation);

			// Updating State
			_manager.CurrentVelocity = movement;
			_manager.OnGround = _onGround;

			// Reset Movement
			CurrentMovement = Vector3.zero;
			_ignoreSpeedCalculations = false;
		}
		#endregion

		#region Private Methods
		private IEnumerator LandingCooldown()
		{
			yield return new WaitForSeconds(LANDING_COOLDOWN);
			_inCooldown = false;
		}
		#endregion
	}
}