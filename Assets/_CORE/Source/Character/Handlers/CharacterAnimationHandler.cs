﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Character.Handler
{
	public class CharacterAnimationHandler : MonoBehaviour
	{
		private const float MAX_ALTITUDE = 0.7f;
		private const float ALTITUDE_SPEED = 0.05f;
		private const float MAX_LOOK_ANGLE = 45f;
		private const float HEAD_RESET_THRESHOLD = 1f;
		private const float HEAD_SPEED = 0.05f;

		private CharacterManager _manager;
		private CharacterManager _playerManager;
		private Animator _animator;
		private AnimatorOverrideController _animatorOverride;
		private float _altitude;
		private Quaternion _realHeadRot;
		private Transform _headLook;
		private int _nod;
		private Vector3? _goTo;

		public float FaintTime { get; private set; }
		public bool IsInImportantAnimation => _animator.GetBool("Important");

		public void HeadLook(Transform look)
		{
			if (look == null)
				throw new ArgumentException("Look cannot be null");
			
			_headLook = look;
		}

		public void StepBack(float distance)
		{
			_goTo = _manager.Rigidbody.position - transform.forward * distance;
		}

		public void ResetLook()
		{
			_headLook = null;
		}

		public void Nod()
		{
			StartCoroutine(StartNodSequence());
		}

		public void SetBool(string name, bool value)
		{
			_animator.SetBool(name, value);
		}

		public void SetInteger(string name, int value)
		{
			_animator.SetInteger(name, value);
		}

		public void SetFloat(string name, float value)
		{
			_animator.SetFloat(name, value);
		}

		public void SetTrigger(string name)
		{
			_animator.SetTrigger(name);
		}

		public void InADungeon(bool on)
		{
			_animator.SetBool("In Combat", on);

			if (on)
			{
				transform.localScale = Vector3.one * _manager.dungeonScale;

				gameObject.layer = 8;
				transform.GetChild(0).SetLayerRecursively(8);
			}
			else
			{
				transform.localScale = Vector3.one;

				gameObject.layer = 0;
				transform.GetChild(0).SetLayerRecursively(0);
			}
		}

		#region Component Messages
		private void Awake()
		{
			_manager = GetComponent<CharacterManager>();
			_animator = GetComponentInChildren<Animator>();
			_animatorOverride = new AnimatorOverrideController(_animator.runtimeAnimatorController);
			_animator.runtimeAnimatorController = _animatorOverride;

			LoadClips();
		}

		private void Start()
		{
			if (!CompareTag("Player"))
			{
				var potentialPlayer = GameObject.FindWithTag("Player");

				if (potentialPlayer != null)
					_playerManager = potentialPlayer.GetComponent<AbstractCharacter>().GetManager();
			}
		}

		private void Update()
		{
			// Movement animations
			if (_manager.handleAnimation)
			{
				_animator.SetBool("Walking", _manager.IsWalking || _manager.Turning);
				_animator.SetBool("Running", _manager.IsRunning);
				_animator.SetBool("Grounded", _manager.OnGround);
			}

			// Look at player
			if (_playerManager != null && _manager.lookAtPlayer && !CompareTag("Player"))
			{
				var playerPos = _playerManager.transform.position;
				var angle = Vector3.Angle(transform.forward, playerPos - transform.position);

				if (Vector3.Distance(playerPos, transform.position) <= CharacterManager.MAX_LOOKAHEAD && angle <= MAX_LOOK_ANGLE)
					HeadLook(_playerManager.head);
				else
					ResetLook();
			}
		}

		private void LateUpdate()
		{
			// Flying
			var flying = (_manager.flyWhenWalking && _manager.IsWalking) || (_manager.flyWhenRunning && _manager.IsRunning);

			if (flying)
			{
				_altitude = Mathf.Lerp(_altitude, MAX_ALTITUDE, ALTITUDE_SPEED);
				_animator.transform.position += transform.up * _altitude;
			}
			else
			{
				_altitude = Mathf.Lerp(_altitude, 0f, ALTITUDE_SPEED);
				_animator.transform.position += transform.up * _altitude;
			}

			// Go To
			if (_goTo != null)
			{
				_manager.transform.position = Vector3.Lerp(_manager.Rigidbody.position, _goTo.Value, Time.deltaTime * 5f);

				if (Vector3.Distance(_manager.Rigidbody.position, _goTo.Value) < 0.1f)
					_goTo = null;
			}

			// Head Orientation
			var desiredHeadRotation = _manager.head.rotation;

			if (_nod != 0)
			{
				var multiplier = Quaternion.Euler(0f, _nod, 0f);
				desiredHeadRotation = _manager.head.rotation * multiplier;
			}
			else if (_headLook != null)
			{
				var multiplier = Quaternion.Euler(0f, _manager.headOffset, 0f);
				desiredHeadRotation = Quaternion.LookRotation(_headLook.position - _manager.head.position, _manager.head.up) * multiplier;
			}

			if (!_manager.Turning && _manager.CurrentVelocity.magnitude == 0f && Quaternion.Angle(_realHeadRot, desiredHeadRotation) >= HEAD_RESET_THRESHOLD)
				_realHeadRot = Quaternion.Lerp(_realHeadRot, desiredHeadRotation, HEAD_SPEED);
			else
				_realHeadRot = desiredHeadRotation;

			_manager.head.rotation = _realHeadRot;
		}
		#endregion

		#region Private Methods
		private void LoadClips()
		{
			var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();

			foreach (AnimationClip clip in _animator.runtimeAnimatorController.animationClips)
			{
				var newclip = _manager.animationClips.FirstOrDefault(x => x.name == clip.name);

				if (newclip != null)
				{
					overrides.Add(new KeyValuePair<AnimationClip, AnimationClip>(clip, newclip));

					if (newclip.name == "Hurt" || newclip.name == "Faint")
						FaintTime += newclip.length;
				}
			}

			_animatorOverride.ApplyOverrides(overrides);
		}

		private IEnumerator StartNodSequence()
		{
			while (_nod < 45)
			{
				_nod += 5;
				yield return new WaitForFixedUpdate();
			}

			while (_nod > 0)
			{
				_nod -= 5;
				yield return new WaitForFixedUpdate();
			}

			_nod = 0;
		}
		#endregion
	}
}