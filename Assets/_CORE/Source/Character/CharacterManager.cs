﻿using Character.Handler;
using Character.Lexic;
using UnityEngine;

namespace Character
{
	[RequireComponent(typeof(CharacterMovementHandler))]
	[RequireComponent(typeof(CharacterAnimationHandler))]
	[RequireComponent(typeof(CharacterEmotionHandler))]
	public class CharacterManager : AbstractCharacter
	{
		private const float RUNNING_THRESHOLD = 1.9f;
		public const float MAX_LOOKAHEAD = 2f;

		#region Component Fields
		[Header("Movement")]

		public float maxWalkSpeed = 3f;
		public float turnSmoothing = 15f;
		public float runSpeedMultiplier = 1.5f;
		[Range(0f, 1f)]
		public float airControl = 0.2f;
		public bool flyWhenWalking = false;
		public bool flyWhenRunning = false;
		public bool alwaysVisible = false;

		[Header("Emotion")]

		public Transform head;
		public Vector3 headInteractPoint;
		public float headOffset;
		public bool lookAtPlayer = false;
		public SkinnedMeshRenderer eyes;
		public SkinnedMeshRenderer mouth;
		public EmotionEffects effects;

		[Header("Animation")]

		public bool handleAnimation = true;
		public float dungeonScale = 1f;
		public AnimationClip[] animationClips;
		#endregion

		// Handlers
		public Rigidbody Rigidbody { get; private set; }
		public Collider Collider { get; private set; }
		public CharacterMovementHandler MovementHandler { get; private set; }
		public CharacterAnimationHandler AnimationHandler { get; private set; }
		public CharacterEmotionHandler EmotionHandler { get; private set; }

		// Character State
		public bool Visible { get; private set; }
		public Vector3 CurrentVelocity { get; set; }
		public bool OnGround { get; set; }
		public bool Turning { get; set; }

		// Misc
		public Vector3 InteractPoint => head.TransformPoint(headInteractPoint);
		public bool IsWalking => CurrentVelocity.magnitude > 0f;
		public bool IsRunning => CurrentVelocity.magnitude > RUNNING_THRESHOLD;

		// Interface Implementation
		public override CharacterManager GetManager() => this;

		public void Teleport(Vector3 location, TeleportMode mode = TeleportMode.Normal)
		{
			switch (mode)
			{
				case TeleportMode.Normal:
					Rigidbody.position = location;
					break;

				case TeleportMode.Hard:
					transform.position = location;
					break;

				case TeleportMode.Interpolate:
					Rigidbody.MovePosition(location);
					break;
			}

			MovementHandler.ResetPositionAndRotation();
		}

		#region Component Messages
		private void Awake()
		{
			Rigidbody = GetComponent<Rigidbody>();
			Collider = GetComponent<Collider>();
			MovementHandler = GetComponent<CharacterMovementHandler>();
			AnimationHandler = GetComponent<CharacterAnimationHandler>();
			EmotionHandler = GetComponent<CharacterEmotionHandler>();
		}

		private void Start()
		{
			CheckVisibility(true);
		}

		private void Update()
		{
			CheckVisibility();
		}

		private void OnDrawGizmosSelected()
		{
			if (head != null)
			{
				var multiplier = Quaternion.Euler(0f, -headOffset, 0f);
				var finalVector = Quaternion.LookRotation(head.forward, head.up) * multiplier * Vector3.forward;

				Gizmos.color = Color.cyan;
				Gizmos.DrawLine(head.position, head.position + finalVector);
				Gizmos.DrawIcon(InteractPoint, "InteractArrow");

				Gizmos.color = Color.red;
				Gizmos.DrawLine(head.position, head.position + head.right);

				Gizmos.color = Color.green;
				Gizmos.DrawLine(head.position, head.position + head.up);

				Gizmos.color = Color.blue;
				Gizmos.DrawLine(head.position, head.position + head.forward);
			}
		}
		#endregion

		#region Private Methods
		private void CheckVisibility(bool force = false)
		{
			var v = alwaysVisible;

			if (!v)
			{
				var camVector = Camera.main.WorldToViewportPoint(Collider.bounds.center);

				v = camVector.x >= -0.3f
					&& camVector.x <= 1.3f
					&& camVector.y >= -0.3f
					&& camVector.y <= 1.3f
					&& camVector.z >= -0.5f;
			}

			if (Visible != v || force)
			{
				Visible = v;

				foreach (var r in GetComponentsInChildren<Renderer>())
					r.enabled = v;
			}
		}
		#endregion
	}
}