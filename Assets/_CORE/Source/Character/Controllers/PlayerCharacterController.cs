﻿#pragma warning disable IDE0051 // Remove unused private members
using Gentity;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character.Controllers
{
	[RequireComponent(typeof(PlayerInput))]
	public class PlayerCharacterController : MonoBehaviour
	{
		#region Component Fields
		public AbstractCharacter character;
		public BackCameraController cameraController;
		public bool controllable;
		#endregion

		private CharacterManager characterManager;
		private GentityEvent currentInteractObject;
		private Vector3 currentMovement;

		public Vector3 GetCharacterPosition => characterManager.transform.position;
		public GentityEvent GetCurrentInteraction => currentInteractObject;

		#region Component Messages
		private void Start()
		{
			characterManager = character.GetManager();

			characterManager.alwaysVisible = true;
			characterManager.MovementHandler.UseCameraOrientation = true;
		}

		private void Update()
		{
			// Move
			characterManager.MovementHandler.Move(currentMovement);
			
			// Check in front of the player for a Gentity event
			var bounds = characterManager.Collider.bounds;

			if (Physics.BoxCast(bounds.center, bounds.extents, characterManager.transform.forward, out var hitInfo, Quaternion.identity, CharacterManager.MAX_LOOKAHEAD))
			{
				if (hitInfo.transform.TryGetComponent(out currentInteractObject))
				{
					if (hitInfo.transform.TryGetComponent<CharacterManager>(out var otherCharacter))
						characterManager.AnimationHandler.HeadLook(otherCharacter.head.transform);
					else
						characterManager.AnimationHandler.HeadLook(hitInfo.transform);
				}
				else
				{
					ClearInteract();
				}
			}
			else
			{
				ClearInteract();
			}
		}

		private void OnLook(InputValue value)
		{
			var inputVector = value.Get<Vector2>();
			var resultVector = new Vector3(-inputVector.y, inputVector.x);

			cameraController.SetRotateValue(resultVector);
		}

		private void OnMove(InputValue value)
		{
			if (!controllable)
				return;

			var movementInput = value.Get<Vector2>();
			var clamped = Vector2.ClampMagnitude(movementInput, 1f);

			currentMovement = clamped.ToRightHandVector3();
		}

		private void OnRun(InputValue value)
		{
			if (!controllable)
				return;

			characterManager.MovementHandler.SetRunning(value.isPressed);
		}

		private void OnAction()
		{
			if (currentInteractObject != null)
				currentInteractObject.SendActionPressed();
		}
		#endregion

		#region Private Methods
		private void ClearInteract()
		{
			characterManager.AnimationHandler.ResetLook();
			currentInteractObject = null;
		}
		#endregion
	}
}
#pragma warning restore IDE0051 // Remove unused private members
