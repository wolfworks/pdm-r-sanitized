﻿using Character.Handler;
using UnityEngine;

namespace Character.Controllers
{
	public class FollowerCharacterController : MonoBehaviour
	{
		private const float MIN_DISTANCE = 0.1f;
		private const float FOLLOW_THRESHOLD = 0.1f;
		
		#region Component Fields
		public AbstractCharacter character;
		public AbstractCharacter followTarget;
		public float maxSpeed = 3f;
		public bool follow = true;
		public float stayAwayDistance = 2f;
		#endregion

		private Collider characterCollider;
		private Collider targetCollider;
		private bool close;

		#region Component Messages
		private void Start()
		{
			characterCollider = character.GetManager().GetComponent<Collider>();
			targetCollider = followTarget.GetManager().GetComponent<Collider>();
		}

		private void Update()
		{
			var velocity = Vector3.zero;

			var targetClosestPoint = targetCollider.ClosestPoint(character.transform.position);
			var characterClosestPoint = characterCollider.ClosestPoint(targetClosestPoint);
			var distance = Vector3.Distance(characterClosestPoint, targetClosestPoint);

			close = distance <= (MIN_DISTANCE + (close ? FOLLOW_THRESHOLD : 0f));

			if (follow && !close)
			{
				var desiredVelocity = followTarget.transform.position - character.transform.position;
				var desiredMagnitude = desiredVelocity.magnitude;

				desiredVelocity = desiredVelocity.normalized * maxSpeed;

				if (desiredMagnitude < stayAwayDistance)
					desiredVelocity *= desiredMagnitude / stayAwayDistance;

				Vector3 steering = desiredVelocity - velocity;

				velocity = Vector3.ClampMagnitude(velocity + steering, maxSpeed);
				velocity.y = 0f;
			}

			character.GetManager().MovementHandler.Move(velocity);
		}
		#endregion
	}
}
