﻿using UnityEngine;

namespace Character.Controllers
{
	public class WandererCharacterController : MonoBehaviour
	{
		#region Component Fields
		public AbstractCharacter character;
		public bool active = true;
		public float speed;
		[Range(0.01f, 1f)]
		public float frequency = 0.4f;
		public bool waitUntilReached = true;
		public Bounds region;
		#endregion

		private CharacterManager characterManager;
		private Bounds realBounds;
		private Vector3 currentObjective;

		public void StartWandering()
		{
			if (active)
				return;

			active = true;
			MakeNewObjective();
		}

		public void StopWandering()
		{
			active = false;
			characterManager.MovementHandler.Stop();
		}

		#region Component Messages
		void Start()
		{
			realBounds = new Bounds(character.transform.position + region.center, region.extents);
			characterManager = character.GetManager();
		}

		void OnDrawGizmosSelected()
		{
			if (character == null)
				return;
			
			Gizmos.color = new Color(0f, 1f, 1f, 0.5f);
			Gizmos.DrawWireCube(character.transform.position + region.center, region.extents);
		}

		void Update()
		{
			if (!active)
				return;

			if (!waitUntilReached || characterManager.MovementHandler.MotionEnded)
			{
				var r = RandomFromDistribution.RandomFromExponentialDistribution(2f, RandomFromDistribution.Direction_e.Right);

				if (r <= frequency)
					MakeNewObjective();
			}
		}
		#endregion

		private void MakeNewObjective()
		{
			currentObjective = new Vector3
			(
				Random.Range(realBounds.min.x, realBounds.max.x),
				Random.Range(realBounds.min.y, realBounds.max.y),
				Random.Range(realBounds.min.z, realBounds.max.z)
			);

			characterManager.MovementHandler.GoTo(currentObjective, speed);
		}
	}
}
