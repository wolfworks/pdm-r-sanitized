﻿using UnityEngine;

namespace Character
{
	public class CharacterSpawner : AbstractCharacter
	{
		[HideInInspector]
		public Mesh gizmoMesh;
		[Range(0f, 2f)]
		public int teamMateIndex;
		public bool startWithGravity = true;

		private CharacterManager manager;

		public override CharacterManager GetManager() => manager;

		public void SetGravity(bool gravity)
		{
			manager.MovementHandler.UseGravity = gravity;
			manager.Rigidbody.isKinematic = !gravity;
			manager.Rigidbody.useGravity = gravity;
		}

		public void SetHandleAnimations(bool handle)
		{
			manager.handleAnimation = handle;

			if (!handle)
			{
				manager.AnimationHandler.SetBool("Walking", false);
				manager.AnimationHandler.SetBool("Running", false);
				manager.AnimationHandler.SetBool("Grounded", true);
			}
		}

		public void SetLayer(string layerName)
		{
			manager.transform.SetLayerRecursively(LayerMask.NameToLayer(layerName));
		}

		private void Start()
		{
			var prefab = Game.TeamGetPokemonAt(teamMateIndex).Model;
			var instance = Instantiate(prefab, transform.position, transform.rotation);

			instance.transform.SetLayerRecursively(gameObject.layer);

			manager = instance.GetComponent<CharacterManager>();

			if (!startWithGravity)
			{
				manager.MovementHandler.UseGravity = false;
				manager.Rigidbody.isKinematic = true;
			}

			Attach();
		}

		private void OnDrawGizmos()
		{
			switch (teamMateIndex)
			{
				case 0:
					Gizmos.color = Color.yellow;
					break;

				case 1:
					Gizmos.color = Color.cyan;
					break;

				case 2:
					Gizmos.color = Color.magenta;
					break;
			}

			Gizmos.DrawWireMesh(gizmoMesh, transform.position, transform.rotation);
		}

		private void Attach()
		{
			transform.SetParent(manager.transform, false);
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
		}
	}
}